gcc -v 2>&1 | tail -n 1 | awk '{print $3}' > ./version
gcc -v 2>&1 | tail -n 1 | awk '{print $3}' | awk -F '.' '{print $1}' > ./version_main
gcc -v 2>&1 | grep Target | awk '{print $2}' > ./target

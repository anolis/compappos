#!/bin/bash

SUBDIRS="abi libstdc++-prettyprinters performance 17_intro 18_support \
	19_diagnostics 20_util 21_strings 22_locale 23_containers 24_iterators \
	25_algorithms 26_numerics 27_io 28_regex 29_atomics 30_threads"

count=0

for dir in $SUBDIRS;
do
	subcount=`grep '\\\\' $dir/Makefile | grep -A10000 BINS | grep -v BINS | wc -l`
	echo $subcount $dir
	count=$(($count + $subcount))
done

echo "total: $count"

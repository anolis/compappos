#!/bin/bash

basedir=$(cd "$(dirname "$0")"; pwd)

ONLYCOUNT="false"
count=0

if [ "$1" == "-c" ]; then
	ONLYCOUNT="true"
	shift
fi

RPTFILE="${basedir}/rpt_libstdc++-$1.csv"
ERRFILE="${basedir}/rpt_libstdc++-$1.err"
INFOFILE="${basedir}/rpt_libstdc++-$1.log"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./async/async \
#	./async/any \
#	./async/42819 \
#	./async/49668 \
#	./async/sync \
#	./async/54297 \
#	./mutex/try_lock/2 \
#	./future/members/valid \
#	./future/members/get2 \
#	./future/members/45133 \
#	./future/members/wait \
#	./future/members/get \
#	./future/members/share \
#	./future/members/wait_for \
#	./future/members/wait_until \
#	./packaged_task/cons/alloc \
#	./packaged_task/cons/3 \
#	./packaged_task/49668 \
#	./packaged_task/members/invoke5 \
#	./packaged_task/members/reset2 \
#	./packaged_task/members/invoke4 \
#	./packaged_task/members/invoke \
#	./packaged_task/members/get_future \
#	./packaged_task/members/invoke3 \
#	./packaged_task/members/invoke2 \
#	./call_once/constexpr \
#	./call_once/49668 \
#	./call_once/call_once1 \
#	./call_once/39909 \
#	./lock/2 \
#	./lock/4 \
#	./condition_variable/54185 \
#	./condition_variable/members/2 \
#	./condition_variable/members/1 \
#	./condition_variable_any/53830 \
#	./condition_variable_any/50862 \
#	./condition_variable_any/members/2 \
#	./condition_variable_any/members/1 \
#	./try_lock/2 \
#	./try_lock/4 \
#	./this_thread/1 \
#	./this_thread/4 \
#	./this_thread/3 \
#	./shared_future/members/valid \
#	./shared_future/members/get2 \
#	./shared_future/members/wait \
#	./shared_future/members/get \
#	./shared_future/members/wait_for \
#	./shared_future/members/wait_until \
#	./timed_mutex/try_lock_for/3 \
#	./timed_mutex/try_lock_until/2 \
#	./timed_mutex/try_lock_until/57641 \
#	./timed_mutex/try_lock/2 \
#	./promise/cons/alloc \
#	./promise/cons/move \
#	./promise/cons/move_assign \
#	./promise/members/set_value2 \
#	./promise/members/get_future \
#	./promise/members/set_value3 \
#	./promise/members/set_exception2 \
#	./promise/members/set_exception \
#	./promise/members/set_value \
#	./promise/members/swap \
#	./promise/60966 \
#	./thread/native_handle/typesizes \
#	./thread/swap/1 \
#	./thread/adl \
#	./thread/cons/2 \
#	./thread/cons/5 \
#	./thread/cons/moveable \
#	./thread/cons/4 \
#	./thread/cons/49668 \
#	./thread/cons/3 \
#	./thread/cons/8 \
#	./thread/cons/6 \
#	./thread/cons/7 \
#	./thread/cons/9 \
#	./thread/members/2 \
#	./thread/members/1 \
#	./thread/members/3 \

EXECS_30_THREADS="\
	./async/launch \
	./mutex/dest/destructor_locked \
	./mutex/unlock/1 \
	./mutex/cons/constexpr \
	./mutex/cons/1 \
	./mutex/lock/1 \
	./mutex/try_lock/1 \
	./mutex/native_handle/typesizes \
	./mutex/native_handle/1 \
	./future/cons/constexpr \
	./future/cons/move \
	./future/cons/default \
	./future/cons/move_assign \
	./once_flag/cons/constexpr \
	./lock_guard/cons/1 \
	./recursive_mutex/dest/destructor_locked \
	./recursive_mutex/unlock/1 \
	./recursive_mutex/cons/1 \
	./recursive_mutex/lock/1 \
	./recursive_mutex/try_lock/2 \
	./recursive_mutex/try_lock/1 \
	./recursive_mutex/native_handle/typesizes \
	./recursive_mutex/native_handle/1 \
	./packaged_task/60564 \
	./packaged_task/cons/2 \
	./packaged_task/cons/move \
	./packaged_task/cons/1 \
	./packaged_task/cons/move_assign \
	./packaged_task/members/valid \
	./packaged_task/members/get_future2 \
	./packaged_task/members/reset \
	./packaged_task/members/swap \
	./lock/1 \
	./lock/3 \
	./condition_variable/cons/1 \
	./condition_variable/native_handle/typesizes \
	./condition_variable_any/cons/1 \
	./try_lock/1 \
	./try_lock/3 \
	./recursive_timed_mutex/dest/destructor_locked \
	./recursive_timed_mutex/try_lock_for/2 \
	./recursive_timed_mutex/try_lock_for/1 \
	./recursive_timed_mutex/try_lock_for/3 \
	./recursive_timed_mutex/unlock/1 \
	./recursive_timed_mutex/cons/1 \
	./recursive_timed_mutex/lock/2 \
	./recursive_timed_mutex/lock/1 \
	./recursive_timed_mutex/try_lock_until/2 \
	./recursive_timed_mutex/try_lock_until/1 \
	./recursive_timed_mutex/try_lock/2 \
	./recursive_timed_mutex/try_lock/1 \
	./recursive_timed_mutex/native_handle/typesizes \
	./recursive_timed_mutex/native_handle/1 \
	./this_thread/2 \
	./shared_future/cons/constexpr \
	./shared_future/cons/move \
	./shared_future/cons/default \
	./shared_future/cons/move_assign \
	./shared_future/cons/assign \
	./shared_future/members/45133 \
	./timed_mutex/dest/destructor_locked \
	./timed_mutex/try_lock_for/2 \
	./timed_mutex/try_lock_for/1 \
	./timed_mutex/unlock/1 \
	./timed_mutex/cons/1 \
	./timed_mutex/lock/1 \
	./timed_mutex/try_lock_until/1 \
	./timed_mutex/try_lock/1 \
	./timed_mutex/native_handle/typesizes \
	./timed_mutex/native_handle/1 \
	./promise/cons/1 \
	./promise/members/get_future2 \
	./unique_lock/locking/2 \
	./unique_lock/locking/1 \
	./unique_lock/locking/4 \
	./unique_lock/locking/3 \
	./unique_lock/modifiers/2 \
	./unique_lock/modifiers/1 \
	./unique_lock/cons/2 \
	./unique_lock/cons/5 \
	./unique_lock/cons/1 \
	./unique_lock/cons/4 \
	./unique_lock/cons/3 \
	./unique_lock/cons/6 \
	./thread/cons/1 \
	./thread/members/5 \
	./thread/members/hardware_concurrency \
	./thread/members/4 \
	"
EXECS_29_ATOM="\
	./atomic/60658 \
	./atomic/operators/integral_conversion \
	./atomic/operators/pointer_partial_void \
	./atomic/operators/integral_assignment \
	./atomic/operators/51811 \
	./atomic/cons/user_pod \
	./atomic/cons/direct_list \
	./atomic/cons/constexpr \
	./atomic/cons/copy_list \
	./atomic/cons/default \
	./atomic/cons/single_value \
	./atomic/cons/49445 \
	./headers/atomic/macros \
	./atomic_integral/operators/integral_conversion \
	./atomic_integral/operators/decrement \
	./atomic_integral/operators/bitwise \
	./atomic_integral/operators/integral_assignment \
	./atomic_integral/operators/increment \
	./atomic_integral/cons/direct_list \
	./atomic_integral/cons/constexpr \
	./atomic_integral/cons/copy_list \
	./atomic_integral/cons/default \
	./atomic_integral/cons/single_value \
	./atomic_flag/clear/1 \
	./atomic_flag/cons/default \
	./atomic_flag/cons/aggregate \
	./atomic_flag/test_and_set/implicit \
	./atomic_flag/test_and_set/explicit-hle \
	./atomic_flag/test_and_set/explicit \
	"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./basic_regex/ctors/extended/cstring \
#	./basic_regex/ctors/basic/raw_string \
#	./traits/char/lookup_collatename \
#	./traits/char/transform_primary \
#	./traits/char/isctype \
#	./traits/char/lookup_classname \
#	./algorithms/regex_match/extended/string_range_02_03 \
#	./algorithms/regex_match/extended/cstring_questionmark \
#	./algorithms/regex_match/extended/cstring_plus \
#	./algorithms/regex_match/basic/string_range_02_03 \

EXECS_28_REGEX="\
	./constants/error_type \
	./constants/match_flag_type \
	./constants/syntax_option_type \
	./init-list \
	./basic_regex/requirements/constexpr_data \
	./basic_regex/ctors/string_char \
	./basic_regex/ctors/string_wchar_t \
	./basic_regex/ctors/move_char \
	./basic_regex/ctors/copy_char \
	./basic_regex/ctors/extended/string_range_01_02_03 \
	./basic_regex/ctors/47724 \
	./basic_regex/ctors/char/cstring \
	./basic_regex/ctors/char/range \
	./basic_regex/ctors/char/cstring_awk \
	./basic_regex/ctors/char/default \
	./basic_regex/ctors/char/cstring_ecma \
	./basic_regex/ctors/char/cstring_egrep \
	./basic_regex/ctors/char/cstring_grep \
	./basic_regex/ctors/wchar_t/cstring \
	./basic_regex/ctors/wchar_t/range \
	./basic_regex/ctors/wchar_t/default \
	./basic_regex/ctors/basic/cstring \
	./basic_regex/ctors/basic/pstring_char \
	./basic_regex/ctors/basic/default \
	./basic_regex/ctors/basic/string_range_01_02_03 \
	./basic_regex/ctors/basic/pstring_wchar_t \
	./basic_regex/assign/char/cstring \
	./basic_regex/assign/char/range \
	./basic_regex/assign/char/moveable \
	./basic_regex/assign/char/cstring_op \
	./basic_regex/assign/char/string_op \
	./basic_regex/assign/char/string \
	./basic_regex/assign/char/pstring \
	./basic_regex/assign/wchar_t/cstring \
	./basic_regex/assign/wchar_t/range \
	./basic_regex/assign/wchar_t/cstring_op \
	./basic_regex/assign/wchar_t/string_op \
	./basic_regex/assign/wchar_t/string \
	./basic_regex/assign/wchar_t/pstring \
	./traits/char/translate \
	./traits/char/translate_nocase \
	./traits/char/ctor \
	./traits/char/length \
	./traits/char/value \
	./traits/char/transform \
	./traits/wchar_t/translate \
	./traits/wchar_t/translate_nocase \
	./traits/wchar_t/ctor \
	./traits/wchar_t/length \
	./traits/wchar_t/value \
	./traits/wchar_t/transform \
	./algorithms/regex_match/extended/string_range_01_03 \
	./algorithms/regex_match/extended/string_range_00_03 \
	./algorithms/regex_match/extended/string_any \
	./algorithms/regex_match/basic/string_range_01_03 \
	./algorithms/regex_match/basic/string_range_00_03 \
	./algorithms/regex_match/basic/string_01 \
	./match_results/ctors/char/default \
	./match_results/ctors/wchar_t/default \
	"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./basic_istream/extractors_other/char/2 \
#	./basic_istream/extractors_other/wchar_t/2 \
#	./basic_istream/extractors_character/char/9826 \
#	./basic_istream/get/char/2 \
#	./basic_istream/get/wchar_t/2 \
#	./basic_istream/seekg/char/fstream \
#	./basic_istream/seekg/char/sstream \
#	./basic_istream/seekg/wchar_t/fstream \
#	./basic_istream/seekg/wchar_t/sstream \
#	./basic_istream/ignore/char/2 \
#	./basic_istream/ignore/wchar_t/2 \
#	./basic_istream/tellg/char/1 \
#	./basic_istream/tellg/char/fstream \
#	./basic_istream/tellg/char/sstream \
#	./basic_istream/tellg/wchar_t/1 \
#	./basic_istream/tellg/wchar_t/fstream \
#	./basic_istream/tellg/wchar_t/sstream \
#	./basic_istream/readsome/char/6746-2 \
#	./basic_istream/readsome/wchar_t/6746-2 \
#	./basic_filebuf/seekoff/char/2-in \
#	./basic_filebuf/seekoff/char/1-in \
#	./basic_filebuf/seekoff/char/1-io \
#	./basic_filebuf/seekoff/char/2-io \
#	./basic_filebuf/sputbackc/char/2-in \
#	./basic_filebuf/sputbackc/char/1-in \
#	./basic_filebuf/underflow/10096 \
#	./basic_filebuf/in_avail/char/1 \
#	./basic_filebuf/snextc/char/2-in \
#	./basic_filebuf/snextc/char/1-in \
#	./basic_filebuf/snextc/char/1-io \
#	./basic_filebuf/snextc/char/2-io \
#	./basic_filebuf/sungetc/char/2-in \
#	./basic_filebuf/sungetc/char/1-in \
#	./basic_filebuf/close/char/2 \
#	./basic_filebuf/close/char/4 \
#	./basic_filebuf/seekpos/char/2-in \
#	./basic_filebuf/seekpos/char/1-in \
#	./basic_filebuf/seekpos/char/1-io \
#	./basic_filebuf/seekpos/char/2-io \
#	./basic_filebuf/open/char/2 \
#	./basic_filebuf/setbuf/char/1 \
#	./basic_filebuf/sgetn/char/2-in \
#	./basic_filebuf/sgetn/char/1-in \
#	./basic_filebuf/sgetn/char/3 \
#	./basic_filebuf/sgetn/char/1-io \
#	./basic_filebuf/sgetn/char/2-io \
#	./basic_filebuf/overflow/char/2 \
#	./basic_filebuf/imbue/char/2 \
#	./basic_filebuf/imbue/wchar_t/2 \
#	./basic_filebuf/pbackfail/char/9761 \
#	./basic_filebuf/sbumpc/char/2-in \
#	./basic_filebuf/sbumpc/char/1-in \
#	./basic_filebuf/sbumpc/char/1-io \
#	./basic_filebuf/sbumpc/char/2-io \
#	./basic_filebuf/sgetc/char/2-in \
#	./basic_filebuf/sgetc/char/1-in \
#	./basic_filebuf/sgetc/char/1-io \
#	./basic_filebuf/sgetc/char/2-io \
#	./objects/char/10 \
#	./objects/char/12048-3 \
#	./objects/char/12048-4 \
#	./objects/char/12048-1 \
#	./objects/char/12048-5 \
#	./objects/char/12048-2 \
#	./objects/wchar_t/12048-3 \
#	./objects/wchar_t/12048-4 \
#	./objects/wchar_t/12048-1 \
#	./objects/wchar_t/12048-5 \
#	./objects/wchar_t/12048-2 \
#	./objects/char/3_xin \
#	./objects/char/9661-2_xin \
#	./objects/char/2523-2_xin \
#	./objects/char/6548_xin \
#	./objects/char/7744_xin \
#	./objects/char/5280_xin \
#	./objects/char/2523-1_xin \
#	./objects/char/6648-1_xin \
#	./objects/char/4_xin \
#	./objects/char/6648-2_xin \
#	./objects/wchar_t/3_xin \
#	./objects/wchar_t/9661-2_xin \
#	./objects/wchar_t/2523-2_xin \
#	./objects/wchar_t/9_xin \
#	./objects/wchar_t/6548_xin \
#	./objects/wchar_t/7744_xin \
#	./objects/wchar_t/5280_xin \
#	./objects/wchar_t/2523-1_xin \
#	./objects/wchar_t/6648-1_xin \
#	./objects/wchar_t/4_xin \
#	./objects/wchar_t/13582-1_xin \
#	./objects/wchar_t/6648-2_xin \
#	./basic_ostream/inserters_other/char/1 \
#	./basic_ostream/inserters_other/wchar_t/1 \
#	./basic_ifstream/cons/char/1 \
#	./basic_ifstream/open/char/1 \
#	./basic_istream/ignore/char/3 \
#	./basic_istream/ignore/wchar_t/3 \

EXECS_27_IO="\
	./basic_iostream/cons/2020 \
	./basic_ostringstream/rdbuf/char/2832 \
	./basic_ostringstream/rdbuf/wchar_t/2832 \
	./basic_ostringstream/cons/2020 \
	./basic_ostringstream/cons/char/3 \
	./basic_ostringstream/cons/wchar_t/3 \
	./basic_ostringstream/pthread3 \
	./basic_ostringstream/str/char/2 \
	./basic_ostringstream/str/char/1 \
	./basic_ostringstream/str/wchar_t/2 \
	./basic_ostringstream/str/wchar_t/1 \
	./basic_istream/extractors_other/pod/3983-3 \
	./basic_istream/extractors_other/char/9555-io \
	./basic_istream/extractors_other/char/9424-in \
	./basic_istream/extractors_other/char/error_failbit \
	./basic_istream/extractors_other/char/1 \
	./basic_istream/extractors_other/char/exceptions_failbit_throw \
	./basic_istream/extractors_other/char/exceptions_null \
	./basic_istream/extractors_other/char/exceptions_badbit_throw \
	./basic_istream/extractors_other/char/3 \
	./basic_istream/extractors_other/char/26181 \
	./basic_istream/extractors_other/char/9318-in \
	./basic_istream/extractors_other/wchar_t/9555-io \
	./basic_istream/extractors_other/wchar_t/9424-in \
	./basic_istream/extractors_other/wchar_t/error_failbit \
	./basic_istream/extractors_other/wchar_t/1 \
	./basic_istream/extractors_other/wchar_t/exceptions_failbit_throw \
	./basic_istream/extractors_other/wchar_t/exceptions_null \
	./basic_istream/extractors_other/wchar_t/exceptions_badbit_throw \
	./basic_istream/extractors_other/wchar_t/3 \
	./basic_istream/extractors_other/wchar_t/26181 \
	./basic_istream/extractors_other/wchar_t/9318-in \
	./basic_istream/extractors_character/pod/3983-2 \
	./basic_istream/extractors_character/char/2 \
	./basic_istream/extractors_character/char/1 \
	./basic_istream/extractors_character/char/4 \
	./basic_istream/extractors_character/char/3 \
	./basic_istream/extractors_character/char/11095-i \
	./basic_istream/extractors_character/char/9555-ic \
	./basic_istream/extractors_character/wchar_t/2 \
	./basic_istream/extractors_character/wchar_t/1 \
	./basic_istream/extractors_character/wchar_t/4 \
	./basic_istream/extractors_character/wchar_t/3 \
	./basic_istream/extractors_character/wchar_t/11095-i \
	./basic_istream/extractors_character/wchar_t/9555-ic \
	./basic_istream/read/char/2 \
	./basic_istream/read/char/1 \
	./basic_istream/read/char/3 \
	./basic_istream/read/wchar_t/2 \
	./basic_istream/read/wchar_t/1 \
	./basic_istream/read/wchar_t/3 \
	./basic_istream/getline/char/2 \
	./basic_istream/getline/char/5 \
	./basic_istream/getline/char/1 \
	./basic_istream/getline/char/4 \
	./basic_istream/getline/char/3 \
	./basic_istream/getline/char/6 \
	./basic_istream/getline/wchar_t/2 \
	./basic_istream/getline/wchar_t/5 \
	./basic_istream/getline/wchar_t/1 \
	./basic_istream/getline/wchar_t/4 \
	./basic_istream/getline/wchar_t/3 \
	./basic_istream/getline/wchar_t/6 \
	./basic_istream/extractors_arithmetic/pod/3983-1 \
	./basic_istream/extractors_arithmetic/char/9555-ia \
	./basic_istream/extractors_arithmetic/char/08 \
	./basic_istream/extractors_arithmetic/char/10 \
	./basic_istream/extractors_arithmetic/char/01 \
	./basic_istream/extractors_arithmetic/char/12 \
	./basic_istream/extractors_arithmetic/char/03 \
	./basic_istream/extractors_arithmetic/char/06 \
	./basic_istream/extractors_arithmetic/char/02 \
	./basic_istream/extractors_arithmetic/char/exceptions_failbit_throw \
	./basic_istream/extractors_arithmetic/char/exceptions_badbit_throw \
	./basic_istream/extractors_arithmetic/char/09 \
	./basic_istream/extractors_arithmetic/char/exceptions_failbit \
	./basic_istream/extractors_arithmetic/char/dr696 \
	./basic_istream/extractors_arithmetic/char/07 \
	./basic_istream/extractors_arithmetic/char/11 \
	./basic_istream/extractors_arithmetic/char/13 \
	./basic_istream/extractors_arithmetic/wchar_t/9555-ia \
	./basic_istream/extractors_arithmetic/wchar_t/08 \
	./basic_istream/extractors_arithmetic/wchar_t/10 \
	./basic_istream/extractors_arithmetic/wchar_t/01 \
	./basic_istream/extractors_arithmetic/wchar_t/12 \
	./basic_istream/extractors_arithmetic/wchar_t/03 \
	./basic_istream/extractors_arithmetic/wchar_t/06 \
	./basic_istream/extractors_arithmetic/wchar_t/02 \
	./basic_istream/extractors_arithmetic/wchar_t/exceptions_failbit_throw \
	./basic_istream/extractors_arithmetic/wchar_t/exceptions_badbit_throw \
	./basic_istream/extractors_arithmetic/wchar_t/09 \
	./basic_istream/extractors_arithmetic/wchar_t/exceptions_failbit \
	./basic_istream/extractors_arithmetic/wchar_t/dr696 \
	./basic_istream/extractors_arithmetic/wchar_t/07 \
	./basic_istream/extractors_arithmetic/wchar_t/11 \
	./basic_istream/extractors_arithmetic/wchar_t/13 \
	./basic_istream/get/char/1 \
	./basic_istream/get/char/3 \
	./basic_istream/get/wchar_t/1 \
	./basic_istream/get/wchar_t/3 \
	./basic_istream/exceptions/char/9561 \
	./basic_istream/exceptions/wchar_t/9561 \
	./basic_istream/cons/3 \
	./basic_istream/putback/char/1 \
	./basic_istream/putback/wchar_t/1 \
	./basic_istream/peek/char/6414 \
	./basic_istream/peek/char/12296 \
	./basic_istream/peek/char/1 \
	./basic_istream/peek/wchar_t/6414 \
	./basic_istream/peek/wchar_t/12296 \
	./basic_istream/peek/wchar_t/1 \
	./basic_istream/seekg/char/2 \
	./basic_istream/seekg/char/8348-2 \
	./basic_istream/seekg/char/8348-1 \
	./basic_istream/seekg/char/exceptions_badbit_throw \
	./basic_istream/seekg/char/26211 \
	./basic_istream/seekg/wchar_t/2 \
	./basic_istream/seekg/wchar_t/8348-2 \
	./basic_istream/seekg/wchar_t/8348-1 \
	./basic_istream/seekg/wchar_t/exceptions_badbit_throw \
	./basic_istream/seekg/wchar_t/26211 \
	./basic_istream/ws/char/1 \
	./basic_istream/ws/wchar_t/1 \
	./basic_istream/ignore/char/7220 \
	./basic_istream/ignore/char/6360 \
	./basic_istream/ignore/char/1 \
	./basic_istream/ignore/wchar_t/7220 \
	./basic_istream/ignore/wchar_t/6360 \
	./basic_istream/ignore/wchar_t/1 \
	./basic_istream/sentry/pod/1 \
	./basic_istream/sentry/char/2 \
	./basic_istream/sentry/char/12297 \
	./basic_istream/sentry/char/1 \
	./basic_istream/sentry/char/3 \
	./basic_istream/sentry/wchar_t/2 \
	./basic_istream/sentry/wchar_t/12297 \
	./basic_istream/sentry/wchar_t/1 \
	./basic_istream/sentry/wchar_t/3 \
	./basic_istream/tellg/char/2 \
	./basic_istream/tellg/char/exceptions_badbit_throw \
	./basic_istream/tellg/char/26211 \
	./basic_istream/tellg/char/8348 \
	./basic_istream/tellg/wchar_t/2 \
	./basic_istream/tellg/wchar_t/exceptions_badbit_throw \
	./basic_istream/tellg/wchar_t/26211 \
	./basic_istream/tellg/wchar_t/8348 \
	./basic_istream/readsome/char/8258 \
	./basic_istream/readsome/char/6746-1 \
	./basic_istream/readsome/wchar_t/8258 \
	./basic_istream/readsome/wchar_t/6746-1 \
	./basic_fstream/rdbuf/char/2832 \
	./basic_fstream/cons/1 \
	./fpos/14320-5 \
	./fpos/14320-3 \
	./fpos/mbstate_t/2 \
	./fpos/mbstate_t/1 \
	./fpos/mbstate_t/3 \
	./fpos/mbstate_t/12065 \
	./fpos/14775 \
	./fpos/11450 \
	./fpos/14320-2 \
	./fpos/14320-4 \
	./fpos/14252 \
	./fpos/14320-1 \
	./basic_stringstream/rdbuf/char/2832 \
	./basic_stringstream/rdbuf/wchar_t/2832 \
	./basic_stringstream/cons/2020 \
	./basic_stringstream/str/char/2 \
	./basic_stringstream/str/char/1 \
	./basic_stringstream/str/char/4 \
	./basic_stringstream/str/char/3 \
	./basic_stringstream/str/wchar_t/2 \
	./basic_stringstream/str/wchar_t/1 \
	./basic_stringstream/str/wchar_t/4 \
	./basic_stringstream/str/wchar_t/3 \
	./rvalue_streams \
	./basic_filebuf/seekoff/12790-3 \
	./basic_filebuf/seekoff/char/3-out \
	./basic_filebuf/seekoff/char/26777 \
	./basic_filebuf/seekoff/char/11543 \
	./basic_filebuf/seekoff/char/12232 \
	./basic_filebuf/seekoff/char/1-out \
	./basic_filebuf/seekoff/char/12790-3 \
	./basic_filebuf/seekoff/char/4 \
	./basic_filebuf/seekoff/char/12790-4 \
	./basic_filebuf/seekoff/char/12790-2 \
	./basic_filebuf/seekoff/char/3-io \
	./basic_filebuf/seekoff/char/45628-1 \
	./basic_filebuf/seekoff/char/2-out \
	./basic_filebuf/seekoff/char/3-in \
	./basic_filebuf/seekoff/char/12790-1 \
	./basic_filebuf/seekoff/12790-4 \
	./basic_filebuf/seekoff/12790-2 \
	./basic_filebuf/seekoff/10132-2 \
	./basic_filebuf/seekoff/wchar_t/9875_seekoff \
	./basic_filebuf/seekoff/wchar_t/2 \
	./basic_filebuf/seekoff/wchar_t/11543 \
	./basic_filebuf/seekoff/wchar_t/12790-3 \
	./basic_filebuf/seekoff/wchar_t/1 \
	./basic_filebuf/seekoff/wchar_t/4 \
	./basic_filebuf/seekoff/wchar_t/12790-4 \
	./basic_filebuf/seekoff/wchar_t/12790-2 \
	./basic_filebuf/seekoff/wchar_t/3 \
	./basic_filebuf/seekoff/wchar_t/12790-1 \
	./basic_filebuf/seekoff/12790-1 \
	./basic_filebuf/seekoff/45628-2 \
	./basic_filebuf/sputbackc/char/1-out \
	./basic_filebuf/sputbackc/char/9425 \
	./basic_filebuf/sputbackc/char/1-io \
	./basic_filebuf/sputbackc/char/2-io \
	./basic_filebuf/sputbackc/char/2-out \
	./basic_filebuf/underflow/char/2 \
	./basic_filebuf/underflow/char/10097 \
	./basic_filebuf/underflow/char/45841 \
	./basic_filebuf/underflow/char/9027 \
	./basic_filebuf/underflow/char/1 \
	./basic_filebuf/underflow/char/3 \
	./basic_filebuf/underflow/wchar_t/2 \
	./basic_filebuf/underflow/wchar_t/5 \
	./basic_filebuf/underflow/wchar_t/45841 \
	./basic_filebuf/underflow/wchar_t/11603 \
	./basic_filebuf/underflow/wchar_t/11389-3 \
	./basic_filebuf/underflow/wchar_t/11389-1 \
	./basic_filebuf/underflow/wchar_t/11389-4 \
	./basic_filebuf/underflow/wchar_t/1 \
	./basic_filebuf/underflow/wchar_t/4 \
	./basic_filebuf/underflow/wchar_t/11544-1 \
	./basic_filebuf/underflow/wchar_t/3 \
	./basic_filebuf/underflow/wchar_t/11389-2 \
	./basic_filebuf/underflow/wchar_t/11544-2 \
	./basic_filebuf/underflow/wchar_t/9178 \
	./basic_filebuf/underflow/wchar_t/9520 \
	./basic_filebuf/sputc/char/2-in \
	./basic_filebuf/sputc/char/1-out \
	./basic_filebuf/sputc/char/1-in \
	./basic_filebuf/sputc/char/1057 \
	./basic_filebuf/sputc/char/9701-2 \
	./basic_filebuf/sputc/char/1-io \
	./basic_filebuf/sputc/char/2-io \
	./basic_filebuf/sputc/char/2-out \
	./basic_filebuf/in_avail/char/9701-3 \
	./basic_filebuf/snextc/char/1-out \
	./basic_filebuf/snextc/char/2-out \
	./basic_filebuf/sputn/char/9339 \
	./basic_filebuf/sputn/char/2-in \
	./basic_filebuf/sputn/char/9701-1 \
	./basic_filebuf/sputn/char/1-out \
	./basic_filebuf/sputn/char/1-in \
	./basic_filebuf/sputn/char/1057 \
	./basic_filebuf/sputn/char/1-io \
	./basic_filebuf/sputn/char/2-io \
	./basic_filebuf/sputn/char/2-out \
	./basic_filebuf/sync/char/9182-1 \
	./basic_filebuf/sync/char/1057 \
	./basic_filebuf/cons/2020 \
	./basic_filebuf/cons/char/1 \
	./basic_filebuf/cons/wchar_t/10132-1 \
	./basic_filebuf/cons/wchar_t/1 \
	./basic_filebuf/sungetc/char/1-out \
	./basic_filebuf/sungetc/char/1-io \
	./basic_filebuf/sungetc/char/2-io \
	./basic_filebuf/sungetc/char/2-out \
	./basic_filebuf/close/char/9964 \
	./basic_filebuf/close/char/5 \
	./basic_filebuf/close/char/4879 \
	./basic_filebuf/close/char/12790-3 \
	./basic_filebuf/close/char/1 \
	./basic_filebuf/close/char/12790-4 \
	./basic_filebuf/close/char/12790-2 \
	./basic_filebuf/close/char/3 \
	./basic_filebuf/close/char/12790-1 \
	./basic_filebuf/close/wchar_t/12790-3 \
	./basic_filebuf/close/wchar_t/12790-4 \
	./basic_filebuf/close/wchar_t/12790-2 \
	./basic_filebuf/close/wchar_t/12790-1 \
	./basic_filebuf/close/12790-1 \
	./basic_filebuf/seekpos/12790-3 \
	./basic_filebuf/seekpos/char/3-out \
	./basic_filebuf/seekpos/char/1-out \
	./basic_filebuf/seekpos/char/12790-3 \
	./basic_filebuf/seekpos/char/12790-4 \
	./basic_filebuf/seekpos/char/12790-2 \
	./basic_filebuf/seekpos/char/3-io \
	./basic_filebuf/seekpos/char/2-out \
	./basic_filebuf/seekpos/char/3-in \
	./basic_filebuf/seekpos/char/12790-1 \
	./basic_filebuf/seekpos/12790-2 \
	./basic_filebuf/seekpos/wchar_t/9875_seekpos \
	./basic_filebuf/seekpos/wchar_t/9874 \
	./basic_filebuf/seekpos/wchar_t/12790-3 \
	./basic_filebuf/seekpos/wchar_t/1 \
	./basic_filebuf/seekpos/wchar_t/12790-4 \
	./basic_filebuf/seekpos/wchar_t/12790-2 \
	./basic_filebuf/seekpos/wchar_t/12790-1 \
	./basic_filebuf/seekpos/12790-1 \
	./basic_filebuf/seekpos/10132-3 \
	./basic_filebuf/open/char/1 \
	./basic_filebuf/open/char/4 \
	./basic_filebuf/open/char/3 \
	./basic_filebuf/open/char/9507 \
	./basic_filebuf/open/12790-1 \
	./basic_filebuf/setbuf/char/2 \
	./basic_filebuf/setbuf/char/12875-2 \
	./basic_filebuf/setbuf/char/3 \
	./basic_filebuf/setbuf/char/12875-1 \
	./basic_filebuf/sgetn/char/1-out \
	./basic_filebuf/sgetn/char/2-out \
	./basic_filebuf/overflow/char/2-unbuf \
	./basic_filebuf/overflow/char/3599 \
	./basic_filebuf/overflow/char/1 \
	./basic_filebuf/overflow/char/9988 \
	./basic_filebuf/overflow/char/9182-2 \
	./basic_filebuf/overflow/char/13858 \
	./basic_filebuf/overflow/char/9169 \
	./basic_filebuf/overflow/wchar_t/11305-2 \
	./basic_filebuf/overflow/wchar_t/11305-4 \
	./basic_filebuf/overflow/wchar_t/11305-3 \
	./basic_filebuf/overflow/wchar_t/11305-1 \
	./basic_filebuf/overflow/wchar_t/13858 \
	./basic_filebuf/imbue/char/13171-2 \
	./basic_filebuf/imbue/char/13582-2 \
	./basic_filebuf/imbue/char/13007 \
	./basic_filebuf/imbue/char/1 \
	./basic_filebuf/imbue/char/13171-1 \
	./basic_filebuf/imbue/char/3 \
	./basic_filebuf/imbue/char/14975-1 \
	./basic_filebuf/imbue/char/13171-4 \
	./basic_filebuf/imbue/char/9322 \
	./basic_filebuf/imbue/wchar_t/13171-3 \
	./basic_filebuf/imbue/wchar_t/13582-2 \
	./basic_filebuf/imbue/wchar_t/12868 \
	./basic_filebuf/imbue/wchar_t/14975-2 \
	./basic_filebuf/imbue/wchar_t/13582-3 \
	./basic_filebuf/imbue/wchar_t/13007 \
	./basic_filebuf/imbue/wchar_t/1 \
	./basic_filebuf/imbue/wchar_t/3 \
	./basic_filebuf/imbue/wchar_t/9322 \
	./basic_filebuf/imbue/12206 \
	./basic_filebuf/showmanyc/char/9533-1 \
	./basic_filebuf/showmanyc/char/9533-2 \
	./basic_filebuf/is_open/char/1 \
	./basic_filebuf/sbumpc/char/1-out \
	./basic_filebuf/sbumpc/char/2-out \
	./basic_filebuf/sgetc/char/1-out \
	./basic_filebuf/sgetc/char/2-out \
	./basic_filebuf/sbumpc/char/9825 \
	./objects/char/2 \
	./objects/char/5 \
	./objects/char/5268 \
	./objects/char/dr455 \
	./objects/char/41037 \
	./objects/char/3045 \
	./objects/char/1 \
	./objects/char/3647 \
	./objects/char/8 \
	./objects/char/6 \
	./objects/char/7 \
	./objects/char/9 \
	./objects/char/9661-1 \
	./objects/wchar_t/2 \
	./objects/wchar_t/5 \
	./objects/wchar_t/1 \
	./objects/wchar_t/9662 \
	./objects/wchar_t/5268 \
	./objects/wchar_t/10 \
	./objects/wchar_t/dr455 \
	./objects/wchar_t/12 \
	./objects/wchar_t/41037 \
	./objects/wchar_t/3045 \
	./objects/wchar_t/3647 \
	./objects/wchar_t/8 \
	./objects/wchar_t/6 \
	./objects/wchar_t/9520 \
	./objects/wchar_t/7 \
	./objects/wchar_t/11 \
	./objects/wchar_t/9661-1 \
	./objects/wchar_t/13 \
	./ios_base/state/1 \
	./ios_base/storage/2 \
	./ios_base/storage/11584 \
	./ios_base/storage/1 \
	./ios_base/storage/3 \
	./ios_base/callbacks/1 \
	./ios_base/sync_with_stdio/2 \
	./ios_base/sync_with_stdio/1 \
	./ios_base/sync_with_stdio/16959 \
	./ios_base/sync_with_stdio/9523 \
	./ios_base/failure/what-1 \
	./ios_base/failure/cons_virtual_derivation \
	./ios_base/failure/what-2 \
	./ios_base/failure/what-3 \
	./ios_base/failure/what-big \
	./ios_base/types/fmtflags/constexpr_operators \
	./ios_base/types/fmtflags/bitmask_operators \
	./ios_base/types/openmode/constexpr_operators \
	./ios_base/types/openmode/bitmask_operators \
	./ios_base/types/iostate/constexpr_operators \
	./ios_base/types/iostate/bitmask_operators \
	./basic_ofstream/pthread2 \
	./basic_ofstream/rdbuf/char/2832 \
	./basic_ofstream/cons/2020 \
	./basic_ofstream/cons/char/1 \
	./basic_ofstream/open/char/1 \
	./basic_ostream/tellp/char/2 \
	./basic_ostream/tellp/char/1 \
	./basic_ostream/tellp/char/exceptions_badbit_throw \
	./basic_ostream/tellp/wchar_t/2 \
	./basic_ostream/tellp/wchar_t/1 \
	./basic_ostream/tellp/wchar_t/exceptions_badbit_throw \
	./basic_ostream/endl/char/1 \
	./basic_ostream/endl/wchar_t/1 \
	./basic_ostream/inserters_character/char/2 \
	./basic_ostream/inserters_character/char/5 \
	./basic_ostream/inserters_character/char/9555-oc \
	./basic_ostream/inserters_character/char/11095-ob \
	./basic_ostream/inserters_character/char/28277-4 \
	./basic_ostream/inserters_character/char/1 \
	./basic_ostream/inserters_character/char/4 \
	./basic_ostream/inserters_character/char/11095-oc \
	./basic_ostream/inserters_character/char/3 \
	./basic_ostream/inserters_character/char/8 \
	./basic_ostream/inserters_character/char/6 \
	./basic_ostream/inserters_character/char/11095-oa \
	./basic_ostream/inserters_character/char/28277-3 \
	./basic_ostream/inserters_character/wchar_t/2 \
	./basic_ostream/inserters_character/wchar_t/5 \
	./basic_ostream/inserters_character/wchar_t/9555-oc \
	./basic_ostream/inserters_character/wchar_t/28277-4 \
	./basic_ostream/inserters_character/wchar_t/28277-1 \
	./basic_ostream/inserters_character/wchar_t/1 \
	./basic_ostream/inserters_character/wchar_t/28277-2 \
	./basic_ostream/inserters_character/wchar_t/4 \
	./basic_ostream/inserters_character/wchar_t/3 \
	./basic_ostream/inserters_character/wchar_t/11095-of \
	./basic_ostream/inserters_character/wchar_t/8 \
	./basic_ostream/inserters_character/wchar_t/6 \
	./basic_ostream/inserters_character/wchar_t/11095-od \
	./basic_ostream/inserters_character/wchar_t/28277-3 \
	./basic_ostream/inserters_character/wchar_t/7 \
	./basic_ostream/inserters_character/wchar_t/11095-oe \
	./basic_ostream/ends/char/2 \
	./basic_ostream/ends/char/1 \
	./basic_ostream/ends/wchar_t/2 \
	./basic_ostream/ends/wchar_t/1 \
	./basic_ostream/exceptions/char/9561 \
	./basic_ostream/exceptions/wchar_t/9561 \
	./basic_ostream/flush/char/2 \
	./basic_ostream/flush/char/1 \
	./basic_ostream/flush/char/exceptions_badbit_throw \
	./basic_ostream/flush/wchar_t/2 \
	./basic_ostream/flush/wchar_t/1 \
	./basic_ostream/flush/wchar_t/exceptions_badbit_throw \
	./basic_ostream/inserters_other/char/2 \
	./basic_ostream/inserters_other/char/5 \
	./basic_ostream/inserters_other/char/9424-out \
	./basic_ostream/inserters_other/char/error_code \
	./basic_ostream/inserters_other/char/error_failbit \
	./basic_ostream/inserters_other/char/4 \
	./basic_ostream/inserters_other/char/exceptions_failbit_throw \
	./basic_ostream/inserters_other/char/exceptions_null \
	./basic_ostream/inserters_other/char/exceptions_badbit_throw \
	./basic_ostream/inserters_other/char/9318-out \
	./basic_ostream/inserters_other/char/3 \
	./basic_ostream/inserters_other/char/9555-oo \
	./basic_ostream/inserters_other/wchar_t/2 \
	./basic_ostream/inserters_other/wchar_t/5 \
	./basic_ostream/inserters_other/wchar_t/9424-out \
	./basic_ostream/inserters_other/wchar_t/error_code \
	./basic_ostream/inserters_other/wchar_t/error_failbit \
	./basic_ostream/inserters_other/wchar_t/4 \
	./basic_ostream/inserters_other/wchar_t/exceptions_failbit_throw \
	./basic_ostream/inserters_other/wchar_t/exceptions_null \
	./basic_ostream/inserters_other/wchar_t/exceptions_badbit_throw \
	./basic_ostream/inserters_other/wchar_t/9318-out \
	./basic_ostream/inserters_other/wchar_t/3 \
	./basic_ostream/inserters_other/wchar_t/9555-oo \
	./basic_ostream/cons/2020 \
	./basic_ostream/cons/char/9827 \
	./basic_ostream/cons/wchar_t/9827 \
	./basic_ostream/put/char/1 \
	./basic_ostream/put/wchar_t/1 \
	./basic_ostream/write/char/1 \
	./basic_ostream/write/wchar_t/1 \
	./basic_ostream/seekp/char/2346-sstream \
	./basic_ostream/seekp/char/exceptions_badbit_throw \
	./basic_ostream/seekp/char/2346-fstream \
	./basic_ostream/seekp/wchar_t/2346-sstream \
	./basic_ostream/seekp/wchar_t/exceptions_badbit_throw \
	./basic_ostream/seekp/wchar_t/2346-fstream \
	./basic_ostream/sentry/pod/1 \
	./basic_ostream/sentry/char/2 \
	./basic_ostream/sentry/char/1 \
	./basic_ostream/sentry/wchar_t/2 \
	./basic_ostream/sentry/wchar_t/1 \
	./basic_ostream/inserters_arithmetic/pod/23875 \
	./basic_ostream/inserters_arithmetic/char/2 \
	./basic_ostream/inserters_arithmetic/char/5 \
	./basic_ostream/inserters_arithmetic/char/9555-oa \
	./basic_ostream/inserters_arithmetic/char/4402 \
	./basic_ostream/inserters_arithmetic/char/1 \
	./basic_ostream/inserters_arithmetic/char/4 \
	./basic_ostream/inserters_arithmetic/char/exceptions_failbit_throw \
	./basic_ostream/inserters_arithmetic/char/exceptions_badbit_throw \
	./basic_ostream/inserters_arithmetic/char/3 \
	./basic_ostream/inserters_arithmetic/char/31031 \
	./basic_ostream/inserters_arithmetic/char/6 \
	./basic_ostream/inserters_arithmetic/char/7 \
	./basic_ostream/inserters_arithmetic/wchar_t/2 \
	./basic_ostream/inserters_arithmetic/wchar_t/5 \
	./basic_ostream/inserters_arithmetic/wchar_t/9555-oa \
	./basic_ostream/inserters_arithmetic/wchar_t/4402 \
	./basic_ostream/inserters_arithmetic/wchar_t/1 \
	./basic_ostream/inserters_arithmetic/wchar_t/4 \
	./basic_ostream/inserters_arithmetic/wchar_t/exceptions_failbit_throw \
	./basic_ostream/inserters_arithmetic/wchar_t/exceptions_badbit_throw \
	./basic_ostream/inserters_arithmetic/wchar_t/3 \
	./basic_ostream/inserters_arithmetic/wchar_t/31031 \
	./basic_ostream/inserters_arithmetic/wchar_t/6 \
	./basic_ostream/inserters_arithmetic/wchar_t/7 \
	./basic_stringbuf/seekoff/char/2 \
	./basic_stringbuf/seekoff/char/1 \
	./basic_stringbuf/seekoff/char/16956 \
	./basic_stringbuf/seekoff/char/10975 \
	./basic_stringbuf/seekoff/wchar_t/2 \
	./basic_stringbuf/seekoff/wchar_t/1 \
	./basic_stringbuf/seekoff/wchar_t/16956 \
	./basic_stringbuf/seekoff/wchar_t/10975 \
	./basic_stringbuf/sputbackc/char/9425 \
	./basic_stringbuf/sputbackc/char/1 \
	./basic_stringbuf/sputbackc/wchar_t/9425 \
	./basic_stringbuf/sputbackc/wchar_t/1 \
	./basic_stringbuf/sputc/char/1057 \
	./basic_stringbuf/sputc/char/1 \
	./basic_stringbuf/sputc/char/9404-1 \
	./basic_stringbuf/sputc/wchar_t/1057 \
	./basic_stringbuf/sputc/wchar_t/1 \
	./basic_stringbuf/sputc/wchar_t/9404-1 \
	./basic_stringbuf/in_avail/char/1 \
	./basic_stringbuf/in_avail/char/21955 \
	./basic_stringbuf/in_avail/wchar_t/1 \
	./basic_stringbuf/snextc/char/1 \
	./basic_stringbuf/snextc/wchar_t/1 \
	./basic_stringbuf/sputn/char/9404-2 \
	./basic_stringbuf/sputn/char/1057 \
	./basic_stringbuf/sputn/char/1 \
	./basic_stringbuf/sputn/wchar_t/9404-2 \
	./basic_stringbuf/sputn/wchar_t/1057 \
	./basic_stringbuf/sputn/wchar_t/1 \
	./basic_stringbuf/sync/char/1057 \
	./basic_stringbuf/sync/wchar_t/1057 \
	./basic_stringbuf/cons/2020 \
	./basic_stringbuf/cons/char/1 \
	./basic_stringbuf/cons/wchar_t/1 \
	./basic_stringbuf/sungetc/char/1 \
	./basic_stringbuf/sungetc/wchar_t/1 \
	./basic_stringbuf/seekpos/char/2 \
	./basic_stringbuf/seekpos/char/1 \
	./basic_stringbuf/seekpos/char/29354 \
	./basic_stringbuf/seekpos/char/3 \
	./basic_stringbuf/seekpos/wchar_t/2 \
	./basic_stringbuf/seekpos/wchar_t/1 \
	./basic_stringbuf/seekpos/wchar_t/29354 \
	./basic_stringbuf/seekpos/wchar_t/3 \
	./basic_stringbuf/setbuf/char/2 \
	./basic_stringbuf/setbuf/char/1 \
	./basic_stringbuf/setbuf/char/4 \
	./basic_stringbuf/setbuf/char/3 \
	./basic_stringbuf/setbuf/wchar_t/2 \
	./basic_stringbuf/setbuf/wchar_t/1 \
	./basic_stringbuf/setbuf/wchar_t/4 \
	./basic_stringbuf/setbuf/wchar_t/3 \
	./basic_stringbuf/sgetn/char/1 \
	./basic_stringbuf/sgetn/wchar_t/1 \
	./basic_stringbuf/overflow/char/2 \
	./basic_stringbuf/overflow/char/3599 \
	./basic_stringbuf/overflow/char/1 \
	./basic_stringbuf/overflow/char/26250 \
	./basic_stringbuf/overflow/char/9988 \
	./basic_stringbuf/overflow/wchar_t/2 \
	./basic_stringbuf/overflow/wchar_t/3599 \
	./basic_stringbuf/overflow/wchar_t/1 \
	./basic_stringbuf/overflow/wchar_t/26250 \
	./basic_stringbuf/overflow/wchar_t/9988 \
	./basic_stringbuf/imbue/char/1 \
	./basic_stringbuf/imbue/char/9322 \
	./basic_stringbuf/imbue/wchar_t/1 \
	./basic_stringbuf/imbue/wchar_t/9322 \
	./basic_stringbuf/pbackfail/char/2 \
	./basic_stringbuf/pbackfail/char/1 \
	./basic_stringbuf/pbackfail/wchar_t/2 \
	./basic_stringbuf/pbackfail/wchar_t/1 \
	./basic_stringbuf/str/char/2 \
	./basic_stringbuf/str/char/3955 \
	./basic_stringbuf/str/char/1 \
	./basic_stringbuf/str/char/3 \
	./basic_stringbuf/str/wchar_t/2 \
	./basic_stringbuf/str/wchar_t/3955 \
	./basic_stringbuf/str/wchar_t/1 \
	./basic_stringbuf/str/wchar_t/3 \
	./basic_stringbuf/sbumpc/char/9825 \
	./basic_stringbuf/sbumpc/char/1 \
	./basic_stringbuf/sbumpc/wchar_t/9825 \
	./basic_stringbuf/sbumpc/wchar_t/1 \
	./basic_stringbuf/sgetc/char/1 \
	./basic_stringbuf/sgetc/wchar_t/1 \
	./basic_ifstream/rdbuf/char/2832 \
	./basic_ifstream/cons/2020 \
	./basic_istringstream/rdbuf/char/2832 \
	./basic_istringstream/rdbuf/wchar_t/2832 \
	./basic_istringstream/cons/2020 \
	./basic_istringstream/str/char/1 \
	./basic_istringstream/str/wchar_t/1 \
	./basic_ios/locales/char/1 \
	./basic_ios/copyfmt/char/2 \
	./basic_ios/copyfmt/char/1 \
	./basic_ios/exceptions/char/2 \
	./basic_ios/exceptions/char/1 \
	./basic_ios/clear/char/1 \
	./basic_ios/cons/2020 \
	./basic_ios/cons/char/2 \
	./basic_ios/cons/char/1 \
	./basic_ios/cons/char/3 \
	./basic_ios/imbue/14072 \
	./rvalue_streams-2 \
	./basic_streambuf/sputbackc/char/9538 \
	./basic_streambuf/sputbackc/wchar_t/9538 \
	./basic_streambuf/sputc/char/1057 \
	./basic_streambuf/sputc/wchar_t/1057 \
	./basic_streambuf/in_avail/char/1 \
	./basic_streambuf/in_avail/wchar_t/1 \
	./basic_streambuf/sputn/char/1057 \
	./basic_streambuf/sputn/char/1 \
	./basic_streambuf/sputn/wchar_t/1057 \
	./basic_streambuf/sputn/wchar_t/1 \
	./basic_streambuf/sync/char/1057 \
	./basic_streambuf/sync/wchar_t/1057 \
	./basic_streambuf/cons/2020 \
	./basic_streambuf/cons/char/1 \
	./basic_streambuf/cons/wchar_t/1 \
	./basic_streambuf/sgetn/char/1 \
	./basic_streambuf/sgetn/wchar_t/1 \
	./basic_streambuf/overflow/char/2 \
	./basic_streambuf/overflow/char/3599 \
	./basic_streambuf/overflow/char/1 \
	./basic_streambuf/overflow/wchar_t/2 \
	./basic_streambuf/overflow/wchar_t/3599 \
	./basic_streambuf/overflow/wchar_t/1 \
	./basic_streambuf/imbue/char/13007-2 \
	./basic_streambuf/imbue/char/13007-1 \
	./basic_streambuf/imbue/char/1 \
	./basic_streambuf/imbue/char/9322 \
	./basic_streambuf/imbue/wchar_t/13007-2 \
	./basic_streambuf/imbue/wchar_t/13007-1 \
	./basic_streambuf/imbue/wchar_t/1 \
	./basic_streambuf/imbue/wchar_t/9322 \
	./basic_streambuf/sgetc/char/1 \
	./basic_streambuf/sgetc/wchar_t/1 \
	./types/2 \
	./types/1 \
	./types/3 \
	./manipulators/adjustfield/char/2 \
	./manipulators/adjustfield/char/1 \
	./manipulators/adjustfield/wchar_t/2 \
	./manipulators/adjustfield/wchar_t/1 \
	./manipulators/extended/put_money/char/51288 \
	./manipulators/extended/put_money/char/1 \
	./manipulators/extended/put_money/wchar_t/51288 \
	./manipulators/extended/put_money/wchar_t/1 \
	./manipulators/extended/get_money/char/51288 \
	./manipulators/extended/get_money/char/1 \
	./manipulators/extended/get_money/wchar_t/51288 \
	./manipulators/extended/get_money/wchar_t/1 \
	./manipulators/basefield/char/1 \
	./manipulators/basefield/wchar_t/1 \
	./manipulators/standard/char/2 \
	./manipulators/standard/char/1 \
	./manipulators/standard/wchar_t/2 \
	./manipulators/standard/wchar_t/1 \
	"
EXECS_26_NUMER="\
	./adjacent_difference/1 \
	./inner_product/1 \
	./headers/cstdlib/13943 \
	./headers/cstdlib/2190 \
	./headers/cmath/overloads \
	./headers/cmath/51083 \
	./headers/cmath/fabs_inline \
	./headers/cmath/c_math \
	./headers/cmath/powi \
	./headers/cmath/dr550 \
	./headers/cmath/c_math_dynamic \
	./headers/cmath/19322 \
	./headers/cmath/c99_classification_macros_c++ \
	./partial_sum/1 \
	./valarray/operators \
	./valarray/binary_closure \
	./valarray/dr630-1 \
	./valarray/30416 \
	./valarray/const_bracket \
	./valarray/algo \
	./valarray/init-list \
	./valarray/moveable \
	./valarray/27867 \
	./valarray/40691 \
	./valarray/dr543 \
	./valarray/subset_assignment \
	./valarray/33084 \
	./valarray/dr630-2 \
	./valarray/28277 \
	./valarray/swap \
	./slice_array/array_assignment \
	./iota/1 \
	./complex/51083 \
	./complex/inserters_extractors/char/1 \
	./complex/inserters_extractors/wchar_t/1 \
	./complex/requirements/constexpr_functions \
	./complex/c99 \
	./complex/value_operations/dr387 \
	./complex/value_operations/constexpr \
	./complex/value_operations/1 \
	./complex/comparison_operators/constexpr \
	./complex/buggy_complex \
	./complex/cons/constexpr \
	./complex/cons/48760 \
	./complex/pow \
	./complex/13450 \
	./complex/dr781_dr1137 \
	./complex/dr844 \
	./complex/50880 \
	./random/extreme_value_distribution/operators/inequal \
	./random/extreme_value_distribution/operators/equal \
	./random/extreme_value_distribution/operators/serialize \
	./random/extreme_value_distribution/cons/parms \
	./random/extreme_value_distribution/cons/default \
	./random/ranlux24 \
	./random/gamma_distribution/operators/inequal \
	./random/gamma_distribution/operators/equal \
	./random/gamma_distribution/operators/serialize \
	./random/gamma_distribution/cons/parms \
	./random/gamma_distribution/cons/default \
	./random/ranlux48 \
	./random/cauchy_distribution/operators/inequal \
	./random/cauchy_distribution/operators/equal \
	./random/cauchy_distribution/operators/serialize \
	./random/cauchy_distribution/cons/parms \
	./random/cauchy_distribution/cons/default \
	./random/uniform_real_distribution/operators/inequal \
	./random/uniform_real_distribution/operators/equal \
	./random/uniform_real_distribution/operators/serialize \
	./random/uniform_real_distribution/cons/parms \
	./random/uniform_real_distribution/cons/default \
	./random/exponential_distribution/operators/inequal \
	./random/exponential_distribution/operators/equal \
	./random/exponential_distribution/operators/serialize \
	./random/exponential_distribution/cons/parms \
	./random/exponential_distribution/cons/default \
	./random/poisson_distribution/operators/inequal \
	./random/poisson_distribution/operators/values \
	./random/poisson_distribution/operators/equal \
	./random/poisson_distribution/operators/serialize \
	./random/poisson_distribution/cons/parms \
	./random/poisson_distribution/cons/default \
	./random/minstd_rand \
	./random/mt19937_64 \
	./random/subtract_with_carry_engine/requirements/constexpr_data \
	./random/subtract_with_carry_engine/requirements/constexpr_functions \
	./random/subtract_with_carry_engine/requirements/constants \
	./random/subtract_with_carry_engine/operators/inequal \
	./random/subtract_with_carry_engine/operators/equal \
	./random/subtract_with_carry_engine/operators/serialize \
	./random/subtract_with_carry_engine/cons/seed1 \
	./random/subtract_with_carry_engine/cons/55215 \
	./random/subtract_with_carry_engine/cons/seed_seq \
	./random/subtract_with_carry_engine/cons/seed2 \
	./random/subtract_with_carry_engine/cons/default \
	./random/subtract_with_carry_engine/cons/copy \
	./random/discrete_distribution/operators/inequal \
	./random/discrete_distribution/operators/call-default \
	./random/discrete_distribution/operators/values \
	./random/discrete_distribution/operators/equal \
	./random/discrete_distribution/operators/serialize \
	./random/discrete_distribution/cons/range \
	./random/discrete_distribution/cons/num_xbound_fun \
	./random/discrete_distribution/cons/default \
	./random/discrete_distribution/cons/initlist \
	./random/lognormal_distribution/operators/inequal \
	./random/lognormal_distribution/operators/equal \
	./random/lognormal_distribution/operators/serialize \
	./random/lognormal_distribution/cons/parms \
	./random/lognormal_distribution/cons/default \
	./random/negative_binomial_distribution/operators/inequal \
	./random/negative_binomial_distribution/operators/values \
	./random/negative_binomial_distribution/operators/equal \
	./random/negative_binomial_distribution/operators/serialize \
	./random/negative_binomial_distribution/cons/parms \
	./random/negative_binomial_distribution/cons/default \
	./random/minstd_rand0 \
	./random/linear_congruential_engine/requirements/constexpr_data \
	./random/linear_congruential_engine/requirements/constexpr_functions \
	./random/linear_congruential_engine/requirements/constants \
	./random/linear_congruential_engine/operators/inequal \
	./random/linear_congruential_engine/operators/equal \
	./random/linear_congruential_engine/operators/serialize \
	./random/linear_congruential_engine/operators/51795 \
	./random/linear_congruential_engine/cons/seed1 \
	./random/linear_congruential_engine/cons/55215 \
	./random/linear_congruential_engine/cons/seed_seq \
	./random/linear_congruential_engine/cons/seed2 \
	./random/linear_congruential_engine/cons/default \
	./random/linear_congruential_engine/cons/copy \
	./random/seed_seq/cons/range \
	./random/seed_seq/cons/default \
	./random/seed_seq/cons/initlist \
	./random/mt19937 \
	./random/shuffle_order_engine/requirements/constexpr_data \
	./random/shuffle_order_engine/requirements/constexpr_functions \
	./random/shuffle_order_engine/requirements/constants \
	./random/shuffle_order_engine/operators/inequal \
	./random/shuffle_order_engine/operators/equal \
	./random/shuffle_order_engine/operators/serialize \
	./random/shuffle_order_engine/cons/base_copy \
	./random/shuffle_order_engine/cons/seed1 \
	./random/shuffle_order_engine/cons/55215 \
	./random/shuffle_order_engine/cons/seed_seq \
	./random/shuffle_order_engine/cons/seed2 \
	./random/shuffle_order_engine/cons/default \
	./random/shuffle_order_engine/cons/base_move \
	./random/shuffle_order_engine/cons/copy \
	./random/normal_distribution/operators/inequal \
	./random/normal_distribution/operators/equal \
	./random/normal_distribution/operators/serialize \
	./random/normal_distribution/cons/parms \
	./random/normal_distribution/cons/default \
	./random/bernoulli_distribution/operators/inequal \
	./random/bernoulli_distribution/operators/values \
	./random/bernoulli_distribution/operators/equal \
	./random/bernoulli_distribution/operators/serialize \
	./random/bernoulli_distribution/cons/parms \
	./random/bernoulli_distribution/cons/default \
	./random/student_t_distribution/operators/inequal \
	./random/student_t_distribution/operators/equal \
	./random/student_t_distribution/operators/serialize \
	./random/student_t_distribution/cons/parms \
	./random/student_t_distribution/cons/default \
	./random/binomial_distribution/operators/inequal \
	./random/binomial_distribution/operators/values \
	./random/binomial_distribution/operators/equal \
	./random/binomial_distribution/operators/serialize \
	./random/binomial_distribution/cons/parms \
	./random/binomial_distribution/cons/default \
	./random/uniform_int_distribution/operators/inequal \
	./random/uniform_int_distribution/operators/values \
	./random/uniform_int_distribution/operators/equal \
	./random/uniform_int_distribution/operators/serialize \
	./random/uniform_int_distribution/cons/parms \
	./random/uniform_int_distribution/cons/default \
	./random/fisher_f_distribution/operators/inequal \
	./random/fisher_f_distribution/operators/equal \
	./random/fisher_f_distribution/operators/serialize \
	./random/fisher_f_distribution/cons/parms \
	./random/fisher_f_distribution/cons/default \
	./random/ranlux48_base \
	./random/discard_block_engine/requirements/constexpr_data \
	./random/discard_block_engine/requirements/constexpr_functions \
	./random/discard_block_engine/operators/inequal \
	./random/discard_block_engine/operators/equal \
	./random/discard_block_engine/operators/serialize \
	./random/discard_block_engine/cons/base_copy \
	./random/discard_block_engine/cons/seed1 \
	./random/discard_block_engine/cons/55215 \
	./random/discard_block_engine/cons/seed_seq \
	./random/discard_block_engine/cons/seed2 \
	./random/discard_block_engine/cons/default \
	./random/discard_block_engine/cons/base_move \
	./random/discard_block_engine/cons/copy \
	./random/random_device/cons/token \
	./random/random_device/cons/default \
	./random/default_random_engine \
	./random/weibull_distribution/operators/inequal \
	./random/weibull_distribution/operators/equal \
	./random/weibull_distribution/operators/serialize \
	./random/weibull_distribution/cons/parms \
	./random/weibull_distribution/cons/default \
	./random/geometric_distribution/operators/inequal \
	./random/geometric_distribution/operators/values \
	./random/geometric_distribution/operators/equal \
	./random/geometric_distribution/operators/serialize \
	./random/geometric_distribution/cons/parms \
	./random/geometric_distribution/cons/default \
	./random/ranlux24_base \
	./random/chi_squared_distribution/operators/inequal \
	./random/chi_squared_distribution/operators/equal \
	./random/chi_squared_distribution/operators/serialize \
	./random/chi_squared_distribution/cons/parms \
	./random/chi_squared_distribution/cons/default \
	./random/piecewise_linear_distribution/operators/inequal \
	./random/piecewise_linear_distribution/operators/call-default \
	./random/piecewise_linear_distribution/operators/equal \
	./random/piecewise_linear_distribution/operators/serialize \
	./random/piecewise_linear_distribution/cons/range \
	./random/piecewise_linear_distribution/cons/num_xbound_fun \
	./random/piecewise_linear_distribution/cons/default \
	./random/piecewise_linear_distribution/cons/initlist_fun \
	./random/knuth_b \
	./random/piecewise_constant_distribution/operators/inequal \
	./random/piecewise_constant_distribution/operators/call-default \
	./random/piecewise_constant_distribution/operators/equal \
	./random/piecewise_constant_distribution/operators/serialize \
	./random/piecewise_constant_distribution/cons/range \
	./random/piecewise_constant_distribution/cons/num_xbound_fun \
	./random/piecewise_constant_distribution/cons/default \
	./random/piecewise_constant_distribution/cons/initlist_fun \
	./random/independent_bits_engine/requirements/constexpr_functions \
	./random/independent_bits_engine/operators/inequal \
	./random/independent_bits_engine/operators/equal \
	./random/independent_bits_engine/operators/serialize \
	./random/independent_bits_engine/cons/base_copy \
	./random/independent_bits_engine/cons/seed1 \
	./random/independent_bits_engine/cons/55215 \
	./random/independent_bits_engine/cons/seed_seq \
	./random/independent_bits_engine/cons/seed2 \
	./random/independent_bits_engine/cons/default \
	./random/independent_bits_engine/cons/base_move \
	./random/independent_bits_engine/cons/copy \
	./random/mersenne_twister_engine/requirements/constexpr_data \
	./random/mersenne_twister_engine/requirements/constexpr_functions \
	./random/mersenne_twister_engine/requirements/constants \
	./random/mersenne_twister_engine/operators/inequal \
	./random/mersenne_twister_engine/operators/equal \
	./random/mersenne_twister_engine/operators/serialize \
	./random/mersenne_twister_engine/cons/seed1 \
	./random/mersenne_twister_engine/cons/55215 \
	./random/mersenne_twister_engine/cons/seed_seq \
	./random/mersenne_twister_engine/cons/seed2 \
	./random/mersenne_twister_engine/cons/default \
	./random/mersenne_twister_engine/cons/copy \
	./slice/1 \
	./accumulate/1 \
	./accumulate/48750 \
	"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./copy/streambuf_iterators/char/4 \
#	./copy/streambuf_iterators/wchar_t/4 \
#	./find/istreambuf_iterators/char/2 \
#	./find/istreambuf_iterators/wchar_t/2 \

EXECS_25_ALGO="\
	./replace_copy/1 \
	./min_element/1 \
	./next_permutation/moveable \
	./next_permutation/1 \
	./adjacent_find/1 \
	./lower_bound/2 \
	./lower_bound/1 \
	./copy/2 \
	./copy/streambuf_iterators/char/2 \
	./copy/streambuf_iterators/char/1 \
	./copy/streambuf_iterators/char/3 \
	./copy/streambuf_iterators/wchar_t/2 \
	./copy/streambuf_iterators/wchar_t/1 \
	./copy/streambuf_iterators/wchar_t/3 \
	./copy/34595 \
	./copy/deque_iterators/1 \
	./copy/1 \
	./copy/4 \
	./copy/3 \
	./copy/move_iterators/1 \
	./partition_point/1 \
	./find_first_of/1 \
	./find_first_of/concept_check_1 \
	./remove_if/moveable \
	./remove_if/1 \
	./fill_n/1 \
	./mismatch/1 \
	./nth_element/2 \
	./nth_element/58800 \
	./nth_element/moveable \
	./nth_element/1 \
	./nth_element/3 \
	./search/1 \
	./rotate/moveable2 \
	./rotate/moveable \
	./rotate/1 \
	./rotate/rotate \
	./is_heap/1 \
	./min/2 \
	./min/1 \
	./min/4 \
	./min/37547 \
	./min/3 \
	./for_each/1 \
	./upper_bound/2 \
	./upper_bound/1 \
	./fill/2 \
	./fill/1 \
	./fill/4 \
	./fill/3 \
	./equal/no_operator_ne \
	./equal/1 \
	./copy_n/2 \
	./copy_n/50119 \
	./copy_n/1 \
	./copy_n/4 \
	./copy_n/3 \
	./copy_n/move_iterators/1 \
	./remove/moveable \
	./remove/1 \
	./heap/moveable2 \
	./heap/moveable \
	./heap/1 \
	./shuffle/1 \
	./max/2 \
	./max/1 \
	./max/4 \
	./max/37547 \
	./max/3 \
	./random_shuffle/59603 \
	./random_shuffle/moveable \
	./random_shuffle/1 \
	./partition_copy/1 \
	./stable_sort/2 \
	./stable_sort/mem_check \
	./stable_sort/49559 \
	./stable_sort/moveable2 \
	./stable_sort/moveable \
	./stable_sort/1 \
	./stable_sort/3 \
	./stable_sort/check_compare_by_value \
	./search_n/58358 \
	./search_n/iterator \
	./partial_sort_copy/2 \
	./partial_sort_copy/1 \
	./move_backward/deque_iterators/1 \
	./move_backward/1 \
	./binary_search/2 \
	./binary_search/1 \
	./prev_permutation/moveable \
	./prev_permutation/1 \
	./stable_partition/mem_check \
	./stable_partition/moveable \
	./stable_partition/1 \
	./stable_partition/pr52822 \
	./set_intersection/1 \
	./copy_backward/deque_iterators/1 \
	./copy_backward/move_iterators/1 \
	./find_if/1 \
	./merge/1 \
	./lexicographical_compare/1 \
	./minmax_element/1 \
	./inplace_merge/49559 \
	./inplace_merge/moveable2 \
	./inplace_merge/moveable \
	./inplace_merge/1 \
	./unique_copy/2 \
	./unique_copy/26133 \
	./unique_copy/1 \
	./is_heap_until/1 \
	./all_of/1 \
	./set_union/1 \
	./replace_copy_if/1 \
	./set_symmetric_difference/1 \
	./is_partitioned/1 \
	./count/1 \
	./sort_heap/check_compare_by_value \
	./set_difference/1 \
	./swap_ranges/1 \
	./replace/1 \
	./find_if_not/1 \
	./partition/moveable \
	./partition/1 \
	./is_sorted_until/1 \
	./none_of/1 \
	./is_sorted/1 \
	./minmax/2 \
	./minmax/1 \
	./minmax/37547 \
	./minmax/3 \
	./partial_sort/2 \
	./partial_sort/moveable \
	./partial_sort/1 \
	./partial_sort/check_compare_by_value \
	./replace_if/1 \
	./includes/1 \
	./move/deque_iterators/1 \
	./move/1 \
	./unique/2 \
	./unique/moveable \
	./unique/1 \
	./unique/11480 \
	./is_permutation/1 \
	./equal_range/2 \
	./equal_range/1 \
	./max_element/1 \
	./find_end/1 \
	./sort/34095 \
	./sort/moveable \
	./sort/34636 \
	./sort/1 \
	./sort/check_compare_by_value \
	./sort/35588 \
	./sort/vectorbool \
	./any_of/1 \
	./iter_swap/20577 \
	./count_if/1 \
	./find/17441 \
	./find/1 \
	./find/istreambuf_iterators/char/1 \
	./find/istreambuf_iterators/wchar_t/1 \
	./find/39546 \
	./pop_heap/57010 \
	"
EXECS_24_ITER="\
	./move_iterator/greedy_ops \
	./front_insert_iterator/2 \
	./ostreambuf_iterator/2 \
	./normal_iterator/greedy_ops \
	./operations/prev \
	./operations/next \
	./back_insert_iterator/2 \
	./istreambuf_iterator/2 \
	./istreambuf_iterator/cons/constexpr \
	./istreambuf_iterator/2627 \
	./insert_iterator/2 \
	./random_access_iterator/26020 \
	./random_access_iterator/string_vector_iterators \
	./istream_iterator/2 \
	./istream_iterator/cons/constexpr \
	./reverse_iterator/2 \
	./reverse_iterator/greedy_ops \
	./reverse_iterator/11729 \
	./reverse_iterator/3 \
	"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./deque/debug/shrink_to_fit \

EXECS_23_CONTA="\
	./unordered_set/insert/hash_policy \
	./unordered_set/insert/24061-set \
	./unordered_set/insert/move_range \
	./unordered_set/insert/set_single_move \
	./unordered_set/insert/set_range \
	./unordered_set/insert/set_single \
	./unordered_set/erase/24061-set \
	./unordered_set/erase/1 \
	./unordered_set/requirements/exception/basic \
	./unordered_set/requirements/exception/generation_prohibited \
	./unordered_set/requirements/exception/propagation_consistent \
	./unordered_set/requirements/cliterators \
	./unordered_set/requirements/citerators \
	./unordered_set/requirements/debug_mode \
	./unordered_set/buckets/swap \
	./unordered_set/init-list \
	./unordered_set/operators/1 \
	./unordered_set/modifiers/reserve \
	./unordered_set/modifiers/emplace \
	./unordered_set/operations/count \
	./unordered_set/cons/moveable \
	./unordered_set/hash_policy/dr1189 \
	./unordered_set/hash_policy/rehash \
	./unordered_set/hash_policy/26132 \
	./unordered_set/hash_policy/load_factor \
	./unordered_set/max_load_factor/robustness \
	./unordered_set/56267 \
	./bitset/to_string/1 \
	./bitset/to_string/dr396 \
	./bitset/test/1 \
	./bitset/to_ullong/1 \
	./bitset/requirements/constexpr_functions \
	./bitset/debug/invalidation/1 \
	./bitset/input/1 \
	./bitset/all/1 \
	./bitset/ext/15361 \
	./bitset/operations/2 \
	./bitset/operations/constexpr-2 \
	./bitset/operations/constexpr \
	./bitset/operations/1 \
	./bitset/operations/13838 \
	./bitset/cons/2 \
	./bitset/cons/50268 \
	./bitset/cons/constexpr \
	./bitset/cons/16020 \
	./bitset/cons/6282 \
	./bitset/cons/1 \
	./bitset/cons/dr1325-2 \
	./bitset/cons/3 \
	./bitset/cons/dr396 \
	./bitset/to_ulong/1 \
	./bitset/count/6124 \
	./bitset/hash/1 \
	./forward_list/requirements/exception/basic \
	./forward_list/requirements/exception/generation_prohibited \
	./forward_list/requirements/exception/propagation_consistent \
	./forward_list/requirements/citerators \
	./forward_list/comparable \
	./forward_list/debug/move_constructor \
	./forward_list/debug/clear \
	./forward_list/debug/splice_after \
	./forward_list/debug/swap \
	./forward_list/capacity/resize_size \
	./forward_list/capacity/1 \
	./forward_list/modifiers/2 \
	./forward_list/modifiers/5 \
	./forward_list/modifiers/1 \
	./forward_list/modifiers/4 \
	./forward_list/modifiers/3 \
	./forward_list/modifiers/6 \
	./forward_list/operations/2 \
	./forward_list/operations/5 \
	./forward_list/operations/remove_freed \
	./forward_list/operations/1 \
	./forward_list/operations/4 \
	./forward_list/operations/3 \
	./forward_list/operations/6 \
	./forward_list/operations/7 \
	./forward_list/cons/2 \
	./forward_list/cons/5 \
	./forward_list/cons/10 \
	./forward_list/cons/12 \
	./forward_list/cons/moveable \
	./forward_list/cons/1 \
	./forward_list/cons/4 \
	./forward_list/cons/cons_size \
	./forward_list/cons/3 \
	./forward_list/cons/8 \
	./forward_list/cons/6 \
	./forward_list/cons/7 \
	./forward_list/cons/9 \
	./forward_list/cons/11 \
	./forward_list/cons/13 \
	./forward_list/allocator/minimal \
	./forward_list/allocator/noexcept \
	./forward_list/allocator/copy_assign \
	./forward_list/allocator/move_assign \
	./forward_list/allocator/copy \
	./forward_list/allocator/swap \
	./stack/members/7158 \
	./priority_queue/moveable \
	./priority_queue/members/7161 \
	./queue/moveable \
	./queue/members/7157 \
	./map/56613 \
	./map/requirements/exception/basic \
	./map/requirements/exception/generation_prohibited \
	./map/requirements/exception/propagation_consistent \
	./map/requirements/citerators \
	./map/debug/invalidation/2 \
	./map/debug/invalidation/1 \
	./map/capacity/29134 \
	./map/init-list \
	./map/operators/2 \
	./map/operators/1 \
	./map/modifiers/insert/16813 \
	./map/modifiers/insert/2 \
	./map/modifiers/insert/5 \
	./map/modifiers/insert/1 \
	./map/modifiers/insert/4 \
	./map/modifiers/insert/3 \
	./map/modifiers/swap/2 \
	./map/modifiers/swap/1 \
	./map/modifiers/swap/3 \
	./map/modifiers/emplace/1 \
	./map/modifiers/dr130 \
	./map/element_access/2 \
	./map/element_access/46148 \
	./map/element_access/1 \
	./map/element_access/39901 \
	./map/operations/count \
	./map/operations/1 \
	./map/cons/moveable \
	./map/14340 \
	./map/pthread6 \
	./array/tuple_interface/tuple_element \
	./array/tuple_interface/tuple_size \
	./array/requirements/fill \
	./array/requirements/citerators \
	./array/requirements/zero_sized_arrays \
	./array/requirements/contiguous \
	./array/requirements/member_swap \
	./array/capacity/max_size \
	./array/capacity/empty \
	./array/capacity/constexpr_functions \
	./array/capacity/size \
	./array/element_access/constexpr_element_access \
	./array/element_access/data \
	./array/element_access/front \
	./array/element_access/back \
	./array/element_access/54388 \
	./array/element_access/at_out_of_range \
	./array/specialized_algorithms/swap \
	./array/comparison_operators/less_or_equal \
	./array/comparison_operators/less \
	./array/comparison_operators/equal \
	./array/comparison_operators/not_equal \
	./array/comparison_operators/greater_or_equal \
	./array/comparison_operators/greater \
	./array/cons/aggregate_initialization \
	./array/iterators/end_is_one_past \
	./multiset/requirements/exception/basic \
	./multiset/requirements/exception/generation_prohibited \
	./multiset/requirements/exception/propagation_consistent \
	./multiset/requirements/citerators \
	./multiset/debug/invalidation/2 \
	./multiset/debug/invalidation/1 \
	./multiset/capacity/29134 \
	./multiset/init-list \
	./multiset/modifiers/insert/2 \
	./multiset/modifiers/insert/1 \
	./multiset/modifiers/insert/4 \
	./multiset/modifiers/insert/3 \
	./multiset/modifiers/insert/22102 \
	./multiset/modifiers/swap/2 \
	./multiset/modifiers/swap/1 \
	./multiset/modifiers/swap/3 \
	./multiset/modifiers/emplace/1 \
	./multiset/modifiers/dr130 \
	./multiset/operations/count \
	./multiset/operations/1 \
	./multiset/cons/moveable \
	./multiset/14340 \
	./vector/bool/clear_allocator \
	./vector/bool/requirements/citerators \
	./vector/bool/6886 \
	./vector/bool/capacity/shrink_to_fit \
	./vector/bool/capacity/29134 \
	./vector/bool/capacity/1 \
	./vector/bool/modifiers/insert/1 \
	./vector/bool/modifiers/insert/31370 \
	./vector/bool/modifiers/erase/1 \
	./vector/bool/modifiers/swap/2 \
	./vector/bool/modifiers/swap/1 \
	./vector/bool/1 \
	./vector/bool/23632 \
	./vector/bool/cons/2 \
	./vector/bool/cons/1 \
	./vector/bool/hash/1 \
	./vector/bool/swap \
	./vector/data_access/1 \
	./vector/requirements/exception/basic \
	./vector/requirements/exception/generation_prohibited \
	./vector/requirements/exception/propagation_consistent \
	./vector/requirements/citerators \
	./vector/profile/vector \
	./vector/debug/bool/shrink_to_fit \
	./vector/debug/invalidation/2 \
	./vector/debug/invalidation/1 \
	./vector/debug/invalidation/4 \
	./vector/debug/invalidation/3 \
	./vector/debug/shrink_to_fit \
	./vector/debug/multithreaded_swap \
	./vector/debug/alloc_prop \
	./vector/capacity/2 \
	./vector/capacity/shrink_to_fit \
	./vector/capacity/shrink_to_fit2 \
	./vector/capacity/29134 \
	./vector/capacity/1 \
	./vector/capacity/reserve/moveable2 \
	./vector/capacity/reserve/moveable \
	./vector/capacity/29134-2 \
	./vector/capacity/44190 \
	./vector/capacity/resize/resize_size \
	./vector/capacity/resize/moveable2 \
	./vector/capacity/resize/moveable \
	./vector/capacity/resize/1 \
	./vector/capacity/8230 \
	./vector/init-list \
	./vector/modifiers/insert/1 \
	./vector/modifiers/2 \
	./vector/modifiers/erase/50529 \
	./vector/modifiers/erase/moveable \
	./vector/modifiers/erase/1 \
	./vector/modifiers/push_back/49836 \
	./vector/modifiers/swap/2 \
	./vector/modifiers/swap/1 \
	./vector/modifiers/swap/3 \
	./vector/modifiers/moveable2 \
	./vector/modifiers/moveable \
	./vector/modifiers/1 \
	./vector/modifiers/emplace/52799 \
	./vector/element_access/1 \
	./vector/zero_sized_allocations \
	./vector/cons/2 \
	./vector/cons/clear_allocator \
	./vector/cons/6513 \
	./vector/cons/moveable \
	./vector/cons/1 \
	./vector/cons/4 \
	./vector/cons/cons_size \
	./vector/cons/3 \
	./vector/check_construct_destroy \
	./vector/debug_mode_requires_reallocation-2 \
	./vector/allocator/minimal \
	./vector/allocator/move \
	./vector/allocator/noexcept \
	./vector/allocator/copy_assign \
	./vector/allocator/move_assign \
	./vector/allocator/copy \
	./vector/allocator/swap \
	./vector/ext_pointer/data_access \
	./vector/ext_pointer/citerators \
	./vector/ext_pointer/resize \
	./vector/ext_pointer/modifiers/erase \
	./vector/ext_pointer/modifiers/element \
	./vector/ext_pointer/modifiers/insert \
	./vector/ext_pointer/types/2 \
	./vector/ext_pointer/types/1 \
	./vector/14340 \
	./vector/types/1 \
	./vector/debug_mode_requires_reallocation-1 \
	./vector/15523 \
	./deque/requirements/exception/basic \
	./deque/requirements/exception/generation_prohibited \
	./deque/requirements/exception/propagation_consistent \
	./deque/requirements/citerators \
	./deque/debug/invalidation/2 \
	./deque/debug/invalidation/1 \
	./deque/debug/invalidation/4 \
	./deque/debug/invalidation/3 \
	./deque/capacity/shrink_to_fit \
	./deque/capacity/resize_size \
	./deque/capacity/29134 \
	./deque/capacity/moveable \
	./deque/capacity/29134-2 \
	./deque/init-list \
	./deque/operators/2 \
	./deque/operators/1 \
	./deque/modifiers/erase/2 \
	./deque/modifiers/erase/50529 \
	./deque/modifiers/erase/moveable \
	./deque/modifiers/erase/1 \
	./deque/modifiers/erase/3 \
	./deque/modifiers/push_front/49836 \
	./deque/modifiers/push_back/49836 \
	./deque/modifiers/swap/2 \
	./deque/modifiers/swap/1 \
	./deque/modifiers/swap/3 \
	./deque/modifiers/moveable \
	./deque/modifiers/emplace/52799 \
	./deque/cons/2 \
	./deque/cons/clear_allocator \
	./deque/cons/moveable \
	./deque/cons/1 \
	./deque/cons/cons_size \
	./deque/cons/assign/1 \
	./deque/check_construct_destroy \
	./deque/14340 \
	./deque/types/1 \
	./multimap/requirements/exception/basic \
	./multimap/requirements/exception/generation_prohibited \
	./multimap/requirements/exception/propagation_consistent \
	./multimap/requirements/citerators \
	./multimap/debug/invalidation/2 \
	./multimap/debug/invalidation/1 \
	./multimap/capacity/29134 \
	./multimap/init-list \
	./multimap/modifiers/insert/2 \
	./multimap/modifiers/insert/1 \
	./multimap/modifiers/insert/4 \
	./multimap/modifiers/insert/3 \
	./multimap/modifiers/insert/22102 \
	./multimap/modifiers/swap/2 \
	./multimap/modifiers/swap/1 \
	./multimap/modifiers/swap/3 \
	./multimap/modifiers/emplace/1 \
	./multimap/modifiers/dr130 \
	./multimap/operations/count \
	./multimap/operations/1 \
	./multimap/cons/moveable \
	./multimap/14340 \
	./list/requirements/exception/basic \
	./list/requirements/exception/generation_prohibited \
	./list/requirements/exception/propagation_consistent \
	./list/requirements/citerators \
	./list/debug/invalidation/2 \
	./list/debug/invalidation/1 \
	./list/debug/invalidation/4 \
	./list/debug/invalidation/3 \
	./list/capacity/resize_size \
	./list/capacity/29134 \
	./list/capacity/1 \
	./list/modifiers/insert/25288 \
	./list/modifiers/2 \
	./list/modifiers/swap/2 \
	./list/modifiers/swap/1 \
	./list/modifiers/swap/3 \
	./list/modifiers/1 \
	./list/modifiers/3 \
	./list/modifiers/emplace/52799 \
	./list/operations/2 \
	./list/operations/5 \
	./list/operations/35969 \
	./list/operations/1 \
	./list/operations/4 \
	./list/operations/42352 \
	./list/operations/3 \
	./list/cons/2 \
	./list/cons/5 \
	./list/cons/clear_allocator \
	./list/cons/moveable \
	./list/cons/1 \
	./list/cons/4 \
	./list/cons/cons_size \
	./list/cons/3 \
	./list/cons/8 \
	./list/cons/6 \
	./list/cons/7 \
	./list/cons/9 \
	./list/check_construct_destroy \
	./list/14340 \
	./list/pthread1 \
	./list/pthread5 \
	./unordered_multimap/insert/24061-multimap \
	./unordered_multimap/insert/multimap_range \
	./unordered_multimap/insert/multimap_single_move-1 \
	./unordered_multimap/insert/multimap_single_move-2 \
	./unordered_multimap/insert/53115 \
	./unordered_multimap/insert/51866 \
	./unordered_multimap/insert/multimap_single \
	./unordered_multimap/insert/52476 \
	./unordered_multimap/insert/55028-debug \
	./unordered_multimap/erase/24061-multimap \
	./unordered_multimap/erase/2 \
	./unordered_multimap/erase/51845-multimap \
	./unordered_multimap/erase/54276 \
	./unordered_multimap/erase/1 \
	./unordered_multimap/requirements/exception/basic \
	./unordered_multimap/requirements/exception/generation_prohibited \
	./unordered_multimap/requirements/exception/propagation_consistent \
	./unordered_multimap/requirements/cliterators \
	./unordered_multimap/requirements/citerators \
	./unordered_multimap/init-list \
	./unordered_multimap/operators/2 \
	./unordered_multimap/operators/1 \
	./unordered_multimap/modifiers/reserve \
	./unordered_multimap/modifiers/emplace \
	./unordered_multimap/operations/count \
	./unordered_multimap/cons/moveable \
	./unordered_multimap/hash_policy/dr1189 \
	./unordered_map/insert/array_syntax_move \
	./unordered_map/insert/map_range \
	./unordered_map/insert/map_single_move-1 \
	./unordered_map/insert/24061-map \
	./unordered_map/insert/map_single \
	./unordered_map/insert/array_syntax \
	./unordered_map/insert/map_single_move-2 \
	./unordered_map/erase/54276 \
	./unordered_map/erase/1 \
	./unordered_map/erase/24061-map \
	./unordered_map/59548 \
	./unordered_map/requirements/exception/basic \
	./unordered_map/requirements/exception/generation_prohibited \
	./unordered_map/requirements/exception/propagation_consistent \
	./unordered_map/requirements/cliterators \
	./unordered_map/requirements/citerators \
	./unordered_map/requirements/debug_mode \
	./unordered_map/profile/hash_map \
	./unordered_map/profile/unordered \
	./unordered_map/init-list \
	./unordered_map/operators/2 \
	./unordered_map/operators/1 \
	./unordered_map/modifiers/reserve \
	./unordered_map/modifiers/emplace \
	./unordered_map/operations/count \
	./unordered_map/cons/moveable \
	./unordered_map/cons/56112 \
	./unordered_map/hash_policy/dr1189 \
	./unordered_map/dr761 \
	./set/requirements/exception/basic \
	./set/requirements/exception/generation_prohibited \
	./set/requirements/exception/propagation_consistent \
	./set/requirements/citerators \
	./set/debug/invalidation/2 \
	./set/debug/invalidation/1 \
	./set/capacity/29134 \
	./set/init-list \
	./set/modifiers/insert/2 \
	./set/modifiers/insert/1 \
	./set/modifiers/insert/3 \
	./set/modifiers/17948 \
	./set/modifiers/16728 \
	./set/modifiers/swap/2 \
	./set/modifiers/swap/1 \
	./set/modifiers/swap/3 \
	./set/modifiers/emplace/1 \
	./set/modifiers/dr130 \
	./set/operations/count \
	./set/operations/1 \
	./set/cons/moveable \
	./set/check_construct_destroy \
	./set/14340 \
	./unordered_multiset/insert/hash_policy \
	./unordered_multiset/insert/53115 \
	./unordered_multiset/insert/51866 \
	./unordered_multiset/insert/52476 \
	./unordered_multiset/insert/multiset_single \
	./unordered_multiset/insert/24061-multiset \
	./unordered_multiset/insert/multiset_single_move \
	./unordered_multiset/insert/multiset_range \
	./unordered_multiset/erase/2 \
	./unordered_multiset/erase/24061-multiset \
	./unordered_multiset/erase/1 \
	./unordered_multiset/requirements/exception/basic \
	./unordered_multiset/requirements/exception/generation_prohibited \
	./unordered_multiset/requirements/exception/propagation_consistent \
	./unordered_multiset/requirements/cliterators \
	./unordered_multiset/requirements/citerators \
	./unordered_multiset/init-list \
	./unordered_multiset/operators/1 \
	./unordered_multiset/modifiers/reserve \
	./unordered_multiset/modifiers/emplace \
	./unordered_multiset/operations/count \
	./unordered_multiset/cons/moveable \
	./unordered_multiset/cons/copy \
	./unordered_multiset/hash_policy/dr1189 \
	"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./ctype_byname/1 \
#	./moneypunct_byname/named_equivalence \
#	./numpunct/members/char/2 \
#	./numpunct/members/char/1 \
#	./numpunct/members/char/wrapped_locale \
#	./numpunct/members/char/wrapped_env \
#	./messages_byname/named_equivalence \
#	./messages/members/char/2 \
#	./messages/members/char/1 \
#	./messages/members/char/wrapped_locale \
#	./messages/members/char/wrapped_env \

EXECS_22_LOCALE="\
	./time_get/get_weekday/char/2 \
	./time_get/get_weekday/char/38081-1 \
	./time_get/get_weekday/char/5 \
	./time_get/get_weekday/char/38081-2 \
	./time_get/get_weekday/char/1 \
	./time_get/get_weekday/char/wrapped_locale \
	./time_get/get_weekday/char/3 \
	./time_get/get_weekday/char/wrapped_env \
	./time_get/get_weekday/char/6 \
	./time_get/get_weekday/wchar_t/2 \
	./time_get/get_weekday/wchar_t/5 \
	./time_get/get_weekday/wchar_t/1 \
	./time_get/get_weekday/wchar_t/wrapped_locale \
	./time_get/get_weekday/wchar_t/3 \
	./time_get/get_weekday/wchar_t/wrapped_env \
	./time_get/get_weekday/wchar_t/6 \
	./time_get/get_date/char/2 \
	./time_get/get_date/char/5 \
	./time_get/get_date/char/26701 \
	./time_get/get_date/char/12791 \
	./time_get/get_date/char/1 \
	./time_get/get_date/char/wrapped_locale \
	./time_get/get_date/char/12750 \
	./time_get/get_date/char/3 \
	./time_get/get_date/char/wrapped_env \
	./time_get/get_date/wchar_t/2 \
	./time_get/get_date/wchar_t/5 \
	./time_get/get_date/wchar_t/26701 \
	./time_get/get_date/wchar_t/12791 \
	./time_get/get_date/wchar_t/1 \
	./time_get/get_date/wchar_t/4 \
	./time_get/get_date/wchar_t/wrapped_locale \
	./time_get/get_date/wchar_t/12750 \
	./time_get/get_date/wchar_t/3 \
	./time_get/get_date/wchar_t/wrapped_env \
	./time_get/get_monthname/char/2 \
	./time_get/get_monthname/char/5 \
	./time_get/get_monthname/char/1 \
	./time_get/get_monthname/char/4 \
	./time_get/get_monthname/char/wrapped_locale \
	./time_get/get_monthname/char/3 \
	./time_get/get_monthname/char/wrapped_env \
	./time_get/get_monthname/char/6 \
	./time_get/get_monthname/wchar_t/2 \
	./time_get/get_monthname/wchar_t/5 \
	./time_get/get_monthname/wchar_t/1 \
	./time_get/get_monthname/wchar_t/4 \
	./time_get/get_monthname/wchar_t/wrapped_locale \
	./time_get/get_monthname/wchar_t/3 \
	./time_get/get_monthname/wchar_t/wrapped_env \
	./time_get/get_monthname/wchar_t/6 \
	./time_get/date_order/char/1 \
	./time_get/date_order/char/wrapped_locale \
	./time_get/date_order/char/wrapped_env \
	./time_get/date_order/wchar_t/1 \
	./time_get/date_order/wchar_t/wrapped_locale \
	./time_get/date_order/wchar_t/wrapped_env \
	./time_get/get_year/char/5 \
	./time_get/get_year/char/1 \
	./time_get/get_year/char/wrapped_locale \
	./time_get/get_year/char/3 \
	./time_get/get_year/char/wrapped_env \
	./time_get/get_year/wchar_t/5 \
	./time_get/get_year/wchar_t/1 \
	./time_get/get_year/wchar_t/wrapped_locale \
	./time_get/get_year/wchar_t/3 \
	./time_get/get_year/wchar_t/wrapped_env \
	./time_get/get_time/char/2 \
	./time_get/get_time/char/5 \
	./time_get/get_time/char/1 \
	./time_get/get_time/char/4 \
	./time_get/get_time/char/wrapped_locale \
	./time_get/get_time/char/3 \
	./time_get/get_time/char/wrapped_env \
	./time_get/get_time/char/6 \
	./time_get/get_time/wchar_t/2 \
	./time_get/get_time/wchar_t/5 \
	./time_get/get_time/wchar_t/1 \
	./time_get/get_time/wchar_t/4 \
	./time_get/get_time/wchar_t/wrapped_locale \
	./time_get/get_time/wchar_t/3 \
	./time_get/get_time/wchar_t/wrapped_env \
	./time_get/get_time/wchar_t/6 \
	./collate/compare/char/2 \
	./collate/compare/char/1 \
	./collate/compare/char/wrapped_locale \
	./collate/compare/char/3 \
	./collate/compare/char/wrapped_env \
	./collate/compare/wchar_t/2 \
	./collate/compare/wchar_t/1 \
	./collate/compare/wchar_t/wrapped_locale \
	./collate/compare/wchar_t/3 \
	./collate/compare/wchar_t/wrapped_env \
	./collate/hash/char/2 \
	./collate/hash/char/1 \
	./collate/hash/char/wrapped_locale \
	./collate/hash/char/wrapped_env \
	./collate/hash/wchar_t/2 \
	./collate/hash/wchar_t/1 \
	./collate/hash/wchar_t/wrapped_locale \
	./collate/hash/wchar_t/wrapped_env \
	./collate/transform/char/2 \
	./collate/transform/char/wrapped_locale \
	./collate/transform/char/3 \
	./collate/transform/char/wrapped_env \
	./collate/transform/char/28277 \
	./collate/transform/wchar_t/2 \
	./collate/transform/wchar_t/wrapped_locale \
	./collate/transform/wchar_t/3 \
	./collate/transform/wchar_t/wrapped_env \
	./collate/transform/wchar_t/28277 \
	./ctype/narrow/char/2 \
	./ctype/narrow/char/1 \
	./ctype/narrow/char/wrapped_locale \
	./ctype/narrow/char/wrapped_env \
	./ctype/narrow/char/19955 \
	./ctype/narrow/wchar_t/2 \
	./ctype/narrow/wchar_t/1 \
	./ctype/narrow/wchar_t/wrapped_locale \
	./ctype/narrow/wchar_t/3 \
	./ctype/narrow/wchar_t/wrapped_env \
	./ctype/to/char/1 \
	./ctype/to/char/wrapped_locale \
	./ctype/to/char/wrapped_env \
	./ctype/to/wchar_t/1 \
	./ctype/to/wchar_t/wrapped_locale \
	./ctype/to/wchar_t/wrapped_env \
	./ctype/cons/char/1 \
	./ctype/cons/char/wrapped_locale \
	./ctype/cons/char/wrapped_env \
	./ctype/scan/char/1 \
	./ctype/scan/char/wrapped_locale \
	./ctype/scan/char/wrapped_env \
	./ctype/scan/wchar_t/1 \
	./ctype/scan/wchar_t/wrapped_locale \
	./ctype/scan/wchar_t/wrapped_env \
	./ctype/widen/char/1 \
	./ctype/widen/char/wrapped_locale \
	./ctype/widen/char/wrapped_env \
	./ctype/widen/wchar_t/2 \
	./ctype/widen/wchar_t/1 \
	./ctype/widen/wchar_t/wrapped_locale \
	./ctype/widen/wchar_t/3 \
	./ctype/widen/wchar_t/wrapped_env \
	./ctype/is/char/2 \
	./ctype/is/char/9858 \
	./ctype/is/char/1 \
	./ctype/is/char/wrapped_locale \
	./ctype/is/char/3 \
	./ctype/is/char/wrapped_env \
	./ctype/is/wchar_t/2 \
	./ctype/is/wchar_t/1 \
	./ctype/is/wchar_t/wrapped_locale \
	./ctype/is/wchar_t/11740 \
	./ctype/is/wchar_t/wrapped_env \
	./ctype_base/mask \
	./ctype_base/11844 \
	./money_put/cons/3 \
	./money_put/put/char/2 \
	./money_put/put/char/12971 \
	./money_put/put/char/9780-3 \
	./money_put/put/char/5 \
	./money_put/put/char/1 \
	./money_put/put/char/4 \
	./money_put/put/char/wrapped_locale \
	./money_put/put/char/3 \
	./money_put/put/char/wrapped_env \
	./money_put/put/char/6 \
	./money_put/put/char/39168 \
	./money_put/put/wchar_t/2 \
	./money_put/put/wchar_t/12971 \
	./money_put/put/wchar_t/5 \
	./money_put/put/wchar_t/1 \
	./money_put/put/wchar_t/4 \
	./money_put/put/wchar_t/wrapped_locale \
	./money_put/put/wchar_t/3 \
	./money_put/put/wchar_t/wrapped_env \
	./money_put/put/wchar_t/6 \
	./money_put/put/wchar_t/39168 \
	./numpunct/members/pod/2 \
	./numpunct/members/pod/1 \
	./numpunct/members/char/cache_2 \
	./numpunct/members/char/cache_1 \
	./numpunct/members/char/3 \
	./numpunct/members/wchar_t/2 \
	./numpunct/members/wchar_t/cache_2 \
	./numpunct/members/wchar_t/cache_1 \
	./numpunct/members/wchar_t/1 \
	./numpunct/members/wchar_t/wrapped_locale \
	./numpunct/members/wchar_t/wrapped_env \
	./collate_byname/named_equivalence \
	./num_put/cons/3 \
	./num_put/put/char/2 \
	./num_put/put/char/5 \
	./num_put/put/char/10 \
	./num_put/put/char/12 \
	./num_put/put/char/38196 \
	./num_put/put/char/23953 \
	./num_put/put/char/15565 \
	./num_put/put/char/1 \
	./num_put/put/char/4 \
	./num_put/put/char/wrapped_locale \
	./num_put/put/char/38210 \
	./num_put/put/char/9780-2 \
	./num_put/put/char/3 \
	./num_put/put/char/wrapped_env \
	./num_put/put/char/8 \
	./num_put/put/char/6 \
	./num_put/put/char/20914 \
	./num_put/put/char/20909 \
	./num_put/put/char/7 \
	./num_put/put/char/9 \
	./num_put/put/char/11 \
	./num_put/put/char/14220 \
	./num_put/put/wchar_t/2 \
	./num_put/put/wchar_t/5 \
	./num_put/put/wchar_t/10 \
	./num_put/put/wchar_t/12 \
	./num_put/put/wchar_t/38196 \
	./num_put/put/wchar_t/23953 \
	./num_put/put/wchar_t/15565 \
	./num_put/put/wchar_t/1 \
	./num_put/put/wchar_t/4 \
	./num_put/put/wchar_t/wrapped_locale \
	./num_put/put/wchar_t/38210 \
	./num_put/put/wchar_t/3 \
	./num_put/put/wchar_t/wrapped_env \
	./num_put/put/wchar_t/8 \
	./num_put/put/wchar_t/6 \
	./num_put/put/wchar_t/20914 \
	./num_put/put/wchar_t/20909 \
	./num_put/put/wchar_t/7 \
	./num_put/put/wchar_t/9 \
	./num_put/put/wchar_t/11 \
	./num_put/put/wchar_t/14220 \
	./codecvt/encoding/char/1 \
	./codecvt/encoding/char/wrapped_locale \
	./codecvt/encoding/char/wrapped_env \
	./codecvt/encoding/wchar_t/2 \
	./codecvt/encoding/wchar_t/1 \
	./codecvt/encoding/wchar_t/4 \
	./codecvt/encoding/wchar_t/wrapped_locale \
	./codecvt/encoding/wchar_t/3 \
	./codecvt/encoding/wchar_t/wrapped_env \
	./codecvt/out/char/1 \
	./codecvt/out/char/wrapped_locale \
	./codecvt/out/char/wrapped_env \
	./codecvt/out/wchar_t/2 \
	./codecvt/out/wchar_t/5 \
	./codecvt/out/wchar_t/1 \
	./codecvt/out/wchar_t/4 \
	./codecvt/out/wchar_t/wrapped_locale \
	./codecvt/out/wchar_t/3 \
	./codecvt/out/wchar_t/wrapped_env \
	./codecvt/out/wchar_t/6 \
	./codecvt/out/wchar_t/7 \
	./codecvt/length/char/2 \
	./codecvt/length/char/1 \
	./codecvt/length/char/wrapped_locale \
	./codecvt/length/char/wrapped_env \
	./codecvt/length/wchar_t/2 \
	./codecvt/length/wchar_t/5 \
	./codecvt/length/wchar_t/1 \
	./codecvt/length/wchar_t/4 \
	./codecvt/length/wchar_t/wrapped_locale \
	./codecvt/length/wchar_t/3 \
	./codecvt/length/wchar_t/wrapped_env \
	./codecvt/length/wchar_t/6 \
	./codecvt/length/wchar_t/7 \
	./codecvt/always_noconv/char/1 \
	./codecvt/always_noconv/char/wrapped_locale \
	./codecvt/always_noconv/char/wrapped_env \
	./codecvt/always_noconv/wchar_t/2 \
	./codecvt/always_noconv/wchar_t/1 \
	./codecvt/always_noconv/wchar_t/4 \
	./codecvt/always_noconv/wchar_t/wrapped_locale \
	./codecvt/always_noconv/wchar_t/3 \
	./codecvt/always_noconv/wchar_t/wrapped_env \
	./codecvt/max_length/char/1 \
	./codecvt/max_length/char/wrapped_locale \
	./codecvt/max_length/char/wrapped_env \
	./codecvt/max_length/wchar_t/2 \
	./codecvt/max_length/wchar_t/1 \
	./codecvt/max_length/wchar_t/4 \
	./codecvt/max_length/wchar_t/wrapped_locale \
	./codecvt/max_length/wchar_t/3 \
	./codecvt/max_length/wchar_t/wrapped_env \
	./codecvt/unshift/char/1 \
	./codecvt/unshift/char/wrapped_locale \
	./codecvt/unshift/char/wrapped_env \
	./codecvt/unshift/wchar_t/2 \
	./codecvt/unshift/wchar_t/1 \
	./codecvt/unshift/wchar_t/4 \
	./codecvt/unshift/wchar_t/wrapped_locale \
	./codecvt/unshift/wchar_t/3 \
	./codecvt/unshift/wchar_t/wrapped_env \
	./codecvt/in/char/1 \
	./codecvt/in/char/wrapped_locale \
	./codecvt/in/char/wrapped_env \
	./codecvt/in/wchar_t/2 \
	./codecvt/in/wchar_t/5 \
	./codecvt/in/wchar_t/1 \
	./codecvt/in/wchar_t/4 \
	./codecvt/in/wchar_t/wrapped_locale \
	./codecvt/in/wchar_t/3 \
	./codecvt/in/wchar_t/wrapped_env \
	./codecvt/in/wchar_t/8 \
	./codecvt/in/wchar_t/6 \
	./codecvt/in/wchar_t/7 \
	./codecvt/in/wchar_t/9 \
	./moneypunct/requirements/true/intl \
	./moneypunct/requirements/false/intl \
	./moneypunct/40712 \
	./moneypunct/members/char/2 \
	./moneypunct/members/char/1 \
	./moneypunct/members/char/wrapped_locale \
	./moneypunct/members/char/wrapped_env \
	./moneypunct/members/wchar_t/2 \
	./moneypunct/members/wchar_t/1 \
	./moneypunct/members/wchar_t/wrapped_locale \
	./moneypunct/members/wchar_t/wrapped_env \
	./global_templates/user_facet_hierarchies \
	./global_templates/standard_facet_hierarchies \
	./global_templates/1 \
	./time_put/put/char/2 \
	./time_put/put/char/5 \
	./time_put/put/char/17038 \
	./time_put/put/char/10 \
	./time_put/put/char/1 \
	./time_put/put/char/4 \
	./time_put/put/char/wrapped_locale \
	./time_put/put/char/9780-1 \
	./time_put/put/char/3 \
	./time_put/put/char/wrapped_env \
	./time_put/put/char/8 \
	./time_put/put/char/6 \
	./time_put/put/char/12439_3 \
	./time_put/put/char/12439_1 \
	./time_put/put/char/7 \
	./time_put/put/char/9 \
	./time_put/put/wchar_t/2 \
	./time_put/put/wchar_t/5 \
	./time_put/put/wchar_t/17038 \
	./time_put/put/wchar_t/10 \
	./time_put/put/wchar_t/12439_2 \
	./time_put/put/wchar_t/1 \
	./time_put/put/wchar_t/4 \
	./time_put/put/wchar_t/wrapped_locale \
	./time_put/put/wchar_t/3 \
	./time_put/put/wchar_t/wrapped_env \
	./time_put/put/wchar_t/8 \
	./time_put/put/wchar_t/6 \
	./time_put/put/wchar_t/12439_3 \
	./time_put/put/wchar_t/12439_1 \
	./time_put/put/wchar_t/7 \
	./time_put/put/wchar_t/9 \
	./money_get/get/char/2 \
	./money_get/get/char/5 \
	./money_get/get/char/14 \
	./money_get/get/char/38399 \
	./money_get/get/char/10 \
	./money_get/get/char/12 \
	./money_get/get/char/15 \
	./money_get/get/char/11528 \
	./money_get/get/char/1 \
	./money_get/get/char/4 \
	./money_get/get/char/wrapped_locale \
	./money_get/get/char/17 \
	./money_get/get/char/22131 \
	./money_get/get/char/3 \
	./money_get/get/char/18 \
	./money_get/get/char/wrapped_env \
	./money_get/get/char/8 \
	./money_get/get/char/16 \
	./money_get/get/char/19 \
	./money_get/get/char/6 \
	./money_get/get/char/39168 \
	./money_get/get/char/7 \
	./money_get/get/char/9 \
	./money_get/get/char/11 \
	./money_get/get/char/13 \
	./money_get/get/wchar_t/2 \
	./money_get/get/wchar_t/5 \
	./money_get/get/wchar_t/14 \
	./money_get/get/wchar_t/38399 \
	./money_get/get/wchar_t/10 \
	./money_get/get/wchar_t/12 \
	./money_get/get/wchar_t/15 \
	./money_get/get/wchar_t/11528 \
	./money_get/get/wchar_t/1 \
	./money_get/get/wchar_t/4 \
	./money_get/get/wchar_t/wrapped_locale \
	./money_get/get/wchar_t/17 \
	./money_get/get/wchar_t/22131 \
	./money_get/get/wchar_t/3 \
	./money_get/get/wchar_t/18 \
	./money_get/get/wchar_t/wrapped_env \
	./money_get/get/wchar_t/8 \
	./money_get/get/wchar_t/16 \
	./money_get/get/wchar_t/19 \
	./money_get/get/wchar_t/6 \
	./money_get/get/wchar_t/39168 \
	./money_get/get/wchar_t/7 \
	./money_get/get/wchar_t/9 \
	./money_get/get/wchar_t/11 \
	./money_get/get/wchar_t/13 \
	./money_get/cons/3 \
	./locale/operations/2 \
	./locale/operations/1 \
	./locale/cons/2 \
	./locale/cons/5 \
	./locale/cons/12658_thread-1 \
	./locale/cons/40184 \
	./locale/cons/29217 \
	./locale/cons/unicode \
	./locale/cons/1 \
	./locale/cons/4 \
	./locale/cons/38365 \
	./locale/cons/12438 \
	./locale/cons/12658_thread-2 \
	./locale/cons/7222-c \
	./locale/cons/8 \
	./locale/cons/7222-env \
	./locale/cons/6 \
	./locale/cons/7 \
	./locale/cons/12352 \
	./locale/cons/38368 \
	./locale/global_locale_objects/2 \
	./locale/global_locale_objects/14071 \
	./locale/global_locale_objects/1 \
	./locale/global_locale_objects/3 \
	./locale/13630 \
	./codecvt_byname/50714 \
	./messages/members/char/3 \
	./numpunct_byname/named_equivalence \
	./facet/25421 \
	./facet/2 \
	./facet/1 \
	./num_get/get/char/2 \
	./num_get/get/char/5 \
	./num_get/get/char/14 \
	./num_get/get/char/10 \
	./num_get/get/char/12 \
	./num_get/get/char/37958 \
	./num_get/get/char/15 \
	./num_get/get/char/23953 \
	./num_get/get/char/1 \
	./num_get/get/char/4 \
	./num_get/get/char/wrapped_locale \
	./num_get/get/char/39802 \
	./num_get/get/char/22131 \
	./num_get/get/char/3 \
	./num_get/get/char/wrapped_env \
	./num_get/get/char/8 \
	./num_get/get/char/16 \
	./num_get/get/char/6 \
	./num_get/get/char/39168 \
	./num_get/get/char/7 \
	./num_get/get/char/9 \
	./num_get/get/char/11 \
	./num_get/get/char/13 \
	./num_get/get/wchar_t/2 \
	./num_get/get/wchar_t/5 \
	./num_get/get/wchar_t/14 \
	./num_get/get/wchar_t/10 \
	./num_get/get/wchar_t/12 \
	./num_get/get/wchar_t/37958 \
	./num_get/get/wchar_t/15 \
	./num_get/get/wchar_t/23953 \
	./num_get/get/wchar_t/1 \
	./num_get/get/wchar_t/4 \
	./num_get/get/wchar_t/wrapped_locale \
	./num_get/get/wchar_t/39802 \
	./num_get/get/wchar_t/22131 \
	./num_get/get/wchar_t/3 \
	./num_get/get/wchar_t/wrapped_env \
	./num_get/get/wchar_t/8 \
	./num_get/get/wchar_t/16 \
	./num_get/get/wchar_t/6 \
	./num_get/get/wchar_t/39168 \
	./num_get/get/wchar_t/7 \
	./num_get/get/wchar_t/9 \
	./num_get/get/wchar_t/11 \
	./num_get/get/wchar_t/13 \
	./num_get/cons/3 \
	"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./debug/shrink_to_fit \

EXECS_21_STRINGS="\
	./char_traits/requirements/short/1 \
	./char_traits/requirements/char16_t/typedefs \
	./char_traits/requirements/constexpr_functions \
	./char_traits/requirements/char/1 \
	./char_traits/requirements/char/typedefs \
	./char_traits/requirements/char32_t/typedefs \
	./char_traits/requirements/wchar_t/1 \
	./char_traits/requirements/wchar_t/typedefs \
	./c_strings/char/2 \
	./c_strings/char/1 \
	./c_strings/wchar_t/2 \
	./c_strings/wchar_t/24559 \
	./c_strings/wchar_t/1 \
	./basic_string/insert/char/2 \
	./basic_string/insert/char/1 \
	./basic_string/insert/wchar_t/2 \
	./basic_string/insert/wchar_t/1 \
	./basic_string/pthread18185 \
	./basic_string/inserters_extractors/pod/10081-out \
	./basic_string/inserters_extractors/pod/10081-in \
	./basic_string/inserters_extractors/char/5 \
	./basic_string/inserters_extractors/char/10 \
	./basic_string/inserters_extractors/char/1 \
	./basic_string/inserters_extractors/char/4 \
	./basic_string/inserters_extractors/char/8 \
	./basic_string/inserters_extractors/char/6 \
	./basic_string/inserters_extractors/char/7 \
	./basic_string/inserters_extractors/char/9 \
	./basic_string/inserters_extractors/char/28277 \
	./basic_string/inserters_extractors/char/11 \
	./basic_string/inserters_extractors/wchar_t/5 \
	./basic_string/inserters_extractors/wchar_t/10 \
	./basic_string/inserters_extractors/wchar_t/1 \
	./basic_string/inserters_extractors/wchar_t/4 \
	./basic_string/inserters_extractors/wchar_t/8 \
	./basic_string/inserters_extractors/wchar_t/6 \
	./basic_string/inserters_extractors/wchar_t/7 \
	./basic_string/inserters_extractors/wchar_t/9 \
	./basic_string/inserters_extractors/wchar_t/28277 \
	./basic_string/inserters_extractors/wchar_t/11 \
	./basic_string/append/char/2 \
	./basic_string/append/char/1 \
	./basic_string/append/char/3 \
	./basic_string/append/wchar_t/2 \
	./basic_string/append/wchar_t/1 \
	./basic_string/append/wchar_t/3 \
	./basic_string/requirements/exception/basic \
	./basic_string/requirements/exception/generation_prohibited \
	./basic_string/requirements/exception/propagation_consistent \
	./basic_string/requirements/citerators \
	./basic_string/rfind/char/2 \
	./basic_string/rfind/char/1 \
	./basic_string/rfind/char/3 \
	./basic_string/rfind/wchar_t/2 \
	./basic_string/rfind/wchar_t/1 \
	./basic_string/rfind/wchar_t/3 \
	./basic_string/capacity/1 \
	./basic_string/capacity/char/2 \
	./basic_string/capacity/char/shrink_to_fit \
	./basic_string/capacity/char/18654 \
	./basic_string/capacity/char/1 \
	./basic_string/capacity/wchar_t/2 \
	./basic_string/capacity/wchar_t/shrink_to_fit \
	./basic_string/capacity/wchar_t/18654 \
	./basic_string/capacity/wchar_t/1 \
	./basic_string/numeric_conversions/char/stoll \
	./basic_string/numeric_conversions/char/to_string \
	./basic_string/numeric_conversions/char/stold \
	./basic_string/numeric_conversions/char/dr1261 \
	./basic_string/numeric_conversions/char/stod \
	./basic_string/numeric_conversions/char/stoull \
	./basic_string/numeric_conversions/char/stol \
	./basic_string/numeric_conversions/char/stoi \
	./basic_string/numeric_conversions/char/stof \
	./basic_string/numeric_conversions/char/stoul \
	./basic_string/numeric_conversions/wchar_t/stoll \
	./basic_string/numeric_conversions/wchar_t/stold \
	./basic_string/numeric_conversions/wchar_t/dr1261 \
	./basic_string/numeric_conversions/wchar_t/stod \
	./basic_string/numeric_conversions/wchar_t/stoull \
	./basic_string/numeric_conversions/wchar_t/stol \
	./basic_string/numeric_conversions/wchar_t/stoi \
	./basic_string/numeric_conversions/wchar_t/stof \
	./basic_string/numeric_conversions/wchar_t/stoul \
	./basic_string/numeric_conversions/wchar_t/to_wstring \
	./basic_string/init-list \
	./basic_string/operators/char/2 \
	./basic_string/operators/char/1 \
	./basic_string/operators/char/4 \
	./basic_string/operators/char/3 \
	./basic_string/operators/wchar_t/2 \
	./basic_string/operators/wchar_t/1 \
	./basic_string/operators/wchar_t/4 \
	./basic_string/operators/wchar_t/3 \
	./basic_string/modifiers/char/pop_back \
	./basic_string/modifiers/wchar_t/pop_back \
	./basic_string/element_access/char/2 \
	./basic_string/element_access/char/front_back \
	./basic_string/element_access/char/empty \
	./basic_string/element_access/char/1 \
	./basic_string/element_access/char/4 \
	./basic_string/element_access/char/3 \
	./basic_string/element_access/char/21674 \
	./basic_string/element_access/wchar_t/2 \
	./basic_string/element_access/wchar_t/front_back \
	./basic_string/element_access/wchar_t/empty \
	./basic_string/element_access/wchar_t/1 \
	./basic_string/element_access/wchar_t/4 \
	./basic_string/element_access/wchar_t/3 \
	./basic_string/element_access/wchar_t/21674 \
	./basic_string/operations/char/1 \
	./basic_string/operations/wchar_t/1 \
	./basic_string/cons/char/2 \
	./basic_string/cons/char/5 \
	./basic_string/cons/char/moveable2 \
	./basic_string/cons/char/42261 \
	./basic_string/cons/char/moveable \
	./basic_string/cons/char/1 \
	./basic_string/cons/char/4 \
	./basic_string/cons/char/3 \
	./basic_string/cons/char/6 \
	./basic_string/cons/wchar_t/2 \
	./basic_string/cons/wchar_t/5 \
	./basic_string/cons/wchar_t/moveable2 \
	./basic_string/cons/wchar_t/42261 \
	./basic_string/cons/wchar_t/moveable \
	./basic_string/cons/wchar_t/1 \
	./basic_string/cons/wchar_t/4 \
	./basic_string/cons/wchar_t/3 \
	./basic_string/cons/wchar_t/6 \
	./basic_string/replace/char/2 \
	./basic_string/replace/char/5 \
	./basic_string/replace/char/1 \
	./basic_string/replace/char/4 \
	./basic_string/replace/char/3 \
	./basic_string/replace/char/6 \
	./basic_string/replace/wchar_t/2 \
	./basic_string/replace/wchar_t/5 \
	./basic_string/replace/wchar_t/1 \
	./basic_string/replace/wchar_t/4 \
	./basic_string/replace/wchar_t/3 \
	./basic_string/replace/wchar_t/6 \
	./basic_string/pthread4 \
	./basic_string/substr/char/1 \
	./basic_string/substr/wchar_t/1 \
	./basic_string/compare/char/1 \
	./basic_string/compare/char/13650 \
	./basic_string/compare/wchar_t/1 \
	./basic_string/compare/wchar_t/13650 \
	./basic_string/assign/char/2 \
	./basic_string/assign/char/1 \
	./basic_string/assign/char/3 \
	./basic_string/assign/char/move_assign \
	./basic_string/assign/wchar_t/2 \
	./basic_string/assign/wchar_t/1 \
	./basic_string/assign/wchar_t/3 \
	./basic_string/assign/wchar_t/move_assign \
	./basic_string/types/1 \
	./basic_string/find/char/2 \
	./basic_string/find/char/1 \
	./basic_string/find/char/4 \
	./basic_string/find/char/3 \
	./basic_string/find/wchar_t/2 \
	./basic_string/find/wchar_t/1 \
	./basic_string/find/wchar_t/4 \
	./basic_string/find/wchar_t/3 \
	"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./is_signed/value \
#	./specialized_algorithms/uninitialized_copy/808590 \
#	./is_integral/value \
#	./allocator_traits/members/select \
#	./is_unsigned/value \
#	./hash/chi2_quality \

EXECS_20_UTIL="\
	./is_lvalue_reference/value \
	./rel_ops \
	./uses_allocator/construction \
	./uses_allocator/value \
	./has_trivial_copy_assign/value \
	./steady_clock/constexpr_data \
	./is_function/24808 \
	./is_function/value \
	./is_convertible/value \
	./function/2 \
	./function/5 \
	./function/invoke/move_only \
	./function/null_pointer_comparisons \
	./function/63840 \
	./function/43397 \
	./function/1 \
	./function/4 \
	./function/cons/55320 \
	./function/cons/57465 \
	./function/cons/callable \
	./function/cons/addressof \
	./function/cons/move \
	./function/cons/move_target \
	./function/48541 \
	./function/3 \
	./function/8 \
	./function/6 \
	./function/assign/move \
	./function/assign/move_target \
	./function/7 \
	./function/9 \
	./duration/requirements/constexpr_functions \
	./duration/comparison_operators/constexpr \
	./duration/comparison_operators/1 \
	./duration/cons/2 \
	./duration/cons/constexpr \
	./duration/cons/1 \
	./duration/arithmetic/2 \
	./duration/arithmetic/constexpr \
	./duration/arithmetic/dr2020 \
	./duration/arithmetic/dr934-2 \
	./duration/arithmetic/1 \
	./is_trivially_destructible/value \
	./is_nothrow_constructible/value \
	./has_trivial_copy_constructor/value \
	./is_object/24808 \
	./is_object/value \
	./shared_ptr/dest/dest \
	./shared_ptr/creation/36949 \
	./shared_ptr/creation/alloc \
	./shared_ptr/creation/dr925 \
	./shared_ptr/creation/make \
	./shared_ptr/creation/dr402 \
	./shared_ptr/creation/private \
	./shared_ptr/casts/1 \
	./shared_ptr/modifiers/reset_alloc \
	./shared_ptr/modifiers/reset \
	./shared_ptr/modifiers/swap \
	./shared_ptr/comparison/cmp \
	./shared_ptr/comparison/less \
	./shared_ptr/cons/unique_ptr_array \
	./shared_ptr/cons/unique_ptr \
	./shared_ptr/cons/unique_ptr_deleter_ref_1 \
	./shared_ptr/cons/constexpr \
	./shared_ptr/cons/alloc \
	./shared_ptr/cons/weak_ptr_expired \
	./shared_ptr/cons/move \
	./shared_ptr/cons/alias \
	./shared_ptr/cons/unique_ptr_deleter_ref_2 \
	./shared_ptr/cons/pointer \
	./shared_ptr/cons/weak_ptr \
	./shared_ptr/cons/default \
	./shared_ptr/cons/nullptr \
	./shared_ptr/cons/58659 \
	./shared_ptr/cons/unique_ptr_deleter \
	./shared_ptr/cons/copy \
	./shared_ptr/cons/auto_ptr \
	./shared_ptr/misc/io \
	./shared_ptr/misc/42019 \
	./shared_ptr/misc/24595 \
	./shared_ptr/misc/swap \
	./shared_ptr/observers/unique \
	./shared_ptr/observers/bool_conv \
	./shared_ptr/observers/get \
	./shared_ptr/observers/owner_before \
	./shared_ptr/observers/use_count \
	./shared_ptr/assign/shared_ptr \
	./shared_ptr/assign/move \
	./shared_ptr/assign/auto_ptr_rvalue \
	./shared_ptr/assign/auto_ptr \
	./shared_ptr/assign/unique_ptr_rvalue \
	./shared_ptr/assign/assign \
	./shared_ptr/hash/1 \
	./shared_ptr/thread/mutex_weaktoshared \
	./shared_ptr/thread/default_weaktoshared \
	./common_type/requirements/typedefs-1 \
	./common_type/requirements/typedefs-2 \
	./is_nothrow_assignable/value \
	./forward/b \
	./forward/e \
	./forward/a \
	./forward/d \
	./weak_ptr/cons/constexpr \
	./weak_ptr/lock/1 \
	./weak_ptr/observers/owner_before \
	./aligned_storage/value \
	./is_member_object_pointer/value \
	./is_nothrow_move_constructible/value \
	./unique_ptr/requirements/pointer_type_array \
	./unique_ptr/requirements/pointer_type \
	./unique_ptr/modifiers/43183 \
	./unique_ptr/specialized_algorithms/comparisons_array \
	./unique_ptr/specialized_algorithms/comparisons \
	./unique_ptr/specialized_algorithms/swap \
	./unique_ptr/cons/constexpr \
	./unique_ptr/cons/pointer \
	./unique_ptr/cons/pointer_array \
	./unique_ptr/cons/nullptr \
	./unique_ptr/cons/auto_ptr \
	./unique_ptr/cons/ptr_deleter \
	./unique_ptr/assign/move \
	./unique_ptr/assign/nullptr \
	./unique_ptr/assign/48635 \
	./unique_ptr/assign/move_array \
	./unique_ptr/54351 \
	./unique_ptr/hash/1 \
	./is_nothrow_move_assignable/value \
	./move_if_noexcept/constexpr \
	./move_if_noexcept/1 \
	./system_clock/constexpr_data \
	./system_clock/1 \
	./bad_function_call/cons_virtual_derivation \
	./bad_function_call/what \
	./temporary_buffer \
	./specialized_algorithms/uninitialized_copy/move_iterators/1 \
	./specialized_algorithms/uninitialized_copy_n/move_iterators/1 \
	./is_move_constructible/value \
	./duration_cast/constexpr \
	./is_move_assignable/value \
	./is_floating_point/value \
	./is_pod/value \
	./is_scalar/value \
	./is_standard_layout/value \
	./is_constructible/value \
	./is_copy_assignable/value \
	./is_nothrow_copy_constructible/value \
	./auto_ptr/2 \
	./auto_ptr/5 \
	./auto_ptr/1 \
	./auto_ptr/4 \
	./auto_ptr/3 \
	./auto_ptr/6 \
	./auto_ptr/7 \
	./auto_ptr/3946 \
	./reference_wrapper/invoke \
	./reference_wrapper/typedefs-3 \
	./reference_wrapper/invoke-2 \
	./time_point/2 \
	./time_point/nonmember/constexpr \
	./time_point/requirements/constexpr_functions \
	./time_point/1 \
	./time_point/comparison_operators/constexpr \
	./time_point/cons/constexpr \
	./time_point/3 \
	./ratio/requirements/constexpr_data \
	./ratio/operations/ops1 \
	./ratio/operations/ops2 \
	./ratio/operations/ops3 \
	./ratio/operations/47913 \
	./ratio/operations/45866 \
	./ratio/cons/cons1 \
	./ratio/comparisons/comp3 \
	./ratio/comparisons/comp2 \
	./ratio/comparisons/comp1 \
	./time_point_cast/constexpr \
	./is_reference/value \
	./allocator/10378 \
	./allocator/14176 \
	./allocator/1 \
	./allocator/8230 \
	./allocator_traits/members/max_size \
	./allocator_traits/members/construct \
	./allocator_traits/members/allocate_hint \
	./allocator_traits/members/destroy \
	./enable_shared_from_this/cons/constexpr \
	./add_rvalue_reference/value \
	./is_nothrow_default_constructible/value \
	./pointer_traits/pointer_to \
	./is_member_pointer/value \
	./remove_reference/value \
	./default_delete/cons/constexpr \
	./is_rvalue_reference/value \
	./is_literal_type/value \
	./add_lvalue_reference/value \
	./is_nothrow_copy_assignable/value \
	./addressof/1 \
	./owner_less/cmp \
	./function_objects/mem_fn/forward \
	./function_objects/binders/3113 \
	./function_objects/dr660 \
	./function_objects/comparisons \
	./pair/2 \
	./pair/requirements/dr801 \
	./pair/make_pair/constexpr \
	./pair/piecewise \
	./pair/moveable \
	./pair/1 \
	./pair/comparison_operators/constexpr \
	./pair/4 \
	./pair/cons/constexpr \
	./pair/3 \
	./pair/swap \
	./is_member_function_pointer/value \
	./tuple/creation_functions/23978 \
	./tuple/creation_functions/tie2 \
	./tuple/creation_functions/make_tuple \
	./tuple/creation_functions/tuple_cat \
	./tuple/creation_functions/forward_as_tuple \
	./tuple/creation_functions/constexpr \
	./tuple/creation_functions/48476 \
	./tuple/creation_functions/tie \
	./tuple/61947 \
	./tuple/requirements/dr801 \
	./tuple/tuple_element \
	./tuple/element_access/get \
	./tuple/moveable2 \
	./tuple/moveable \
	./tuple/comparison_operators/constexpr \
	./tuple/comparison_operators/comparisons \
	./tuple/tuple_size \
	./tuple/cons/constexpr-2 \
	./tuple/cons/assignment \
	./tuple/cons/big_tuples \
	./tuple/cons/allocators \
	./tuple/cons/constexpr-3 \
	./tuple/cons/constructor \
	./tuple/cv_tuple_size \
	./tuple/48476 \
	./tuple/swap \
	./typeindex/hash \
	./typeindex/comparison_operators \
	./typeindex/hash_code \
	./typeindex/name \
	./hash/operators/size_t \
	./hash/quality \
	./is_compound/value \
	./bind/ref2 \
	./bind/placeholders \
	./bind/all_bound \
	./bind/cv_quals_3 \
	./bind/ref \
	./bind/move \
	./bind/45924 \
	./bind/nested \
	./bind/57899 \
	./bind/cv_quals_2 \
	./bind/cv_quals \
	./bind/conv_result \
	./scoped_allocator/2 \
	./scoped_allocator/1 \
	./is_trivial/value \
	./has_trivial_default_constructor/value \
	./is_fundamental/value \
	./is_copy_constructible/value \
	"

#
# Fail or need addition process under centos7 aarch64 libstdc++ 4.8.5
#
#	./headers/system_error/34538 \

EXECS_19_DIAGN="\
	./error_condition/operators/equal \
	./error_condition/operators/not_equal \
	./error_condition/operators/bool \
	./error_condition/modifiers/39881 \
	./error_condition/cons/1 \
	./error_condition/cons/39881 \
	./error_category/operators/equal \
	./error_category/operators/not_equal \
	./error_category/cons/default \
	./error_code/operators/equal \
	./error_code/operators/not_equal \
	./error_code/operators/bool \
	./error_code/modifiers/39882 \
	./error_code/cons/1 \
	./error_code/cons/39882 \
	./logic_error/what-1 \
	./logic_error/cons_virtual_derivation \
	./logic_error/what-2 \
	./logic_error/what-3 \
	./logic_error/what-big \
	./system_error/cons-1 \
	./system_error/what-1 \
	./system_error/cons_virtual_derivation \
	./system_error/what-4 \
	./system_error/what-2 \
	./system_error/what-3 \
	./system_error/what-big \
	./runtime_error/what-1 \
	./runtime_error/cons_virtual_derivation \
	./runtime_error/what-2 \
	./runtime_error/what-3 \
	./runtime_error/what-big \
	./stdexcept \
	"
EXECS_18_SUPPORT="\
	./exception/what \
	./exception/38732 \
	./type_info/hash_code \
	./bad_cast/cons_virtual_derivation \
	./bad_cast/what \
	./50594 \
	./bad_typeid/cons_virtual_derivation \
	./bad_typeid/what \
	./new_delete_placement \
	./exception_ptr/make_exception_ptr \
	./exception_ptr/current_exception \
	./exception_ptr/rethrow_exception \
	./exception_ptr/move \
	./exception_ptr/requirements \
	./exception_ptr/lifespan \
	./initializer_list/constexpr \
	./initializer_list/requirements/constexpr_functions \
	./bad_alloc/cons_virtual_derivation \
	./bad_alloc/what \
	./pthread_guard \
	./numeric_limits/requirements/constexpr_data \
	./numeric_limits/requirements/constexpr_functions \
	./numeric_limits/lowest \
	./numeric_limits/specialization_default_values \
	./numeric_limits/max_digits10 \
	./numeric_limits/denorm_min \
	./numeric_limits/cons/default \
	./numeric_limits/cons/default_c++0x \
	./numeric_limits/infinity \
	./numeric_limits/is_signed \
	./numeric_limits/traps \
	./numeric_limits/char16_32_t \
	./numeric_limits/digits10 \
	./numeric_limits/epsilon \
	./numeric_limits/quiet_NaN \
	./numeric_limits/dr559 \
	./numeric_limits/min_max \
	./numeric_limits/is_iec559 \
	./nested_exception/rethrow_if_nested \
	./nested_exception/cons \
	./nested_exception/rethrow_nested \
	./nested_exception/throw_with_nested \
	./nested_exception/nested_ptr \
	./cxa_vec \
	./quick_exit/quick_exit \
	./bad_exception/cons_virtual_derivation \
	./bad_exception/what \
	./bad_exception/59392 \
	./bad_exception/23591_thread-1 \
	./uncaught_exception/14026 \
	"
EXECS_17_INTRO="\
	./freestanding \
	./shared_with_static_deps \
	./static \
	"
EXECS_PPTR="\
	48362 \
	cxx11 \
	debug \
	shared_ptr \
	simple \
	whatis \
	"
EXECS_ABI="\
	cxx_runtime_only_linkage \
	pr42230 \
	demangle/regression/7986-09 \
	demangle/regression/cw-03 \
	demangle/regression/7986-02 \
	demangle/regression/cw-09 \
	demangle/regression/cw-05 \
	demangle/regression/cw-15 \
	demangle/regression/cw-01 \
	demangle/regression/cw-08 \
	demangle/regression/7986-08 \
	demangle/regression/cw-07 \
	demangle/regression/cw-11 \
	demangle/regression/cw-06 \
	demangle/regression/cw-16 \
	demangle/regression/7986-01 \
	demangle/regression/7986-03 \
	demangle/regression/cw-04 \
	demangle/regression/7986-11 \
	demangle/regression/cw-14 \
	demangle/regression/old \
	demangle/regression/3111-1 \
	demangle/regression/3111-2 \
	demangle/regression/7986-06 \
	demangle/regression/7986 \
	demangle/regression/7986-07 \
	demangle/regression/7986-12 \
	demangle/regression/7986-05 \
	demangle/regression/cw-12 \
	demangle/regression/8897 \
	demangle/regression/7986-04 \
	demangle/regression/cw-02 \
	demangle/regression/cw-10 \
	demangle/regression/7986-10 \
	demangle/regression/cw-13 \
	demangle/abi_examples/05 \
	demangle/abi_examples/14 \
	demangle/abi_examples/08 \
	demangle/abi_examples/10 \
	demangle/abi_examples/01 \
	demangle/abi_examples/12 \
	demangle/abi_examples/03 \
	demangle/abi_examples/15 \
	demangle/abi_examples/21 \
	demangle/abi_examples/06 \
	demangle/abi_examples/02 \
	demangle/abi_examples/20 \
	demangle/abi_examples/23 \
	demangle/abi_examples/24 \
	demangle/abi_examples/17 \
	demangle/abi_examples/09 \
	demangle/abi_examples/25 \
	demangle/abi_examples/18 \
	demangle/abi_examples/26 \
	demangle/abi_examples/16 \
	demangle/abi_examples/19 \
	demangle/abi_examples/22 \
	demangle/abi_examples/04 \
	demangle/abi_examples/07 \
	demangle/abi_examples/11 \
	demangle/abi_examples/13 \
	demangle/cxx0x/rref \
	demangle/abi_text/05 \
	demangle/abi_text/14 \
	demangle/abi_text/08 \
	demangle/abi_text/10 \
	demangle/abi_text/01 \
	demangle/abi_text/12 \
	demangle/abi_text/03 \
	demangle/abi_text/06 \
	demangle/abi_text/02 \
	demangle/abi_text/09 \
	demangle/abi_text/04 \
	demangle/abi_text/07 \
	demangle/abi_text/11 \
	demangle/abi_text/13 \
	"
EXEC=
EXECS=
SUBDIR=

run_tst() {
	echo -n "running $SUBDIR/$EXEC ..."
	echo "" >> ${INFOFILE}
	echo "running $SUBDIR/$EXEC ..." >> ${INFOFILE}
	echo "" >> ${ERRFILE}
	echo "running $SUBDIR/$EXEC ..." >> ${ERRFILE}
	./$EXEC >> ${INFOFILE} 2>>${ERRFILE}
	ret=$?
	echo "   ret: $ret"
	if [ "$ret" != "0" ]; then
		echo "Fail,$ret,$SUBDIR/$EXEC" >> ${RPTFILE}
	fi
	sleep 1
}

run_tsts() {
	if [ "$ONLYCOUNT" == "false" ]; then
		echo "enter $SUBDIR..."
		echo "enter $SUBDIR..." >> ${INFOFILE}
		echo "enter $SUBDIR..." >> ${ERRFILE}
		cd $SUBDIR
		export LD_LIBRARY_PATH=`pwd`
		for EXEC in ${EXECS}
		do
			run_tst
		done
		cd ..
		echo "leave $SUBDIR..."
		echo "leave $SUBDIR..." >> ${INFOFILE}
		echo "leave $SUBDIR..." >> ${ERRFILE}
	else
		subcount=0
		for EXEC in ${EXECS}
		do
			subcount=$(($subcount + 1))
		done
		count=$(($count + $subcount))
		echo "$subcount $SUBDIR"
	fi
}


cd ${basedir}
if [ "$ONLYCOUNT" == "false" ]; then
	rm -rf ${RPTFILE} ${ERRFILE} ${INFOFILE}
	touch ${RPTFILE} ${ERRFILE} ${INFOFILE}
	echo "VERSION: 1.0.0 from libstdc++-4.8.5"
	echo "VERSION: 1.0.0 from libstdc++-4.8.5" >> ${INFOFILE}
	echo "VERSION: 1.0.0 from libstdc++-4.8.5" >> ${ERRFILE}
	echo "VERSION: 1.0.0 from libstdc++-4.8.5" >> ${RPTFILE}
fi

SUBDIR="30_threads"
EXECS=$EXECS_30_THREADS
run_tsts

SUBDIR="29_atomics"
EXECS=$EXECS_29_ATOM
run_tsts

SUBDIR="28_regex"
EXECS=$EXECS_28_REGEX
run_tsts

SUBDIR="27_io"
EXECS=$EXECS_27_IO
run_tsts

SUBDIR="26_numerics"
EXECS=$EXECS_26_NUMER
run_tsts

SUBDIR="25_algorithms"
EXECS=$EXECS_25_ALGO
run_tsts

SUBDIR="24_iterators"
EXECS=$EXECS_24_ITER
run_tsts

SUBDIR="23_containers"
EXECS=$EXECS_23_CONTA
run_tsts

SUBDIR="22_locale"
EXECS=$EXECS_22_LOCALE
run_tsts

SUBDIR="21_strings"
EXECS=$EXECS_21_STRINGS
run_tsts

SUBDIR="20_util"
EXECS=$EXECS_20_UTIL
run_tsts

SUBDIR="19_diagnostics"
EXECS=$EXECS_19_DIAGN
run_tsts

SUBDIR="18_support"
EXECS=$EXECS_18_SUPPORT
run_tsts

SUBDIR="17_intro"
EXECS=$EXECS_17_INTRO
run_tsts

SUBDIR="libstdc++-prettyprinters"
EXECS=$EXECS_PPTR
run_tsts

SUBDIR="abi"
EXECS=$EXECS_ABI
run_tsts

if [ "$ONLYCOUNT" == "false" ]; then
	sync
	gzip -c ${RPTFILE} > ${RPTFILE}.gz
	gzip -c ${ERRFILE} > ${ERRFILE}.gz
	gzip -c ${INFOFILE} > ${INFOFILE}.gz
	sync
else
	echo "total: $count"
fi

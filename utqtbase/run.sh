#!/bin/bash

basedir=$(cd "$(dirname "$0")"; pwd)
cd "$basedir"

export LANG=en_US.UTF-8
export LANGUAGE=en_US:en

btag=$1
rtag=$2
buildir="$3"
if [ "x$buildir" == "x" ]
then
	buildir="${basedir}/work/build"
fi

outdir="${basedir}/output"
logfile="${outdir}/rpt_qtbase-$btag-$rtag.log"
errfile="${outdir}/rpt_qtbase-$btag-$rtag.err"

mkdir -p "${outdir}"
rm -rf "${logfile}" "${errfile}"
pushd "${buildir}"

pgms=`find -type f | grep -v "\.so$" | grep -v "\.o$" | xargs file | grep ":.*ELF\>" | awk -F ':' '{print $1}' | xargs`
for pgm in ${pgms}
do
	echo "running $pgm ..."
	$pgm >> "${logfile}" 2>> "${errfile}"
done

popd

#!/bin/bash

basedir=$(cd "$(dirname "$0")"; pwd)
cd "$basedir"

export LANG=en_US.UTF-8
export LANGUAGE=en_US:en

srcfile="$1"
tag=$2
workdir="${basedir}/work"
outdir="${basedir}/output"
srcdir="src"
buildir="build"
logfile="${outdir}/build-$tag.log"
errfile="${outdir}/build-$tag.err"

rm -rf "${outdir}"
mkdir "${outdir}"
rm -rf "${workdir}"
mkdir "${workdir}"

if [ -d "${srcfile}" ]
then
	cp -a "${srcfile}" "${workdir}"
else
	tar xf "${srcfile}" -C "${workdir}"
fi
pushd "${workdir}"
mv * $srcdir
mkdir $buildir
cd $buildir
qmake ../$srcdir >> "${logfile}" 2>> "${errfile}" && make >> "${logfile}" 2>> "${errfile}"
popd

#include <stdio.h>
#include <stdlib.h>

static char USAGE[] ="\
Feature: backup the old file, and use the new file instead of the old file, all operations are in one process.\n\
Usage:   %s oldfile newfile bakfile\n\
\t\toldfile: the old file, which will be instead of by the new file\n\
\t\tnewfile: the new file, which will instead of the old file, it will be renamed to the old file\n\
\t\tbakfile: the backup file which the old file will use, the old file will be renamed to the backup file.\n\
";

int main(int argc, char *argv[])
{
	char *oname;
	char *nname;
	char *bname;

	if (argc != 4) {
		fprintf(stderr, USAGE, argv[0]);
		return -1;
	}
	oname = argv[1];
	nname = argv[2];
	bname = argv[3];

	rename(oname, bname);
	rename(nname, oname);

	return 0;
}

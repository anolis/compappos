#
# For x86_64 arch
#
# for kylin v4 server ld test
./modild ./ld-2.23.so 41249 8
# for kylin v10 server ld test
./modild ./ld-2.28.so 39534 8
# for uos20 desktop ld test
./modild ./ld-2.28.so 39038 8
# for ubuntu18 ld test
./modild ./ld-2.27.so 42236 2
./modild ./ld-2.27.so 42243 6
# for centos7
./modild ./ld-2.17.so 38950 2
./modild ./ld-2.17.so 38967 6
# centos8 itself ok
# ubuntu20 itself ok

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static char USAGE[] = "\
Feature: write the elf file contents by nop insns data, so let cpu skip the related instructions.\n\
Usage:    %s elfile offset size\n\
\t\telfile: the elf file which will be modified.\n\
\t\toffset: the offset in the elf file which the modification starts.\n\
\t\tsize:   the size which is started at the offset in the elf file, to deside how many nop insns will be written.\n\
";

int main(int argc, char *argv[])
{
	int file;
	long off;
	int size;
	char *filename;
	char buf[0x200];

	if (argc < 4) {
		printf(USAGE, argv[0]);
		return 0;
	}

	filename = argv[1];
	off = atol(argv[2]);
	size = atoi(argv[3]);
	if (size > sizeof(buf)) {
		fprintf(stderr, "invalid sizes %d which is bigger than %ld.\n",
				size, sizeof(buf));
		return -1;
	}
	printf("Input parameter:\n\tfilename: %s\n\toffset: %ld\n\tsize: %d\n",
		filename, off, size);

	memset(buf, 0x90, sizeof(buf)); /* 0x90 is nop inst under x86_64 */

	file = open(filename, O_RDWR);
	if (file == -1) {
		fprintf(stderr, "open file failed: %s.\n", filename);
		return -1;
	}
	if (lseek(file, off, SEEK_SET) != off) {
		fprintf(stderr, "seek failed: %s, offset: %ld.\n",
				filename, off);
		goto err;
	}
	if (write(file, buf, size) != size) {
		fprintf(stderr, "write failed: %s, offset: %ld, size: %d.\n",
				filename, off, size);
		goto err;
	}

	close(file);
	return 0;
err:
	close(file);
	return -1;
}

/*
 * They are the common functions in C code for all wrapped libs
 *
 * This file will be merged into main.c which is the main source code file of
 * the wrapped lib, by gensocode. 1234569fjjoidfjfd is appended to each symbol
 * to avoid the name conflict with the interface of target shared library.
 */

#include <stdio.h>
#include <dlfcn.h>
#include <string.h>

/*
 * Get the memory base address of the real shared library.
 */

static unsigned long module_addr_xdgt1294675349dsT;

static void get_module_addr_xdgt1294675349dsT(void)
{
	FILE *f = fopen("/proc/self/maps", "r");
	char buf[0x400];

	if (!f)
		return;

	buf[sizeof(buf) - 1] = '\0';
	while (fgets(buf, sizeof(buf) - 1, f)) {
		if (strstr(buf, NEX_SONAME)) { /* base addr is 1st matching line */
			if (sscanf(buf, "%lx", &module_addr_xdgt1294675349dsT) == 1)
				break;
		}
	}

	fclose(f);
}

static void *g_fcn_get_module_addr_xdgt1294675349dsT
					= (void *)get_module_addr_xdgt1294675349dsT;


/*
 * HOOK_FOR_QT_QObject_connectImpl_1234569fjjoidfjfd is for the interface
 * _ZN7QObject11connectImplEPKS_PPvS1_S3_PN9QtPrivate15QSlotObjectBaseEN2Qt14ConnectionTypeEPKiPK11QMetaObject
 */

extern void get_nextsym_1234569fjjoidfjfd(void **faddr)
{
	Dl_info info;
	char *pe;
	char name[0x200] = {0};
	char fname[0x200] = {0};

	if (dladdr(*faddr, &info)) {
		/* printf("the original symbol: %s in %s.\n", info.dli_sname, info.dli_fname); */
		snprintf(fname, sizeof(fname), "%s", info.dli_fname);
		if (strstr(fname, HOOK_SONAME)) {
			snprintf(name, sizeof(name), "%s", info.dli_sname);
			pe = strstr(name, HOOK_SUFMARK);
			if (pe) {
				while (pe[strlen(HOOK_SUFMARK)] == '_') {
					pe++;
				}
				pe[0] = '\0';
			}
			faddr[0] = dlsym(RTLD_NEXT, name);
			if (dladdr(*faddr, &info)) {
				/* printf("the changed symbol: %s in %s.\n", info.dli_sname, info.dli_fname); */
			}
		}
	}
}

/* Have to add it to pass gcc building */
static void *g_fcn_get_nextsym_1234569fjjoidfjfd = (void *)get_nextsym_1234569fjjoidfjfd;

#define HOOK_FOR_QT_QObject_connectImpl_1234569fjjoidfjfd \
	{ \
		asm volatile( \
			"push %%rdi\n" \
			"push %%rsi\n" \
			"push %%rdx\n" \
			"push %%rcx\n" \
			"push %%r8\n" \
			"push %%r9\n" \
			"mov %%rdx, %%rdi\n" \
			"call *%0\n" \
			"pop %%r9\n" \
			"pop %%r8\n" \
			"pop %%rcx\n" \
			"pop %%rdx\n" \
			"pop %%rsi\n" \
			"pop %%rdi\n" \
		: : "m"(g_fcn_get_nextsym_1234569fjjoidfjfd) ); \
	}

/*
 * They are the monitor thread related functions in C code for all wrapped libs.
 *
 * This file will be merged into main.c which is the main source code file of
 * the wrapped lib, by gensocode. xdgt1294675349dsT is appended to each symbol
 * to avoid the name conflict with the interface of target shared library.
 */

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include <dlfcn.h>
#include <fcntl.h>

#include "wraplib.h"

struct monrbuf_xdgt1294675349dsT {
	unsigned long *buf; /* point to the caller pc */
	unsigned long *head; /* NULL means not used */
	unsigned long *tail; /* NULL means not used */
	const char *fname; /* the function name in so */
};

struct monthr_xdgt1294675349dsT {
	bool flag;
	struct monrbuf_xdgt1294675349dsT rbufs[]; /* end with !rbufs->buf */
};

static void addr2file_xdgt1294675349dsT(void *addr, char *name, int len)
{
	Dl_info info;

	name[0] = '\0';
	if (dladdr(addr, &info)) {
		if (strstr(info.dli_fname, HOOK_SONAME)) {
			return;
		}
		snprintf(name, len, "%s", info.dli_fname);
	}
	return;
}

static void *monitor_thread_xdgt1294675349dsT(void *param)
{
#define MONITOR_THREAD_WAIT \
	while (rbuf->head == rbuf->tail) { \
		sleep(1); \
		if (!m->flag) \
			goto end; \
	} \

	struct monthr_xdgt1294675349dsT *m =
				(struct monthr_xdgt1294675349dsT *)param;

	char filename[0x100];
	int file;
	struct wraplibrbuf *rbuf = NULL;

	snprintf(filename, sizeof(filename),
			WRAPLIB_MAPFILE_PATH_FMT, HOOK_PKGNAME, HOOK_SONAME, getpid());
	file = open(filename, O_RDWR | O_CREAT | O_TRUNC, 0600);
	if (file != -1) {
		if (ftruncate(file, sizeof(*rbuf)) == 0) {
			rbuf = mmap(0, sizeof(*rbuf), PROT_READ | PROT_WRITE,
					MAP_FILE | MAP_SHARED, file, 0);
			if (rbuf) {
				rbuf->head = 0;
				rbuf->tail = WRAPLIB_ITEMS_COUNT - 1;
			}
		}
	}

	while (m->flag) {
		struct monrbuf_xdgt1294675349dsT *p;
		char name[0x400];
		void *addr;
		struct wraplibitem *item;

		if (rbuf) {
			MONITOR_THREAD_WAIT;
			item = rbuf->items + rbuf->head;
			item->pc = 0;
			item->symbol[0] = '\0';
			item->mname[0] = '\0';
			rbuf->head = WRAPLIB_ITEM_ADD (rbuf->head);
		} else {
			sleep(1);
			printf("\n");
		}

		for (p = m->rbufs; p->buf; p++) {
			if (!p->buf[0])
				continue;
			addr = (void *)p->buf[0];
			addr2file_xdgt1294675349dsT(addr, name, sizeof(name));

			if (name[0]) {
				if (!rbuf) {
					printf("\n%s: %s: %p",
						name, p->fname, addr);
					continue;
				}
				MONITOR_THREAD_WAIT;

				item = rbuf->items + rbuf->head;
				item->pc = (unsigned long)addr;
				snprintf(item->symbol,
						sizeof(item->symbol), p->fname);
				snprintf(item->mname,
						sizeof(item->mname), name);
				rbuf->head = WRAPLIB_ITEM_ADD (rbuf->head);
				printf("\n%d:%d: %s: %s: %p",
						rbuf->head, rbuf->tail,
						name, p->fname, addr);
			}
		}
	}

end:
	if (file != -1) {
		if (rbuf)
			munmap(rbuf, sizeof(*rbuf));
		close(file);
		unlink(filename);
	}
#undef MONITOR_THREAD_WAIT

	return NULL;
}

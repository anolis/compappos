#ifndef WRAPLIB_H_123456lsfjdlksa
#define WRAPLIB_H_123456lsfjdlksa
#ifdef __cplusplus
extern "C" {
#endif

/*
 * The shared map file is in memory filesystem with a special prefix and append
 * with a wrapped shard library file name and current pid, the file contents is
 * a ring buffer.
 */
#define WRAPLIB_MAPFILE_PATH_FMT	"/dev/shm/wraplib_43095dfnvsdlkfj-PKGNAME-%s-LIBNAME-%s-PID-%d"

#define WRAPLIB_ITEMS_COUNT		0x2000 /* total items count in rbuf */
#define WRAPLIB_HEADER_LEN		0x100  /* the rbuf header len */
#define WRAPLIB_MODULENAME_LEN		0x100  /* the max module name len */
#define WRAPLIB_SYMBOL_LEN		0x80   /* the max symbol len */

/*
 * Each item decribes a relationship between an outside caller and a symbol in
 * the wrapped shared library.
 */
struct wraplibitem {
	unsigned long pc; /* the caller pc */
	char symbol[WRAPLIB_SYMBOL_LEN]; /* the symbol name */
	char mname[WRAPLIB_MODULENAME_LEN]; /* the caller module name */
};

/*
 * The shared map file structure which lists the whole items above, it's a rbuf.
 */
struct wraplibrbuf {
	union {
		struct {
			volatile int head; /* ring buffer header */
			volatile int tail; /* ring buffer tail */
		};
		char reserve[WRAPLIB_HEADER_LEN]; /* total header length */
	};
	struct wraplibitem items[WRAPLIB_ITEMS_COUNT]; /* total items */
};
#define WRAPLIB_ITEM_ADD(p) (((p) + 1) % WRAPLIB_ITEMS_COUNT)

#ifdef __cplusplus
}
#endif
#endif /* WRAPLIB_H_123456lsfjdlksa */

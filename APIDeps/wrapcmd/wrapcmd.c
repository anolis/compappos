#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <signal.h>

#include "wraplib.h"

static volatile bool gexit;

static char USAGE[] ="\
Feature: read the shared memory file to get the output information from the wrappped lib, and put the inforamtion to stdout.\n\
Usage:   %s shmfile\n\
\t\tshmfile: the shared memory file which contents the output information from the wrapped lib\n\
";

static void exit_work(int signo)
{
	gexit = true;
}

int main(int argc, char *argv[])
{
	char *filename;
	int file;
	int tail;
	struct wraplibrbuf *rbuf = NULL;
	struct wraplibitem *item;

	if (argc < 2) {
		fprintf(stderr, USAGE, argv[0]);
		return -1;
	}
	filename = argv[1];

	file = open(filename, O_RDWR);
	if (file == -1) {
		fprintf(stderr, "open file failed: %s.\n", filename);
		return -1;
	}
	rbuf = mmap(0, sizeof(*rbuf), PROT_READ | PROT_WRITE,
			MAP_FILE | MAP_SHARED, file, 0);
	if (!rbuf) {
		fprintf(stderr, "mmap file failed: %s.\n", filename);
		goto err_0;
	}

	signal(SIGTERM, exit_work);
	signal(SIGINT, exit_work);

	while (!gexit) {
		tail = WRAPLIB_ITEM_ADD(rbuf->tail);
		if (tail == rbuf->head) {
			sleep(1);
			continue;
		}
		item = rbuf->items + tail;
		printf("%d:%d: %s: %p: %s\n", rbuf->head, rbuf->tail,
					item->mname, item->pc, item->symbol);
		rbuf->tail = tail;
	}

	printf("\nexit normally.\n");
	munmap(rbuf, sizeof(*rbuf));
	close(file);
	return 0;
err_0:
	close(file);
	return -1;
}

#include <linux/elf.h>
#include <stdio.h>

#include "./utest/test.h"

static void test_main(void)
{
	struct test_struct ts = {37, 'k', 567, "hello world!", 0.123456, 0.98};
	int ret;
	char p[] = "the test from test_main";

	printf("g_testdouble: %f, gt_testobj: %ld, gt_testdata: %ld, "
		"g_testobj: %d, g_testdata: %d.\n",
		g_testdouble, gt_testobj, gt_testdata,
		g_testobj, g_testdata);

	ret = _ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn(1, 0.2, p);
	printf("the ret from _ZNSt13biasic_... is %d.\n", ret);
	ret = mytest(2, ts, 'G', "hava a test");
	printf("the ret from mytest is %d.\n", ret);
}

int main(int argc, char *argv[])
{
#if 0
	struct elf64_sym es;
	/* struct elf64_shdr esh; */
	
	printf("struct size: elf64_sym: %lx.\n", sizeof(struct elf64_sym));
	printf("struct size: elf64_sym.st_info: %lx.\n",
					(char *)&es.st_info - (char *)&es);
	printf("struct size: elf64_sym.st_shndx: %lx.\n",
					(char *)&es.st_shndx - (char *)&es);
	printf("struct size: elf64_sym.st_value: %lx.\n",
					(char *)&es.st_value - (char *)&es);
	printf("struct size: elf64_shdr: %lx.\n", sizeof(struct elf64_shdr));
#endif
	test_main();

	return 0;
}

//
// It is only for testing
//

#include <stdio.h>

#if 0

int mybase::g_ibase = 10;
const float mybase::g_cfbase = 1203;

long mybase::mybasevfunc1(char *p, int i)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}

char *mybase::mymultivfunc2(char *p, mybase &c)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return NULL;
}

char *mybase::mybasevfunc3(int i)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return NULL;
}

double mybase::mybasevfunc4(double d)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0.0;
}

int mybase::mymultivfunc5(float f)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}

int mybasefunc(char *p)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}

void ibasefunc1(double d)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
}

char *myadd::mymultivfunc2(char *p, mybase &c)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return NULL;
}

int myadd::mymultivfunc5(float f)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}

char *myextent::mymultivfunc2(char *p, mybase &c)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	mybase::mymultivfunc2(p, c);
	myadd::mymultivfunc2(p, c);
	return NULL;
}

double myextent::mybasevfunc4(double d)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0.0;
}

int mytestT::mytesttvfunc2(int i, long l)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}

#endif

#include <stdio.h>
#include <unistd.h>
#include "utest/test.hpp"


int main(int argc, char *argv[])
{
	mybase b;
	myextent e;
	mybase *pb = (mybase *)&e;
	mybaseT<int, int> bt;
	mytestT tt;

	bt.mybasetvfunc1(1, 2);
	bt.mybasetvfunc2(4, 8);

	sleep(2);

	tt.mytesttvfunc1(12, 301);
	tt.mytesttvfunc2(6, 30001);

	sleep(2);

	e.mymultivfunc2(NULL, b);
	pb->mymultivfunc5(0.0);
	e.mybasevfunc4(0.0);

	sleep(4);
	return 0;
}

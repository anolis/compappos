
struct test_struct {
	int i;
	char c;
	long l;
	char p[0x10];
	double d;
	float f;
};

extern const double g_testdouble;
extern __thread long gt_testobj;
extern __thread long gt_testdata;
extern int g_testobj;
extern int g_testdata;

extern const char g_teststr[sizeof("my test only!")];

const __thread char gt_teststr[sizeof("my test only!")];

extern int _ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn_wrap(int i,
							double d, char *p);
extern int mytest(int i, struct test_struct ts, char c, char *p);

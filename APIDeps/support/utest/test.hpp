//
// It is only for testing
//

class mybase {
public:
	mybase() {ibase = 0;};
	~mybase() {ibase = -1;};

	static int g_ibase;
	static const float g_cfbase;

	virtual long mybasevfunc1(char *p, int i);
	virtual char *mymultivfunc2(char *p, mybase &c);
	virtual char *mybasevfunc3(int i);
	virtual double mybasevfunc4(double d);
	virtual int mymultivfunc5(float f);

	int mybasefunc(char *p);
private:
	int ibase;
	void ibasefunc1(double d);
};

class myadd {
public:
	myadd() {};
	~myadd() {};
	virtual char *mymultivfunc2(char *p, mybase &c);
	virtual int mymultivfunc5(float f);
};

class myextent : mybase, myadd {
public:
	myextent() {};
	~myextent() {};

	virtual char *mymultivfunc2(char *p, mybase &c);
	virtual double mybasevfunc4(double d);
private:
};


template <class T, class TT> class mybaseT {
public:
	mybaseT() {};
	~mybaseT() {};
	virtual int mybasetvfunc1(T t, TT tt);
	virtual int mybasetvfunc2(int i, long l);
};

template <class T, class TT> int mybaseT<T, TT>::mybasetvfunc1(T t, TT tt)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}

template <class T, class TT> int mybaseT<T, TT>::mybasetvfunc2(int i, long l)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}

class mytestT {
public:
	mytestT() {};
	~mytestT() {};
	virtual int mytesttvfunc2(int i, long l);
	template <class T, class TT> int mytesttvfunc1(T t, TT tt);
};

template <class T, class TT> int mytestT::mytesttvfunc1(T t, TT tt)
{
	printf("call %s::%s@%d.\n", __FILE__, __FUNCTION__, __LINE__);
	return 0;
}

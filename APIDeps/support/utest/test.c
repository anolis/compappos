#include <stdio.h>

#include "test.h"

const double g_testdouble = 1.2;

__thread long gt_testobj_deprecated;
__thread long gt_testobj_old;
__thread long gt_testobj_new;
__asm__(".symver gt_testobj_deprecated, gt_testobj@GLIBCXX_3.4.25");
__asm__(".symver gt_testobj_old, gt_testobj@@GLIBCXX_3.4.26");
__asm__(".symver gt_testobj_new, gt_testobj@GLIBCXX_3.4.27");

__thread long gt_testdata_deprecated = 20;
__thread long gt_testdata_old = 20;
__thread long gt_testdata_new = 20;
__asm__(".symver gt_testdata_deprecated, gt_testdata@GLIBCXX_3.4.25");
__asm__(".symver gt_testdata_old, gt_testdata@@GLIBCXX_3.4.26");
__asm__(".symver gt_testdata_new, gt_testdata@GLIBCXX_3.4.27");

int g_testobj_deprecated __attribute__ ((nocommon));
int g_testobj_old __attribute__ ((nocommon));
int g_testobj_new __attribute__ ((nocommon));
__asm__(".symver g_testobj_deprecated, g_testobj@GLIBCXX_3.4.25");
__asm__(".symver g_testobj_old, g_testobj@@GLIBCXX_3.4.26");
__asm__(".symver g_testobj_new, g_testobj@GLIBCXX_3.4.27");

int g_testdata_deprecated = 20;
int g_testdata_old = 20;
int g_testdata_new = 20;
__asm__(".symver g_testdata_deprecated, g_testdata@GLIBCXX_3.4.25");
__asm__(".symver g_testdata_old, g_testdata@@GLIBCXX_3.4.26");
__asm__(".symver g_testdata_new, g_testdata@GLIBCXX_3.4.27");

const char g_teststr_deprecated[]="my test only!";
const char g_teststr_old[]="my test only!";
const char g_teststr_new[]="my test only!";
__asm__(".symver g_teststr_deprecated, g_teststr@GLIBCXX_3.4.25");
__asm__(".symver g_teststr_old, g_teststr@@GLIBCXX_3.4.26");
__asm__(".symver g_teststr_new, g_teststr@GLIBCXX_3.4.27");

const __thread char gt_teststr_deprecated[]="my test only!";
const __thread char gt_teststr_old[]="my test only!";
const __thread char gt_teststr_new[]="my test only!";
__asm__(".symver gt_teststr_deprecated, gt_teststr@GLIBCXX_3.4.25");
__asm__(".symver gt_teststr_old, gt_teststr@@GLIBCXX_3.4.26");
__asm__(".symver gt_teststr_new, gt_teststr@GLIBCXX_3.4.27");

extern int _ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn_wrap(int i, double d, char *p)
{
	printf("originally in %s\n", __FUNCTION__);
	printf("i: %d, d: %f, p: %s.\n", i, d, p);
	return 1;
}
__asm__(".symver _ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn_wrap, _ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn@@GLIBCXX_3.4.26");


extern int mytest_core(int i, struct test_struct ts, char c, char *p)
{
	printf("i: %d, ts.i: %d, ts.p: %s, ts.f: %f, c: %c, p: %s.\n",
		i, ts.i, ts.p, ts.f, c, p);
	fflush(stdout);
	return 0;
}

extern int mytest_deprecated(int i, struct test_struct ts, char c, char *p)
{
	printf("originally in %s\n", __FUNCTION__);
	return mytest_core(i, ts, c, p);
}

extern int mytest_old(int i, struct test_struct ts, char c, char *p)
{
	printf("originally in %s\n", __FUNCTION__);
	return mytest_core(i, ts, c, p);
}

extern int mytest_new(int i, struct test_struct ts, char c, char *p)
{
	printf("originally in %s\n", __FUNCTION__);
	return mytest_core(i, ts, c, p);
}
__asm__(".symver mytest_deprecated, mytest@GLIBCXX_3.4.25");
__asm__(".symver mytest_old, mytest@@GLIBCXX_3.4.26");
__asm__(".symver mytest_new, mytest@GLIBCXX_3.4.27");

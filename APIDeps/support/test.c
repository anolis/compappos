#include <stdio.h>
#include <dlfcn.h>

#include "test.h"

static long (*g_p)(void);
static long (*g_pt)(void);

static long g_ip;
static long g_tip;


extern void internal_dummy_12235(void)
{
	printf("%p", &g_testdouble);
}

__attribute__((constructor)) static void hooklib_init(void)
{
	g_p = dlsym(RTLD_NEXT, "_ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn");
	g_pt = dlsym(RTLD_NEXT, "mytest");
}

__attribute__((destructor)) static void hooklib_uninit(void)
{
	printf("g_ip: %p.\n", (void *)g_ip);
	printf("g_tip: %p.\n", (void *)g_tip);
}

extern int _ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn_hook(void)
{
	asm volatile(
		"pop	%%rbp\n"
		"push	%%rax\n"
		"lea	0x8(%%rsp), %%rax\n"
		"mov	(%%rax), %%rax\n"
		"mov	%%rax, %0\n"
		"pop	%%rax\n"
		"jmp	*%1\n"
		: "=m"(g_ip) : "m"(g_p));
	return 0;
}
__asm__(".symver _ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn_hook, _ZNSt13basic_ostreamIwSt11char_traitsIwEElsEDn@@GLIBCXX_3.4.26");


extern int mytest_deprecated(void)
{
	printf("current hook in %s\n", __FUNCTION__);
	return 0;
}

extern int mytest_old(void)
{
	asm volatile(
		"pop	%%rbp\n"
		"push   %%rax\n"
		"lea	0x8(%%rsp), %%rax\n"
		"mov	(%%rax), %%rax\n"
		"mov	%%rax, %0\n"
		"pop    %%rax\n"
		"jmp	*%1\n"
		: "=m"(g_tip) : "m"(g_pt));
	return 0;
}

extern int mytest_new(void)
{
	printf("current hook in %s\n", __FUNCTION__);
	return 1;
}
__asm__(".symver mytest_deprecated, mytest@GLIBCXX_3.4.25");
__asm__(".symver mytest_old, mytest@@GLIBCXX_3.4.26");
__asm__(".symver mytest_new, mytest@GLIBCXX_3.4.27");

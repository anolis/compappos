#!/bin/bash

basedir=$(cd "$(dirname "$0")"; pwd)
cd "$basedir"

srcpath=$1
tag=$2
if [ "x$srcpath" == "x" ]
then
	echo "need srcpath"
	exit -1
fi
if [ "x$tag" == "x" ]
then
	tag="unknown"
fi

pyname=`python3 -c "import sys; print(\"%s\" % sys.path)" | sed "s/,/\n/g" | grep '/usr/lib/python3' | sed "s/.*usr\/lib\///" | grep -v "\/" | grep -v zip | sed -e "s/'//"`
workdir="$basedir/work"
testdir="${workdir}/test"
outdir="$basedir/output"
logfile="$outdir/rpt_python-$tag.log"
errfile="$outdir/rpt_python-$tag.err"

rm -rf "${workdir}"
rm -rf "${outdir}"
mkdir "${workdir}"
mkdir "${outdir}"
cp -a "/usr/lib/$pyname/test" "${workdir}/"
pushd $srcpath
tar -cf - . | tar -xf - -C  $workdir
popd

pushd "${workdir}"
python3 -m test > "${logfile}" 2> "${errfile}"
popd

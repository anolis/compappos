#!/usr/bin/perl

use File::Basename;

use strict;

###############################################################################
# Main work
###############################################################################

sub getResultNames {
    my ( $names) = @_;
    my @fnames = @{$names->{'fnames'}};
    my @ipaths = @{$names->{'inames'}};
    my @inames;
    my @rnames;
    my @rleft;

    foreach my $ipath (@ipaths) {
        push(@inames, basename($ipath));
    }

    outputResultNames("/tmp/inames1.log", @inames);
    outputResultNames("/tmp/fnames1.log", @fnames);

    foreach my $fpath (@fnames) {
        my $fname = basename($fpath);
        my $hit = 0;
        foreach my $iname (@inames) {
            if ($iname eq $fname) {
                push(@rleft, $fpath);
                $hit = 1;
                last;
            }
        }
        if ($hit == 0) {
            push(@rnames, $fpath);
        }
    }

    foreach my $fpath (@rleft) {
        push(@rnames, $fpath);
    }

    @rnames;
}

###############################################################################
# Common work
###############################################################################

sub helpInfo() {
    print "\nUsage:\n";
    print "    ./prephdrs.pl outfilename dirname0 dirname1 ...\n";
    exit(0);
}

sub getResultFromShell {
    my ( $cmd ) = @_;
    my $fd;
    my $tname = "/tmp/abicc_shell_results";
    my @result;

    print "\ncurrent command: $cmd\n";
    system("$cmd > $tname") && die("\nRun: command failed.\n");
    open($fd, "<", $tname) || die("\nRun: open file for reading failed.\n");
    chomp(@result = <$fd>);
    close($fd);

    @result;
}

sub makestrs {
    my ( @dirnames ) = @_;
    my $str = "";

    foreach my $dirname (@dirnames) {
        $str = $str . " \"$dirname\""
    }

    $str;
}

sub getFileNames {
    my $str = makestrs(@_);

    getResultFromShell("find $str -type f");
}

sub getIncludeStrings {
    my $str = makestrs(@_);

    getResultFromShell("grep -r \"\\<include\\>\" $str | awk -F ':' '{print \$2}' | sed -e \"s/ //g\" -e \"s/\\t//g\" | grep \"^#include\\>\" | sed -e \"s/#include<//\" -e \"s/#include\\\"//\"  | sed -e \"s/>/|/\" -e \"s/\\\"/|/\" | awk -F '|' '{print \$1}' | sed \"s/\\.\\.\\///g\" | sort | uniq");
}

sub outputResultNames {
    my ( $outfname, @rnames ) = @_;
    my $fd;

    open($fd, ">", $outfname) || die("\nRun: open file for writing failed.\n");
    print $fd "$_\n" for @rnames;
    close($fd);
    0;
}

sub main {
    my @args = @_;
    my $arg;
    my @dirs;
    my $outfname;
    my @fnames;
    my @inames;
    my $names;
    my @rnames;

    if ($arg = shift(@args)) {
        $outfname = $arg;
        if ($arg = shift(@args)) {
            push(@dirs,$arg);
            while ($arg = shift(@args)) {
                push(@dirs,$arg);
            }
        } else {
            helpInfo();
        }
    } else {
        helpInfo();
    }
    if ($arg = shift(@args)) {
        helpInfo();
    }

    print "outfilename: $outfname\n";
    print "dirname:     @dirs\n";

    @fnames = getFileNames(@dirs);
    @inames = getIncludeStrings(@dirs);
    $names->{'fnames'} = \@fnames;
    $names->{'inames'} = \@inames;
    @rnames = getResultNames($names);
    return outputResultNames($outfname, @rnames);
}

exit(main(@ARGV));

#!/bin/bash

BINDIR=$(cd "$(dirname "$0")";pwd)
VERSION=1.0.0
INSTNAME="compatchk-$VERSION"
INSTDIR="${BINDIR}/$INSTNAME"

cd "${BINDIR}"
rm -rf "${INSTNAME}" "${INSTNAME}.tar.*"
mkdir -p "${INSTNAME}"
make install prefix="${INSTDIR}"
cp -a compatchk-env.sh "${INSTNAME}"
cp -a doc/*.doc "${INSTNAME}"
cp -a samples "${INSTNAME}"
tar -zcvf "${INSTNAME}.tar.gz" "${INSTNAME}"

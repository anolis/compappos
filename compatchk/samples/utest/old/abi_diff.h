#if 1
#ifndef abi_diff_h_123456789
#define abi_diff_h_123456789

#define BCONST_1	1
#define BCONST_2	2
#define BCONST_3	3

struct class_test0 {
	int i;
	char c;
	long l;
	double d;
	float f;
};

struct abi_diff {
	int i;
	char c;
	long l;
	double d;
	float f;
};

extern struct abi_diff abidiff;

#endif
#endif

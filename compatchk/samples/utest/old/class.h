#if 1
class class_a {
	char c;
	int i;
	int j;
public:
	int fn_i(int it, int *vp) {
		return it + *vp;
	}

public:
	void *pubv;
	long long publl;
	int k;
};

class_a ca;

struct struct_b {
	char c;
	short i;

private:
	int fn_i(int it, int *vp) {
		return it + *vp;
	}

private:
	char *pubvv;
	long long publl;
};

struct_b sb;
#endif

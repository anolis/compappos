#if 1
#ifndef api_diff_h_123456789
#define api_diff_h_123456789

#define CONST_1	1
#define CONST_2	2
#define CONST_3 3


struct api_diff {
	int i;
	char c;
	long l;
	double d;
	float f;
};

extern struct api_diff apidiff;

#endif
#endif

#if 1

enum test_enum {
	E_TEST_0,
	E_TEST_1,
};

#define MY_MAX(a, b)	((a) > (b) ? (a) : (b))

#define MY_CLASS(a, b)	\
	struct s_##a##_##b { \
		int i_##a; \
		char c_##b; \
	};

MY_CLASS(sym, test0);
MY_CLASS(sym, test1);

struct s_sym_test0 sym0;
struct s_sym_test1 sym1;

struct class_test_pfn {
	void (*a_func)(char *);
	int t_b;
	char c_c;
};

extern int a;
extern int b;
extern double d;

int i_a(void);
void i_b(char *);

int test_class_interface(struct class_test_pfn *c);
int test_class_interface1(struct class_test_pfn *c, int a);
int test_class_interface2(struct class_test_pfn *c, char a, int *b, int i);

static inline int test_inline(int a, int b, long *c, void *d,
				int (*pfn)(long *, void *))
{
	return a + b + pfn(c, d);
}

#endif

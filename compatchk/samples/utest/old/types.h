
struct struct_a00 {
	void *va[16];
	float f;
};

struct struct_a0 {
	int i;
	char c;
	struct struct_a00 sa;
};

struct struct_a1 {
	int ii;
	long l;
};

struct struct_a2 {
	void *va[0x10];
	long ll;
};

struct struct_a0 s_a0;

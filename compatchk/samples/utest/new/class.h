#if 1
class class_a {
	char c;
	int i;
	int fn_i(int it, int *vp) {
		return it + *vp;
	}

public:
	char *pubv;
	long long publl;
};

class_a ca;

struct struct_b {
	char c;
	int i;

protected:
	int fn_i(int it, int *vp) {
		return it + *vp;
	}

private:
	char *pubv;
	long long publl;
};

struct_b sb;
#endif


struct struct_a01 {
	char ca[16];
	double d;
};

struct struct_a0 {
	int i;
	char c;
	struct struct_a01 sa;
};

struct struct_a1 {
	int iii;
	long l;
};

struct struct_a3 {
	char ca[16];
	int ii;
};

struct struct_a0 s_a0;

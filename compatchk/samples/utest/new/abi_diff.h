#if 1
#ifndef abi_diff_h_123456789
#define abi_diff_h_123456789

#define TEST_0 0
#define TEST_1 1

struct abi_diff {
	int i;
	long l;
	char c;
	double d;
	float f;
};

extern struct abi_diff abidiff;

#endif
#endif

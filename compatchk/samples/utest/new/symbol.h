#if 1

extern int b;
extern long a;
extern float cc;
extern double ccc;

long * ii_a(void);
int * ii_b(char (*)(void), int);
char * ii_c(double);
int * i_a(void);

struct class_test_pfn {
	void (*a_func)(char *);
	long t_b;
	char c_c;
};

int test_class_interface(struct class_test_pfn *c);
int test_class_interface1(struct class_test_pfn *c, int a, void *b);
int test_class_interface2(struct class_test_pfn *c, char a, int *b);

static inline int test_inline(int a, int b, long *c, void *d,
				int (*pfn)(long *, void *, void *))
{
	return a + b + pfn(c, d, (void *)0);
}

#define MY_CLASS(a, b)	\
	struct s_##a##_##b { \
		int i_##a; \
		short c_##b; \
	};

MY_CLASS(sym, test0);
MY_CLASS(sym, test1);
MY_CLASS(symuseless, test2);

struct s_sym_test0 sym0;
struct s_sym_test1 sym1;
struct s_sym_test1 sym11;

#endif

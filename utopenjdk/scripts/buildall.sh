#!/bin/bash

basedir=$(cd "$(dirname "$0")"; pwd)
cd "$basedir"

projects=`find -maxdepth 2 -type d | grep -v "\.git" | sed -e "s/\.\///" | grep "\/" | xargs`

for project in ${projects}
do
	./build.sh $project
done

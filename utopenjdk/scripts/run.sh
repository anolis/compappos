#!/bin/bash

jname=$1
shift

if [ "x$1" != "x" ]
then
	java -cp $jname.jar "$@"
else
	java -cp $jname.jar $jname
fi

exit $?

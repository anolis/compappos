#!/bin/bash

name="`basename $1`"

rm -rf build
mkdir -p build

find $1 -type f | grep java$ | xargs javac -d build
cp -a $1/resources build/
cd build
jar -cvf ../$name.jar .

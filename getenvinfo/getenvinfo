#!/bin/bash

export LANGUAGE=en_US
export LANG=en_US.UTF-8

WRAPFILE="/tmp/getenvinfo"

BASEDIR=$(cd "$(dirname "$0")"; pwd)

function make_json {
	ifile=$1
	ofile=$2

	echo "{" > "${ofile}"
	grep -B 9999  "^pkgscount=" "${ifile}" | sed "s/\(.*\) $/\1/" | awk -F '=' '{print "\t\""$1"\":\""$2"\","}' | sed "s/\(pkgscount.*\),$/\1/" >> "${ofile}"
	echo "}" >> "${ofile}"
}

if [ $# == 5 ]
then
    echo '#!/usr/bin/expect -f' > $WRAPFILE
    echo 'set ip [lindex $argv 0]' >> $WRAPFILE
    echo 'set user [lindex $argv 1]' >> $WRAPFILE
    echo 'set passwd [lindex $argv 2]' >> $WRAPFILE
    echo "spawn scp $0 \$user@\$ip:/tmp/rcepreinfo" >> $WRAPFILE
    echo 'expect "password: "' >> $WRAPFILE
    echo 'send "$passwd\r"' >> $WRAPFILE
    echo 'expect eof' >> $WRAPFILE
    echo 'spawn ssh $user@$ip "/tmp/rcepreinfo > /tmp/rinfo.log"' >> $WRAPFILE
    echo 'expect "password: "' >> $WRAPFILE
    echo 'send "$passwd\r"' >> $WRAPFILE
    echo 'expect eof' >> $WRAPFILE
    echo "spawn scp \$user@\$ip:/tmp/rinfo.log  \"$4\"" >> $WRAPFILE
    echo 'expect "password: "' >> $WRAPFILE
    echo 'send "$passwd\r"' >> $WRAPFILE
    echo 'expect eof' >> $WRAPFILE

    chmod +x $WRAPFILE
    $WRAPFILE "$1" "$2" "$3"
    make_json $4 $5
    exit 0
elif [ $# == 4 ]
then
    ip=$1
    user=$2
    file=$3
    json=$4
    scp $0 $user@$ip:/tmp/rcepreinfo
    ssh $user@$ip "/tmp/rcepreinfo" > $file
    make_json $file $json
    exit 0;
fi

echo "kernelname=`uname -rm`"
echo "osrelease=`cat /etc/*release | grep \"DISTRIB_DESCRIPTION\|PRETTY_NAME\" | awk -F '=' '{print $2}' | xargs`"

bash -version | grep "GNU bash" | awk -F ':' '{print "bash="$1}' | tee -a $MACHINEINFO | tee -a $SOFTINFO
gcc -v 2>&1 | tail -n 1 | sed -e "s/gcc version//" | awk -F ':' '{print "gcc="$1}' | sed -e "s/= /=/" | tee -a $MACHINEINFO | tee -a $SOFTINFO
g++ -v 2>&1 | tail -n 1 | sed -e "s/gcc version//" | awk -F ':' '{print "gpp="$1}' | sed -e "s/= /=/" | tee -a $MACHINEINFO | tee -a $SOFTINFO
ld -v 2>&1 | tail -n 1 | sed -e "s/gcc version//" | awk -F ':' '{print "binutils="$1}' | tee -a $MACHINEINFO | tee -a $SOFTINFO

python2 --version >/dev/null 2>&1
if [ "x$?" == "x0" ]; then
    python2 --version 2>&1 | awk '{print "python2="$2}' | tee -a $MACHINEINFO | tee -a $SOFTINFO
else
    echo "python=unknown" | tee -a $MACHINEINFO | tee -a $SOFTINFO
fi
python3 --version >/dev/null 2>&1 | tee -a $MACHINEINFO | tee -a $SOFTINFO
if [ "x$?" == "x0" ]; then
    python3 --version 2>&1 | awk '{print "python3="$2}' | tee -a $MACHINEINFO | tee -a $SOFTINFO
else
    echo "python3=unknown" | tee -a $MACHINEINFO | tee -a $SOFTINFO
fi

java -version >/dev/null 2>&1
if [ "x$?" == "x0" ]; then
    java -version 2>&1 | head -n 1 | awk -F '"' '{print "java="$2}' | tee -a $MACHINEINFO | tee -a $SOFTINFO
else
    echo "java=unknown" | tee -a $MACHINEINFO | tee -a $SOFTINFO
fi

perl -v >/dev/null 2>&1
if [ "x$?" == "x0" ]; then
    perl -v | grep "\<version\>" | sed -e "s/.*\((\)/perl=\1/" | tee -a $MACHINEINFO | tee -a $SOFTINFO
else
    echo "perl=unknown" | tee -a $MACHINEINFO | tee -a $SOFTINFO
fi

dpkg --version > /dev/null 2>&1
if [ "x$?" == "x0" ]; then
    dpkg -l | grep "\<libc6.*:" | head -n 1 | awk '{print "glibc="$3}'
    dpkg -l | grep "libglib2\>" | awk '{print $3}' | sort | uniq -c | sort | tail -n 1 | awk '{print "glib="$2}'
    dpkg -l | grep "libstdc++" | grep -v dev | awk '{print $3}' | sort | uniq -c | tail -n 1 | awk '{print "stdcpp="$2}'
    dpkg -l | grep "\<qtbase\|qt5core" | awk '{print $3}' | sort | uniq -c | sort | tail -n 1 | awk '{print "qt="$2}'
    echo "pkgscount=`dpkg -l | grep \"^ii\" | wc -l`"
    pkgs=`dpkg -l | grep "^ii" | awk '{print $2}' | sort | xargs`
    for pkg in ${pkgs}
    do
        dpkg -l $pkg | tail -n 1 | awk '{s=$1"|"$2"|"$3"|"$4"|"; for(i=5; i<=NF; i++) s=s$i" "; print s}' | sed -e "s/ $//"
        dpkg -L $pkg
    done
else
    rpm -qa | grep "glibc-[0-9]" | sed -e "s/glibc-//" | xargs | awk -F ':' '{print "glibc="$1}'
    rpm -qa | grep "^glib2-[0-9]" | sed -e "s/glib2-//" | xargs | awk -F ':' '{print "glib="$1}'
    rpm -qa | grep "libstdc++" | grep -v devel | sed -e "s/libstdc++-//" | xargs | awk -F ':' '{print "stdcpp="$1}'
    rpm -qa | grep "qtbase-[0-9]" | sed -e "s/.*qtbase-//" | xargs | awk -F ':' '{print "qt="$1}'
    echo "pkgscount=`rpm -qa | wc -l`"
    pkgs=`rpm -qa | sort | xargs`
    for pkg in ${pkgs}
    do
        rpm -qi $pkg | grep "Name\|Version\|Release\|Architecture\|Summary" | awk -F ':' '{print "\""$2"\""}' | xargs | awk '{s="ii|"$1"|"$2"-"$3"|"$4"|"; for(i=5; i<=NF; i++) s = s$i" "; print s}' | sed -e "s/ $//"
        rpm -ql $pkg
    done
fi
exit 0

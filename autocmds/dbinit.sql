/*
 * Create tables
 */

create table autodat (
	kind enum (
		'table', /* this item is for a table */
		'cmd',   /* this item is for a command */
		'dir',   /* this item is for an absolute root dir */
		'file'   /* this item is for an absolute file path */
		) comment "this item kind: table - a table; cmd - a command; dir - an absolute root dir; file - an absolute file path",
	name varchar(256) comment 'the name, it can be a table name or a command name, which depends on the kind',
	value varchar(4096) comment "table's summary, or the command's comments, or the absolute root dir"
	) comment="Knowledge data main, which includes all descriptions about tables, commands and dirs";

create table autodatcfgs (
	include enum (
		'inc_all',  /* include with all word matching */
		'inc_reg' /* include with regular expression matching */
		) comment "include ways: inc_all - include with all word matching; inc_reg - include with regular expression matching",
	chkcontent enum (
		'not_chk_content', /* not check the contents of the config file */
		'chk_content' /* check the contents of the config file */
		) comment "whether check the contents of the config file: not_chk_content - not check the contents of the config file; chk_content - check the contents of the config file",
	name varchar(256) comment "the config file name (full path)"
	) comment="Knowledge data for os config files";

create table autodatcmds (
	id int comment "The command id, when id = 0, its workfs value is the absolute root dir for all cmds' data file",
	cmdline varchar(256) comment "the full command line (include parameters) to run",
	workfs varchar(256) comment "the file system under which the command will run, it also includes all data for the command, it is a tar.gz packaged file",
	comments varchar(1024) comment "the comments for this command"
	) comment="Knowledge data for os main commands";

create table autodatenvs (
	runmode enum (
		'gui', /* run under gui mode */
		'text' /* run under text mode */
		) comment "the running mode for the environments: gui - run under gui mode; text - run under text mode",
	name varchar(128) comment "the environments string key name",
	comments varchar(1024) comment "the comment for this environments string key name"
	) comment="Knowledge data for os environments";

create table autodatfiles (
	include enum ( /* incude ways */
		'exc_all', /* exclude with all word matching */
		'exc_reg'), /* exclude with regular expression matching */
	filetype enum (
		'dir', /* it is a directory */
		'file' /* it is a regular file  */
		) comment "the type of this file: dir - it is a directory; file - it is a regular file",
	name varchar(256) comment "the name of the file, it can be a full path or a regular expression, which depends on the include ways"
	) comment="Knowledge data for os files (include directories)";

create table autodatpkgs (
	include enum (
		'exc_all', /* exclude with all word matching */
		'exc_reg' /* exclude with regular expression matching */
		) comment "incude ways: exc_all - exclude with all word matching; exc_reg - exclude with regular expression matching",
	name varchar(256) comment "the name of the package, it can be a full package name or a regular expression, which depends on the include ways"
	) comment="Knowledge data for os packages";

/*
 * Init values
 */

insert into autodat values
	("table", "autodatpkgs",          "对包名的参考数据，包括但不限于：不参与检测比对的包名"),
	("table", "autodatfiles",         "对文件的参考数据，包括但不限于：不参与检测比对的文件或目录"),
	("table", "autodatcmds",          "对主要命令的参考数据，包括命令，参数，输入数据，输出结果的比对方法等"),
	("table", "autodatenvs",          "对主要环境变量的参考数据，包括变量名称，变量比对方法等"),
	("table", "autodatcfgs",          "对主要配置文件或目录名称的参考数据，包括文件或目录名称，内容比对方法等"),
	("cmd",   "autocmds_pkglost",     "用于统计新版OS相对于旧版OS的公共包的缺失程度，其中＂问题个数＂代表旧版OS有但新版OS中没有的公共包的总个数；＂检测总数＂是旧版OS包的总数（包括公共包和厂商私有包）；＂折算分数＂为问题个数乘以100除以检测总数．具体缺失包的包名信息参见notfound.col1文件，旧版OS包全部信息参见out0.log，新版OS包全部信息参见out1.log"),
	("cmd",   "autocmds_pkgdiff",     "用于统计新版OS相对于旧版OS的公共包的差异程度，其中＂问题个数＂代表虽然新旧版OS都有但版本不同的公共包总个数；＂检测总数＂是旧版OS包的总数（包括公共包和厂商私有包）；＂折算分数＂为问题个数乘以100除以检测总数．具体公共包的差异信息参见diffver.col3文件，里面每行为一个公共包的差异信息，第一列为包名，第二列为旧版OS下对应包版本，第三列为新版OS下对应包版本；旧版OS包全部信息参见out0.log，新版OS包全部信息参见out1.log"),
	("cmd",   "autocmds_dir",         "通过获取新版OS中整体一级，二级和三级目录相对于旧版OS对应目录的缺失情况，来衡量新旧版本OS间整体目录结构的差异．其中＂问题个数＂代表一级目录的缺失个数乘以10，加上二级目录的缺失个数乘以5，再加上三级目录的缺失个数；＂检测总数＂未使用，因为整体目录结构基本已形成标准，每个目录对整体的影响都比较显著，不会因实际被检测的目录多少而产生变化；＂折算分数＂同＂问题个数＂一致；具体的详细差异信息汇总参见deldirs.l123，1/2/3级目录各自的差异信息分别在l1/l2/l3.patch文件中进行描述；旧版OS包全部信息参见out0.log，新版OS包全部信息参见out1.log．"),
	("cmd",   "autocmds_filelost",    "用于统计新版OS相对于旧版OS的公共可执行文件和相关目录的缺失程度，其中＂问题个数＂代表旧版OS有但新版OS中没有的公共可执行文件或相关目录的总个数；＂检测总数＂代表新旧版OS先对公用可执行文件和相关目录进行整体比对，再排除新版OS新增公用可执行文件和相关目录，由此所得的比对总数；＂折算分数＂为问题个数乘以100除以检测总数．具体缺失的公共二进制可执行文件的文件名信息参见fileonly.col1，缺失的公用脚本可执行文件的文件名信息参见shfileonly.diff；对公用二进制可执行文件和相关目录的整体比对结果信息参见filediffall.patch，对公用脚本可执行文件和相关目录的整体比对结果信息参见shfilediffall.patch；旧版OS公用可执行文件和相关目录的全部信息参见out0.log，新版OS公用可执行文件和相关目录的全部信息参见out1.log"),
	("cmd",   "autocmds_filediff",    "用于统计新版OS相对于旧版OS的公共可执行文件差异程度，其中＂问题个数＂代表虽然新旧版OS都有但内容不同的公共可执行文件的总个数；＂检测总数＂代表新旧版OS先对公共可执行文件和相关目录进行整体比对，再排除新版OS新增公共可执行文件和相关目录，由此所得的比对总数；＂折算分数＂为问题个数乘以100除以检测总数．具体有差异的公共脚本可执行文件的文件名信息参见shfilediff.diff；对于有差异的公共二进制可执行文件的文件名信息参见filedifftyp.col1（两个文件类型不同），filediffsym.col1（两个文件为软链接，其指向不同），filediffcon.col1（两个文件内容不同）；对公共二进制可执行文件和相关目录的整体比对结果信息参见filediffall.patch，对公共脚本可执行文件和相关目录的整体比对结果信息参见shfilediffall.patch；旧版OS公共可执行文件和相关目录的全部信息参见out0.log，新版OS公共可执行文件和相关目录的全部信息参见out1.log"),
	("cmd",   "autocmds_cfgall",      "用于统计新旧版OS间系统公用配置文件和相关目录的缺失与差异，其中＂问题个数＂代表关键配置文件或相关目录存在问题的文件总个数，其问题可能是旧版OS中存在此文件或目录但新版OS中不存在，也可能是新旧版OS中对应文件有差异；＂检测总数＂＂代表新旧版OS先对共用配置文件和相关目录进行整体比对，再排除新版OS新增共用配置文件和相关目录，由此所得的比对总数；＂折算分数＂为问题核数乘以100除以检测总数．具体问题信息参见cfgchkall.diff，更详细的整体比对信息参见fileall.patch，其中的内容是对全部公用配置文件进行简单整理后再进行比对的结果．新旧版本OS的原始公共配置文件列表分别在out0.log和out1.log中"),
	("cmd",   "autocmds_cfgmain",     "用于新旧版OS间关键配置文件的内容比对，其中＂问题个数＂代表关键配置文件的内容存在问题的总行数，其问题可能是旧版OS中存在此行内容但新版OS中不存在，也可能是新旧版OS中对应行的内容有差异；＂检测总数＂代表旧版OS下所有关键配置文件的内容总行数；＂折算分数＂为问题核数乘以100除以检测总数．具体问题信息参见cfgchk.col2，更详细的比对信息参见diffile.patch，其中的内容是对相关配置文件进行简单整理后的比对差异．新旧版本OS的原始关键配置文件分别打包在chroot0.tar.gz文件和chroot1.tar.gz文件中"),
	("cmd",   "autocmds_attr",        "用于新旧版OS间关键目录和文件的属性比对．其中＂问题个数＂代表属性存在问题的文件或目录的总个数，其问题可能是旧版OS中存在此文件或目录但新版OS中不存在，也可能是新旧版OS中对应的文件或目录的属性有差异；＂检测总数＂代表旧版OS下被检测文件或目录的属性的总数；＂折算分数＂为问题个数乘以100除以检测总数．具体的问题信息参见attrdiff.ion，具体旧版OS上的运行日志信息参见out0.log，新版OS上的运行日志信息参见out1.log"),
	("cmd",   "autocmds_env",         "用于新旧版OS间基础环境变量的内容比对．其中＂问题个数＂代表内容存在问题的环境变量的总个数，其问题可能是旧版OS中存在此环境变量但新版OS中不存在，也可能是新旧版OS中对应的环境变量的内容有差异；＂检测总数＂代表旧版OS下被>检测环境变量的总数；＂折算分数＂为问题个数乘以100除以检测总数．具体的问题信息参见envdiff.ion，具体旧版OS上的运行日志信息参见out0.log，新版OS上的运行日志信息参见out1.log"),
	("cmd",   "autocmds_cmd",         "用于新旧版OS的主要命令的运行比对，其中＂问题个数＂代表运行结果不一致的命令总个数，也包括旧版OS中存在但新版OS中不存在的命令；＂检测总数＂代表所有被检测命令的总数；＂折算分数＂为问题个数乘以100除以检测总数．具体的问题信息参见各个diff文件，其名称为命令id加上.diff后缀；命令id与具体详细命令的对照可在id2cmd.log文件中查看；具体旧版OS上的运行日志信息参见out0.log，新版OS上的运行日志信息参见out1.log"),
	("cmd",   "autocmds_filescripts", "用户新旧版OS的主要脚本库的内容比对．其中＂问题个数＂代表新版OS相对于旧版OS的全部主要脚本库内容的更改和删除的总行数；＂检测总数＂代表旧版OS的全部主要脚本库内容的总行数；＂折算分数＂为问题个数乘以100除以检测总数．具体的问题信息参见shfilediffcon.diff，新旧版OS的原始主要脚本库分别打包在chroot0.shell.tar.gz文件和chroot1.shell.tar.gz文件中");

insert into autodatpkgs values
	('exc_all', 'apt'),
	('exc_all', 'yum'),
	('exc_all', 'rpm'),
	('exc_all', 'devscripts'),
	('exc_reg', '^apt-'),
	('exc_reg', '^rpm-'),
	('exc_reg', '^yum-'),
	('exc_reg', '^centos-'),
	('exc_reg', '^libapt-'),
	('exc_reg', '^libdpkg-');

insert into autodatfiles values
	('exc_reg', 'file', 'bin/apt\\\>'),
	('exc_reg', 'file', 'bin/dpkg\\\>'),
	('exc_reg', 'file', 'bin/yum\\\>'),
	('exc_reg', 'file', 'bin/rpm\\\>'),
	('exc_reg', 'file', 'bin/debuild\\\>'),
	('exc_reg', 'file', 'bin/rpmbuild\\\>'),
	('exc_reg', 'file', '\\\<jpg$'),
	('exc_reg', 'file', '\\\<png$'),
	('exc_reg', 'file', '\\\<gif$'),
	('exc_reg', 'file', '\\\<wav$'),
	('exc_all', 'dir',  '/etc'),
	('exc_all', 'dir',  '/opt'),
	('exc_all', 'dir',  '/home'),
	('exc_all', 'dir',  '/var'),
	('exc_all', 'dir',  '/run'),
	('exc_all', 'dir',  '/dev'),
	('exc_all', 'dir',  '/mnt'),
	('exc_all', 'dir',  '/srv'),
	('exc_all', 'dir',  '/boot'),
	('exc_all', 'dir',  '/root'),
	('exc_all', 'dir',  '/media'),
	('exc_all', 'dir',  '/sys'),
	('exc_all', 'dir',  '/proc'),
	('exc_all', 'dir',  '/tmp'),
	('exc_all', 'dir',  '/lost+found'),
	('exc_all', 'dir',  '/lib/modules'),
	('exc_all', 'dir',  '/lib/firmware'),
	('exc_all', 'dir',  '/usr/games'),
	('exc_all', 'dir',  '/usr/include'),
	('exc_all', 'dir',  '/usr/lib/modules'),
	('exc_all', 'dir',  '/usr/lib/firmware'),
	('exc_all', 'dir',  '/usr/local'),
	('exc_all', 'dir',  '/usr/share/doc'),
	('exc_all', 'dir',  '/usr/share/help'),
	('exc_all', 'dir',  '/usr/share/icons'),
	('exc_all', 'dir',  '/usr/share/licenses'),
	('exc_all', 'dir',  '/usr/share/games'),
	('exc_all', 'dir',  '/usr/src'),
	('exc_reg', 'dir',  '\\\<jvm\\\>'),
	('exc_reg', 'dir',  '\\\<python\\\>'),
	('exc_reg', 'dir',  '\\\<python3'),
	('exc_reg', 'dir',  '\\\<python2'),
	('exc_reg', 'dir',  '\\\<perl\\\>'),
	('exc_reg', 'dir',  '\\\<perl5'),
	('exc_reg', 'dir',  '\\\<gcc\\\>'),
	('exc_reg', 'dir',  '\\\<golang\\\>');

insert into autodatcmds values
	(1, "ls -lat --full-time | awk '{print \$1, \$9}'", "ls.tar.gz", "期望比对结果相同"),
	(2, "export LANG=en_US.UTF-8; ls -lat --full-time | awk '{print \$1, \$9}'", "ls.tar.gz", "期望比对结果相同"),
	(3, "sed -e \"s/work.*c\$/work/\" -e \"s/^test/work/\" test.txt", "sed.tar.gz", "期望比对结果相同"),
	(4, "cat test.txt | awk -F ':' '{print \$1, \$2}'", "awk.tar.gz", "期望比对结果相同"),
	(5, "sort -n test.txt", "sort.tar.gz", "期望比对结果相同"),
	(6, "ls -l `which passwd` | awk '{print \$1}'", "", "期望比对结果相同");

insert into autodatenvs values
	('text', "HOME", "当前登录用户的Home目录，例如对于root一般为/root"),
	('text', "LANG", "系统默认语言，例如zh_CN.UTF-8"),
	('text', "LOGNAME", "登录用户名，两个系统需要先设置成一致, 一般为root"),
	('text', "MAIL", "接收邮件的位置，同用户名相关，两个系统需要先设置成一致的用户名，一般为root"),
	('text', "PATH", "至少包含/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin，并保证其拓扑顺序"),
	('text', "PWD",  "当前目录，等同于pwd命令显示的位置"),
	('text', "SHELL", "当前shell，例如/bin/bash"),
	('text', "SHLVL", "当前shell，嵌套的深度数，刚刚登录后为起始深度，有的系统是从0开始，有的是从1开始"),
	('text', "TERM",  "当前用户使用的通道，所对应的终端程序名称和描述"),
	('text', "USER",  "当前用户名称，两个系统需要先设置成一致, 一般为root"),
	('text', "_",  "当前被使用的环境变量，也可以理解为最后一次使用的环境变量"),
	('gui', "LANGUAGE", "当前应用使用的语言，例如en_US:en"),
	('gui', "DISPLAY", "当前图形环境的显示设备"),
	('gui', "SESSION_MANAGER", "当前系统会话管理器");

insert into autodatcfgs values
	('inc_all', 'not_chk_content', '/etc/group'),
	('inc_all', 'not_chk_content', '/etc/group-'),
	('inc_all', 'not_chk_content', '/etc/gshadow'),
	('inc_all', 'not_chk_content', '/etc/gshadow-'),
	('inc_all', 'chk_content', '/etc/hosts'),
	('inc_all', 'not_chk_content', '/etc/passwd'),
	('inc_all', 'not_chk_content', '/etc/passwd-'),
	('inc_all', 'chk_content', '/etc/protocols'),
	('inc_all', 'chk_content', '/etc/services'),
	('inc_all', 'not_chk_content', '/etc/shadow'),
	('inc_all', 'not_chk_content', '/etc/shadow-'),
	('inc_all', 'not_chk_content', '/etc/sudoers'),
	('inc_all', 'not_chk_content', '/bin/su'),
	('inc_all', 'not_chk_content', '/usr/bin/sudo'),
	('inc_all', 'not_chk_content', '/home'),
	('inc_all', 'not_chk_content', '/root'),
	('inc_all', 'not_chk_content', '/tmp');

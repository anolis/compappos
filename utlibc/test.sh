#!/bin/bash

basedir=$(cd "$(dirname "$0")"; pwd)

ONLYCOUNT="false"
count=0

if [ "$1" == "-c" ]; then
	ONLYCOUNT="true"
	shift
fi

RPTFILE="${basedir}/rpt_libc-$1.csv"
ERRFILE="${basedir}/rpt_libc-$1.err"
INFOFILE="${basedir}/rpt_libc-$1.log"

EXECS_MISC="\
	tst-dirname \
	tst-efgcvt \
	tst-error1 \
	tst-fdset \
	tst-hsearch \
	tst-insremque \
	tst-mntent2 \
	tst-mntent \
	tst-pselect \
	tst-tsearch \
	"
EXECS_POSIX="\
	runptests \
	runtests \
	testfnm \
	test-vfork \
	tst-cpucount \
	tst-cpuset \
	tst-execl1 \
	tst-execl2 \
	tst-execle1 \
	tst-execle2 \
	tst-execlp1 \
	tst-execlp2 \
	tst-execv1 \
	tst-execv2 \
	tst-execve1 \
	tst-execve2 \
	tst-execvp1 \
	tst-execvp2 \
	tst-execvp3 \
	tst-execvp4 \
	tst-fnmatch2 \
	tst-fork \
	tst-getaddrinfo2 \
	tst-getaddrinfo3 \
	tst-getaddrinfo \
	tst-getlogin \
	tst-gnuglob \
	tst-mmap \
	tst-nanosleep \
	tst-nice \
	tst-preadwrite64 \
	tst-preadwrite \
	tst-sysconf \
	tst-truncate64 \
	tst-truncate \
	tst-vfork1 \
	tst-vfork2 \
	tst-vfork3 \
	tst-waitid \
	wordexp-test \
	"
EXECS_DIRENT="\
	tst-fdopendir2 \
	tst-fdopendir \
	tst-seekdir \
	"
EXECS_LIBIO="\
	test-fmemopen \
	tst-atime \
	tst-eof \
	tst-ext2 \
	tst-ext \
	tst-fopenloc2 \
	tst-fopenloc \
	tst-freopen \
	tst-fwrite-error \
	tst-memstream1 \
	tst-memstream2 \
	tst-mmap2-eofsync \
	tst-mmap-eofsync \
	tst-mmap-fflushsync \
	tst-mmap-offend \
	tst-mmap-setvbuf \
	tst-popen1 \
	tst-setvbuf1 \
	tst-sscanf \
	tst-wmemstream1 \
	tst-wmemstream2 \
	"
EXECS_IO="\
	ftwtest \
	test-lfs \
	test-stat2 \
	test-stat \
	test-utime \
	tst-faccessat \
	tst-fchmodat \
	tst-fchownat \
	tst-fcntl \
	tst-fstatat \
	tst-futimesat \
	tst-getcwd \
	tst-linkat \
	tst-mkdirat \
	tst-mkfifoat \
	tst-mknodat \
	tst-openat \
	tst-posix_fallocate \
	tst-readlinkat \
	tst-renameat \
	tst-statvfs \
	tst-symlinkat \
	tst-ttyname_r \
	tst-unlinkat \
	"
EXECS_DLFCN="\
	failtest \
	tst-dladdr \
	tst-dlinfo \
	"
EXECS_NPTL="\
	./tst-cancelx8 \
	./tst-mutex5 \
	./tst-spin2 \
	./tst-exit2 \
	./tst-stack2 \
	./tst-robust6 \
	./tst-signal5 \
	./tst-stack1 \
	./tst-tsd2 \
	./tst-mutexpp1 \
	./tst-cond15 \
	./tst-spin3 \
	./tst-tsd1 \
	./tst-cancel21 \
	./tst-tsd3 \
	./tst-sem1 \
	./tst-cancel19 \
	./tst-basic5 \
	./tst-raise1 \
	./tst-robustpi2 \
	./tst-cancel16 \
	./tst-cancel17 \
	./tst-robust7 \
	./tst-basic4 \
	./tst-mutexpi6 \
	./tst-mutex1 \
	./tst-kill4 \
	./tst-join5 \
	./tst-cancel6 \
	./tst-once1 \
	./tst-dlsym1 \
	./tst-oncex4 \
	./tst-mutexpi1 \
	./tst-sem9 \
	./tst-cancel20 \
	./tst-kill5 \
	./tst-cond17 \
	./tst-cancelx17 \
	./tst-mutexpi4 \
	./tst-oncex3 \
	./tst-sem10 \
	./tst-mutex2 \
	./tst-barrier2 \
	./tst-mutex4 \
	./tst-locale2 \
	./tst-eintr2 \
	./tst-cancelx20 \
	./tst-vfork2 \
	./tst-stack3 \
	./tst-setuid1-static \
	./tst-cancel14 \
	./tst-cancel22 \
	./tst-detach1 \
	./tst-fini1 \
	./tst-join1 \
	./tst-exec2 \
	./tst-signal3 \
	./tst-sem5 \
	./tst-cleanupx1 \
	./tst-rwlock2a \
	./tst-cleanupx3 \
	./tst-cond18 \
	./tst-tsd4 \
	./tst-eintr5 \
	./tst-initializers1 \
	./tst-robust2 \
	./tst-cond7 \
	./tst-backtrace1 \
	./tst-cleanup0 \
	./tst-unload \
	./tst-cond1 \
	./tst-sem8 \
	./tst-cancel3 \
	./tst-cancel24 \
	./tst-stdio1 \
	./tst-mutex6 \
	./tst-cancelx14 \
	./tst-cancel9 \
	./tst-join3 \
	./tst-cancelx11 \
	./tst-initializers1-c99 \
	./tst-robust1 \
	./tst-sem2 \
	./tst-key2 \
	./tst-cancelx3 \
	./tst-mutexpi5 \
	./tst-cancel21-static \
	./tst-flock1 \
	./tst-cleanup4 \
	./tst-robust3 \
	./tst-cond10 \
	./tst-mutex8 \
	./tst-cancel18 \
	./tst-sem4 \
	./tst-vfork1 \
	./tst-cond2 \
	./tst-exit1 \
	./tst-mutexpi2 \
	./tst-cond-except \
	./sysdeps/pthread/tst-mqueue8x \
	./sysdeps/pthread/tst-timer \
	./tst-attr1 \
	./tst-cancelx1 \
	./tst-cancel13 \
	./tst-robust9 \
	./tst-rwlock1 \
	./tst-robustpi4 \
	./tst-cancelx13 \
	./tst-eintr4 \
	./tst-stdio2 \
	./tst-sem7 \
	./tst-key3 \
	./tst-cleanup2 \
	./tst-initializers1-gnu99 \
	./tst-cancel1 \
	./tst-cleanup1 \
	./tst-cleanup3 \
	./tst-sem6 \
	./tst-robustpi5 \
	./tst-cancelx9 \
	./tst-cond11 \
	./tst-rwlock6 \
	./tst-exec3 \
	./tst-cancel-self-testcancel \
	./tst-cleanupx0 \
	./tst-flock2 \
	./tst-join6 \
	./tst-cancel12 \
	./tst-rwlock5 \
	./tst-getpid3 \
	./tst-cond23 \
	./tst-cancel15 \
	./tst-cleanupx2 \
	./tst-align3 \
	./tst-cancelx2 \
	./tst-cond9 \
	./tst-join4 \
	./tst-robustpi3 \
	./tst-signal1 \
	./tst-cond24 \
	./tst-tls1 \
	./tst-sysconf \
	./tst-barrier4 \
	./tst-cancel23 \
	./tst-tsd6 \
	./tst-cond8 \
	./tst-align \
	./tst-rwlock4 \
	./tst-robustpi9 \
	./tst-tls2 \
	./tst-cancel10 \
	./tst-cancelx10 \
	./tst-cancelx16 \
	./tst-fork3 \
	./tst-cancelx12 \
	./tst-kill2 \
	./tst-cond22 \
	./tst-sem3 \
	./tst-barrier1 \
	./tst-basic2 \
	./tst-robustpi1 \
	./tst-cond19 \
	./tst-cond8-static \
	./tst-cancel-self-canceltype \
	./tst-mutex9 \
	./tst-initializers1-c89 \
	./tst-tsd5 \
	./tst-vfork1x \
	./tst-locale1 \
	./tst-fork4 \
	./tst-cancel24-static \
	./tst-robust4 \
	./tst-initializers1-gnu89 \
	./tst-tls4 \
	./tst-setuid1 \
	./tst-rwlock2 \
	./tst-cancelx18 \
	./tst-mutexpi3 \
	./tst-umask1 \
	./tst-signal4 \
	./tst-cancel11 \
	./tst-sched1 \
	./tst-cond6 \
	./tst-kill6 \
	./tst-fork1 \
	./tst-rwlock12 \
	./tst-key4 \
	./tst-popen1 \
	./tst-cancel2 \
	./tst-cond25 \
	./tst-cond16 \
	./tst-cancelx15 \
	./tst-rwlock14 \
	./tst-basic6 \
	./tst-mutex8-static \
	./tst-cancel-self \
	./tst-spin1 \
	./tst-mutexpi9 \
	./tst-mutex5a \
	./tst-cond4 \
	./tst-cancel-self-cancelstate \
	./tst-attr2 \
	./tst-signal6 \
	./tst-basic1 \
	./tst-mutexpi8 \
	./tst-abstime \
	./tst-exec1 \
	./tst-context1 \
	./tst-kill1 \
	./tst-mutex3 \
	./tst-mutexpp10 \
	./tst-mutexpi5a \
	./tst-cond14 \
	./tst-robust5 \
	./tst-mutexpp6 \
	./tst-robustpi6 \
	./tst-cond12 \
	./tst-cond13 \
	./tst-kill3 \
	./tst-atfork1 \
	./tst-vfork2x \
	./tst-join2 \
	./tst-once4 \
	./tst-exit3 \
	./tst-robustpi7 \
	./tst-cancelx21 \
	./tst-once3 \
	./tst-rwlock3 \
	./tst-basic7 \
	./tst-fork2 \
	./tst-clock1 \
	./tst-rwlock13 \
	./tst-signal2 \
	./tst-cleanupx4 \
	./tst-mutexpi8-static \
	./tst-rwlock7 \
	./tst-eintr3 \
	./tst-key1 \
	./tst-cancel8 \
	./tst-cond5 \
	./tst-cancelx6 \
	"
#
# depend on libgomp
#
#	atest-exp2 \
#	atest-exp \
#	atest-sincos \
#
EXECS_MATH="\
	basic-test \
	test-fpucw \
	test-misc \
	test-powl \
	test-tgmath-int \
	test-tgmath-ret \
	tst-CMPLX2 \
	tst-CMPLX \
	tst-definitions \
	"
EXECS_STDIO="\
	temptest \
	test-fseek \
	test-fwrite \
	test-popen \
	test_rdwr \
	test-vfprintf \
	tst-cookie \
	tst-fdopen \
	tst-fileno \
	tst-fmemopen2 \
	tst-fmemopen \
	tst-fphex \
	tst-fphex-wide \
	tst-fseek \
	tst-fwrite \
	tst-long-dbl-fphex \
	tst-obprintf \
	tst-perror \
	tst-popen2 \
	tst-popen \
	tst-printf \
	tst-printf-round \
	tst-printfsz \
	tst-put-error \
	tst-rndseek \
	tst-setvbuf1 \
	tst-sprintf2 \
	tst-sprintf3 \
	tst-sscanf \
	tst-swscanf \
	tst-tmpnam \
	tst-unbputc \
	tst-ungetc \
	tst-unlockedio \
	tst-wc-printf \
	"
EXECS_MALLOC="\
	tst-calloc \
	tst-malloc \
	tst-mallocfork \
	tst-mcheck \
	tst-mtrace \
	tst-obstack \
	tst-trim1 \
	tst-valloc \
	"
EXECS_STDLIB="\
	test-a64l \
	test-canon2 \
	test-canon \
	testmb \
	testrand \
	testsort \
	tst-atof1 \
	tst-atof2 \
	tst-bsearch \
	tst-environ \
	tst-fmtmsg \
	tst-limits \
	tst-makecontext2 \
	tst-makecontext3 \
	tst-makecontext \
	tst-qsort2 \
	tst-qsort \
	tst-rand48-2 \
	tst-rand48 \
	tst-random2 \
	tst-random \
	tst-secure-getenv \
	tst-setcontext \
	tst-strtod2 \
	tst-strtod6 \
	tst-strtod-overflow \
	tst-strtod-round \
	tst-strtol \
	tst-strtoll \
	tst-system \
	tst-unsetenv1 \
	tst-xpg-basename \
	"
EXECS_STRING="\
	inl-tester \
	noinl-tester \
	test-bcopy \
	test-bzero \
	testcopy \
	tester \
	test-ffs \
	test-memccpy \
	test-memchr \
	test-memcmp \
	test-memcpy \
	test-memmem \
	test-memmove \
	test-mempcpy \
	test-memset \
	test-rawmemchr \
	test-stpcpy \
	test-stpncpy \
	test-strcasecmp \
	test-strcasestr \
	test-strcat \
	test-strchr \
	test-strchrnul \
	test-strcmp \
	test-strcpy \
	test-strcspn \
	test-strlen \
	test-strncasecmp \
	test-strncat \
	test-strncmp \
	test-strncpy \
	test-strnlen \
	test-strpbrk \
	test-strrchr \
	test-strspn \
	test-strstr \
	tst-bswap \
	tst-endian \
	tst-inlcall \
	tst-strfry \
	tst-strlen \
	tst-strtok \
	tst-strtok_r \
	tst-svc2 \
	"
#
# The following execs need additonal complex ways to run.
#
# posix/tst-fnmatch
# libio/tst-widetext
# stdio-common/tst-ferror
# stdio-common/tst-fgets
# stdio-common/tst-gets
# stdlib/gen-tst-strtod-round
# stdlib/testdiv
# string/tst-svc
# posix/globtest
# posix/tst-boost
# posix/tst-chmod
# posix/tst-exec
# posix/tst-pcre
# posix/tst-regex2
# posix/tst-rxspencer
# posix/tst-spawn
# libio/test-freopen
# libio/tst-fgetwc
# nptl/tst-rwlock8
# nptl/tst-rwlock11
# nptl/tst-robustpi8
# nptl/tst-basic3
# nptl/tst-cond20
# nptl/tst-mutexpi7
# nptl/tst-mutexpi7a
# nptl/tst-sem12
# nptl/tst-sem14
# nptl/tst-mutex7a
# nptl/tst-mutex7
# nptl/tst-atfork2
# nptl/tst-exec4
# nptl/tst-eintr1
# nptl/tst-sem11
# nptl/tst-barrier3
# nptl/tst-once2
# nptl/tst-robust8
# nptl/tst-sem11-static
# nptl/tst-clock2
# nptl/tst-cond3
# nptl/tst-rwlock9
# nptl/tst-sem12-static
# nptl/tst-rwlock10
# nptl/tst-cond21
# nptl/tst-oddstacklimit
# math/test-double
# math/test-fenv
# math/test-float
# math/test-idouble
# math/test-ifloat
# math/test-ildoubl
# math/test-ldouble
# math/test-tgmath2
# math/test-tgmath
# malloc/tst-malloc-usable
# stdlib/tst-putenv
# string/tst-strtod3
# string/tst-strtod4
# string/tst-strtod5
# string/tst-strtod

#
# Obsoleted
#
# malloc/tst-mallocstate

#
# Fail for glibc-2.28 x86_64
#
# posix/tst-getopt_long1
# libio/tst-fgetws
# libio/tst-fseek
# libio/tst-swscanf
# libio/tst-ungetwc1
# libio/tst-ungetwc2
# math/test-matherr
# stdio-common/tst-grouping
# stdio-common/tst-sprintf
# stdio-common/tst-swprintf
# stdlib/testmb2
# stdlib/tst-strtod-underflow
# stdlib/tst-tininess
# tst-strxfrm2
# tst-strxfrm

#
# Fail for glibc-2.31 x86_64
#
# nptl/tst-cancelx7
# nptl/tst-pthread-getattr
# nptl/tst-sem13
# nptl/tst-cancel7

#
# At present, they are not used
#
# string/test-bcopy-ifunc
# string/test-bzero-ifunc
# string/test-memccpy-ifunc
# string/test-memchr-ifunc
# string/test-memcmp-ifunc
# string/test-memcpy-ifunc
# string/test-memmem-ifunc
# string/test-memmove-ifunc
# string/test-mempcpy-ifunc
# string/test-memset-ifunc
# string/test-rawmemchr-ifunc
# string/test-stpcpy-ifunc
# string/test-stpncpy-ifunc
# string/test-strcasecmp-ifunc
# string/test-strcasestr-ifunc
# string/test-strcat-ifunc
# string/test-strchr-ifunc
# string/test-strchrnul-ifunc
# string/test-strcmp-ifunc
# string/test-strcpy-ifunc
# string/test-strcspn-ifunc
# string/test-strlen-ifunc
# string/test-strncasecmp-ifunc
# string/test-strncat-ifunc
# string/test-strncmp-ifunc
# string/test-strncpy-ifunc
# string/test-strnlen-ifunc
# string/test-strpbrk-ifunc
# string/test-strrchr-ifunc
# string/test-strspn-ifunc
# string/test-strstr-ifunc

EXEC=
EXECS=
SUBDIR=

run_tst() {
	echo -n "running $EXEC ..."
	echo "" >> ${INFOFILE}
	echo "running $EXEC ..." >> ${INFOFILE}
	echo "" >> ${ERRFILE}
	echo "running $EXEC ..." >> ${ERRFILE}
	./$EXEC >> ${INFOFILE} 2>>${ERRFILE}
	ret=$?
	echo "   ret: $ret"
	if [ "$ret" != "0" ]; then
		echo "Fail,$ret,$EXEC" >> ${RPTFILE}
	fi
	sleep 1
}

run_tsts() {
	if [ "$ONLYCOUNT" == "false" ]; then
		echo "enter $SUBDIR..."
		echo "enter $SUBDIR..." >> ${INFOFILE}
		echo "enter $SUBDIR..." >> ${ERRFILE}
		cd $SUBDIR
		export LD_LIBRARY_PATH=`pwd`
		for EXEC in ${EXECS}
		do
			run_tst
		done
		cd ..
		echo "leave $SUBDIR..."
		echo "leave $SUBDIR..." >> ${INFOFILE}
		echo "leave $SUBDIR..." >> ${ERRFILE}
	else
		subcount=0
		for EXEC in ${EXECS}
		do
			subcount=$(($subcount + 1))
		done
		count=$(($count + $subcount))
		echo "$subcount $SUBDIR"
	fi
}


cd ${basedir}
if [ "$ONLYCOUNT" == "false" ]; then
	rm -rf ${RPTFILE} ${ERRFILE} ${INFOFILE}
	touch ${RPTFILE} ${ERRFILE} ${INFOFILE}
	echo "VERSION: 1.0.0 from glibc-2.17"
	echo "VERSION: 1.0.0 from glibc-2.17" >> ${INFOFILE}
	echo "VERSION: 1.0.0 from glibc-2.17" >> ${ERRFILE}
	echo "VERSION: 1.0.0 from glibc-2.17" >> ${RPTFILE}
fi

SUBDIR="misc"
EXECS=$EXECS_MISC
run_tsts

SUBDIR="posix"
EXECS=$EXECS_POSIX
run_tsts

SUBDIR="dirent"
EXECS=$EXECS_DIRENT
run_tsts

SUBDIR="libio"
EXECS=$EXECS_LIBIO
run_tsts

SUBDIR="io"
EXECS=$EXECS_IO
run_tsts

SUBDIR="dlfcn"
EXECS=$EXECS_DLFCN
run_tsts

SUBDIR="nptl"
EXECS=$EXECS_NPTL
run_tsts

SUBDIR="math"
EXECS=$EXECS_MATH
run_tsts

SUBDIR="stdio-common"
EXECS=$EXECS_STDIO
run_tsts

SUBDIR="malloc"
EXECS=$EXECS_MALLOC
run_tsts

SUBDIR="stdlib"
EXECS=$EXECS_STDLIB
run_tsts

SUBDIR="string"
EXECS=$EXECS_STRING
run_tsts

if [ "$ONLYCOUNT" == "false" ]; then
	sync
	gzip -c ${RPTFILE} > ${RPTFILE}.gz
	gzip -c ${ERRFILE} > ${ERRFILE}.gz
	gzip -c ${INFOFILE} > ${INFOFILE}.gz
	sync
else
	echo "total: $count"
fi

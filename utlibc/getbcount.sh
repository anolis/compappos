#!/bin/bash

SUBDIRS="argp assert catgets crypt debug dirent dlfcn gmon grp gshadow \
	iconv iconvdata inet intl io libio locale localedata login malloc \
	math misc nptl nss posix pwd resolv resource rt setjmp shadow signal \
	stdio-common stdlib string time timezone wcsmbs wctype"

count=0

for dir in $SUBDIRS;
do
	subcount=`grep '\\\\' $dir/Makefile | grep -A10000 BINS | grep -v BINS | wc -l`
	echo $subcount $dir
	count=$(($count + $subcount))
done

echo "total: $count"

#!/bin/bash

BINDIR=$(cd "$(dirname "$0")";pwd)

TMPDIR="/tmp/pkgdeps"
TMPFILE="$TMPDIR/tmp.log"
RESFILE="$TMPDIR/result.log"
ALLFILE="$TMPDIR/allfile.log"
ALLFILELF="$TMPDIR/allfilelf.log"
REFFILE="$TMPDIR/reffile.log"
CPYFILE="$TMPDIR/cpyfile.log"
PKGSCMD0="$TMPDIR/pkgscmd0"
PKGSCMD1="$TMPDIR/pkgscmd1"
PKGSCMD2="$TMPDIR/pkgscmd2"
PKGSFILES="$TMPDIR/pkgsfiles"
PKGSFAILURES0="$TMPDIR/pkgsfalures0"
PKGSFAILURES="$TMPDIR/pkgsfalures"
PKGSRESULTS="$TMPDIR/pkgsresults"

PKGCMD=""

#
# Preparation
#

declare -a PARAMS
COUNT=$#
OCMD=$@

if [ "x$1" = "x" ]; then
	echo ""
	echo "Get all shlibs' pkgs which application will use."
	echo ""
	echo "Version 1.0.0"
	echo ""
	echo "Usage: $0 cmdline ..."
	echo "      cmdline ...:    the command line for the application, its environments and its parameters."
	echo ""
	exit 0
fi

for (( i = 0; i < COUNT; i++ ))
do
	if [ "$1" = "%u" ]; then
		PARAMS[$i]="$USER"
	else
		PARAMS[$i]="$1"
	fi
	shift
done

echo "cmdline:    $OCMD"
echo "real cmd:   ${PARAMS[@]}"
echo "tmpdir:     $TMPDIR"

rm -rf $TMPDIR
mkdir -p $TMPDIR

#
# Get all files from strace -f
#
echo "start...."
rm -rf $TMPFILE*
echo "start step0 : strace -e trace=file -ff -x -o $TMPFILE "${PARAMS[@]}" >/dev/null"
strace -e trace=file -ff -x -o $TMPFILE "${PARAMS[@]}" >/dev/null

cat $TMPFILE* | sed -e "s/^\[pid.*\] //" | grep -v "ENOENT" | grep -v "^-" | grep -v "^+" | grep -v "^Gtk-Message" | grep -v "^Sandbox" | grep -v "^WebGL" | grep -v "^rmdir" | grep -v "^getcwd" | grep -v "^statx" | grep -v "^unlink" | sort | uniq  > $RESFILE

cat $RESFILE | grep -v "^symlink" | grep -v "^rename" | grep -v "^mkdir" | grep -v "^inotify_add_watch" | awk -F '"' '{print $2}' | sort | uniq > $ALLFILE

cat $RESFILE | grep "^mkdir" | sort | uniq > $REFFILE
cat $RESFILE | grep "^inotify_add_watch" | sort | uniq >> $REFFILE
cat $RESFILE | grep "^symlink" | sort | uniq >> $REFFILE
cat $RESFILE | grep "^rename" | sort | uniq >> $REFFILE

grep -v "^/dev\>" $ALLFILE | grep -v "^/proc\>" | grep -v "/sys\>" | grep -v "^/run\>" | grep -v "/tmp\>" | grep -v "^/var/cache\>" | grep -v "^/var/lib/dbus\>" > $CPYFILE


#
# Get all elf files simply, which do not consider about the symbol link.
#

rm -rf $ALLFILELF
touch $ALLFILELF

OIFS=$IFS
IFS='
'
pathf=($(cat "$ALLFILE"))
IFS=$OIFS
for pathe in "${pathf[@]}"
do
	path=`echo -en $pathe`
	if ! test -e "${path}"; then
		continue
	fi
	file -b "${path}" | grep -i "^ELF" > /dev/null 2>&1
	if [ "$?" != "0" ]; then
		echo "${path}" | grep "\.so\.[0-9]" > /dev/null 2>&1
		if [ "$?" != "0" ]; then
			echo "${path}" | grep "\.so$" > /dev/null 2>&1
			if [ "$?" != "0" ]; then
				continue
			fi
		fi
	fi
	echo "${path}" >> $ALLFILELF
done


#
# Get pkgs of elf files
#

rm -rf $PKGSCMD0
rm -rf $PKGSCMD1
rm -rf $PKGSCMD2
touch $PKGSCMD0
touch $PKGSCMD1
touch $PKGSCMD2
chmod +x $PKGSCMD0
chmod +x $PKGSCMD1
chmod +x $PKGSCMD2

dpkg --version > /dev/null 2>&1
if [ "$?" == "0" ]; then
	PKGCMD="dpkg -S "
fi

function run_rpm_cmds {
	ifile=$1
	ofile=$2
	efile=$3
	if [ "x$PKGCMD" != "x" ]
	then
		$ifile >> $ofile 2>> $efile
		return
	fi

	OIFS=$IFS
	IFS='
'
	pathf=($(cat "$ifile"))
	IFS=$OIFS
	for pathe in "${pathf[@]}"
	do
		path=`echo -en $pathe`
		rpm -qf "${path}" > $TMPFILE 2>&1 && echo "${path}" >> $TMPFILE
		if [ "$?" != "0" ]
		then
			cat $TMPFILE >> $efile
		else
			cat $TMPFILE | xargs | sed -e "s/ /: /" >> $ofile
		fi
	done
}

cat $ALLFILELF  | awk "{print \"$PKGCMD\"\$1}" >> $PKGSCMD0
run_rpm_cmds $PKGSCMD0 $PKGSFILES $PKGSFAILURES0

grep -v " \/lib" $PKGSFAILURES0 | grep -v " \/usr\/lib" > $PKGSFAILURES
grep " \/lib" $PKGSFAILURES0 | sed -e "s/ \/lib/|\/usr\/lib/" | awk -F '|' '{print $2}' | awk "{print \"$PKGCMD\"\$1}" >> $PKGSCMD1
run_rpm_cmds $PKGSCMD1 $PKGSFILES $PKGSFAILURES
grep " \/usr\/lib" $PKGSFAILURES0 | sed -e "s/ \/usr\/lib/|\/lib/" | awk -F '|' '{print $2}' | awk "{print \"$PKGCMD\"\$1}" >> $PKGSCMD2
run_rpm_cmds $PKGSCMD2 $PKGSFILES $PKGSFAILURES

#
# Report the results
#

cat $PKGSFILES | awk '{print $1}' | sort | uniq > $PKGSRESULTS

echo "Process succeed"
echo "the results in:  $PKGSRESULTS"
echo "the failure in:  $PKGSFAILURES"
exit 0

#ifndef abi_diff_h_123456789
#define abi_diff_h_123456789

struct abi_diff {
	int i;
	char c;
	long l;
	double d;
	float f;
};

extern struct abi_diff abidiff;

#endif

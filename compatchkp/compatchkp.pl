#!/usr/bin/perl
use Getopt::Long;
Getopt::Long::Configure ("posix_default", "no_ignore_case");
use File::Path qw(mkpath rmtree);
use File::Copy qw(copy);
use File::Basename qw(dirname);
use Cwd qw(abs_path cwd);

my $TOOL_VERSION = "2.3";
my $ABI_DUMP_VERSION = "3.5";
my $ABI_DUMP_VERSION_MIN = "3.5";

my $XML_REPORT_VERSION = "1.2";
my $XML_ABI_DUMP_VERSION = "1.2";

# Internal modules
my $MODULES_DIR = getModules();
push(@INC, dirname($MODULES_DIR));

# Basic modules
my %LoadedModules = ();
loadModule("Basic");
loadModule("Input");
loadModule("Path");
loadModule("Logging");
loadModule("Utils");
loadModule("TypeAttr");
loadModule("Filter");
loadModule("SysFiles");
loadModule("PrepHeaders");
loadModule("Descriptor");
loadModule("Mangling");

my $CmdName = getFilename($0);

print "$CmdName, version 1.0.0\n";

if($#ARGV==-1)
{
    exit(0);
}

GetOptions(
  "h|help!" => \$In::Opt{"Help"},
  "i|info!" => \$In::Opt{"InfoMsg"},
  "v|version!" => \$In::Opt{"ShowVersion"},
  "dumpversion!" => \$In::Opt{"DumpVersion"},
# General
  "l|lib|library=s" => \$In::Opt{"TargetLib"},
  "d1|xmlpath|o=s" => \$In::Desc{1}{"Path"},
  "dump|dump-abi|dump_abi=s" => \$In::Opt{"DumpABI"},
  "d|f|filter=s" => \$In::Opt{"FilterPath"},
# Extra
  "debug!" => \$In::Opt{"Debug"},
  "debug-mangling!" => \$In::Opt{"DebugMangling"},
  "ext|extended!" => \$In::Opt{"ExtendedCheck"},
  "static|static-libs!" => \$In::Opt{"UseStaticLibs"},
  "gcc-path|cross-gcc=s" => \$In::Opt{"CrossGcc"},
  "gcc-prefix|cross-prefix=s" => \$In::Opt{"CrossPrefix"},
  "gcc-options=s" => \$In::Opt{"GccOptions"},
  "count-symbols=s" => \$In::Opt{"CountSymbols"},
  "use-dumps!" => \$In::Opt{"UseDumps"},
  "xml!" => \$In::Opt{"UseXML"},
  "app|application=s" => \$In::Opt{"AppPath"},
  "headers-only!" => \$In::Opt{"CheckHeadersOnly"},
  "v1|vnum1|version1=s" => \$In::Desc{1}{"TargetVersion"},
  "relpath1=s" => \$In::Desc{1}{"RelativeDirectory"},
# Test
  "test!" => \$In::Opt{"TestTool"},
  "test-dump!" => \$In::Opt{"TestDump"},
  "test-abi-dumper!" => \$In::Opt{"TestABIDumper"},
# Report
  "s|strict!" => \$In::Opt{"StrictCompat"},
  "binary|bin|abi!" => \$In::Opt{"BinOnly"},
  "source|src|api!" => \$In::Opt{"SrcOnly"},
  "warn-newsym!" => \$In::Opt{"WarnNewSym"},
# Report path
  "report-path=s" => \$In::Opt{"OutputReportPath"},
  "bin-report-path=s" => \$In::Opt{"BinReportPath"},
  "src-report-path=s" => \$In::Opt{"SrcReportPath"},
# Report format
  "show-retval!" => \$In::Opt{"ShowRetVal"},
  "stdout!" => \$In::Opt{"StdOut"},
  "report-format=s" => \$In::Opt{"ReportFormat"},
  "old-style!" => \$In::Opt{"OldStyle"},
  "title=s" => \$In::Opt{"TargetTitle"},
  "component=s" => \$In::Opt{"TargetComponent"},
  "p|params=s" => \$In::Opt{"ParamNamesPath"},
  "limit-affected|affected-limit=s" => \$In::Opt{"AffectLimit"},
  "all-affected!" => \$In::Opt{"AllAffected"},
  "list-affected!" => \$In::Opt{"ListAffected"},
# ABI dump
  "dump-path=s" => \$In::Opt{"OutputDumpPath"},
  "dump-format=s" => \$In::Opt{"DumpFormat"},
  "check!" => \$In::Opt{"CheckInfo"},
  "extra-info=s" => \$In::Opt{"ExtraInfo"},
  "extra-dump!" => \$In::Opt{"ExtraDump"},
  "relpath=s" => \$In::Desc{1}{"RelativeDirectory"},
  "vnum=s" => \$In::Desc{1}{"TargetVersion"},
  "sort!" => \$In::Opt{"SortDump"},
# Filter symbols and types
  "symbols-list=s" => \$In::Opt{"SymbolsListPath"},
  "types-list=s" => \$In::Opt{"TypesListPath"},
  "skip-symbols=s" => \$In::Opt{"SkipSymbolsListPath"},
  "skip-types=s" => \$In::Opt{"SkipTypesListPath"},
  "skip-internal-symbols|skip-internal=s" => \$In::Opt{"SkipInternalSymbols"},
  "skip-internal-types=s" => \$In::Opt{"SkipInternalTypes"},
  "keep-cxx!" => \$In::Opt{"KeepCxx"},
  "keep-reserved!" => \$In::Opt{"KeepReserved"},
# Filter header files
  "skip-headers=s" => \$In::Opt{"SkipHeadersPath"},
  "headers-list=s" => \$In::Opt{"TargetHeadersPath"},
  "header=s" => \$In::Opt{"TargetHeader"},
  "nostdinc!" => \$In::Opt{"NoStdInc"},
  "tolerance=s" => \$In::Opt{"Tolerance"},
  "tolerant!" => \$In::Opt{"Tolerant"},
  "skip-unidentified!" => \$In::Opt{"SkipUnidentified"},
# Filter rules
  "skip-typedef-uncover!" => \$In::Opt{"SkipTypedefUncover"},
  "check-private-abi!" => \$In::Opt{"CheckPrivateABI"},
  "disable-constants-check!" => \$In::Opt{"DisableConstantsCheck"},
  "skip-added-constants!" => \$In::Opt{"SkipAddedConstants"},
  "skip-removed-constants!" => \$In::Opt{"SkipRemovedConstants"},
# Other
  "lang=s" => \$In::Opt{"UserLang"},
  "arch=s" => \$In::Opt{"TargetArch"},
  "mingw-compatible!" => \$In::Opt{"MinGWCompat"},
  "cxx-incompatible|cpp-incompatible!" => \$In::Opt{"CxxIncompat"},
  "cpp-compatible!" => \$In::Opt{"CxxCompat"},
  "quick!" => \$In::Opt{"Quick"},
  "force!" => \$In::Opt{"Force"},
# OS analysis
  "dump-system=s" => \$In::Opt{"DumpSystem"},
  "cmp-systems!" => \$In::Opt{"CmpSystems"},
  "sysroot=s" => \$In::Opt{"SystemRoot"},
  "sysinfo=s" => \$In::Opt{"TargetSysInfo"},
  "libs-list=s" => \$In::Opt{"TargetLibsPath"},
# Logging
  "log-path=s" => \$In::Opt{"LoggingPath"},
  "log1-path=s" => \$In::Desc{1}{"OutputLogPath"},
  "logging-mode=s" => \$In::Opt{"LogMode"},
  "q|quiet!" => \$In::Opt{"Quiet"}
) or errMsg();

sub errMsg()
{
    exit(getErrorCode("Error"));
}

# Default log path
$In::Opt{"DefaultLog"} = "logs/run.log";
$In::Opt{"DefaultTmpDir"} = "/tmp/compatchkp/";

# Aliases
my (%SymbolInfo, %TypeInfo, %TName_Tid, %Constants) = ();


# Symbols
my %AddSymbolParams;

sub getModules()
{
    my $TOOL_DIR = dirname($0);
    if(not $TOOL_DIR)
    { # patch for MS Windows
        $TOOL_DIR = ".";
    }
    my @SEARCH_DIRS = (
        # tool's directory
        abs_path($TOOL_DIR),
        # relative path to modules
        abs_path($TOOL_DIR)."/../share/compatchkp",
        # install path
        'MODULES_INSTALL_PATH'
    );
    foreach my $DIR (@SEARCH_DIRS)
    {
        if($DIR!~/\A(\/|\w+:[\/\\])/)
        { # relative path
            $DIR = abs_path($TOOL_DIR)."/".$DIR;
        }
        if(-d $DIR."/modules") {
            return $DIR."/modules";
        }
    }
    
    print STDERR "ERROR: can't find modules (Did you installed the tool by 'make install' command?)\n";
    exit(9); # Module_Error
}

sub loadModule($)
{
    my $Name = $_[0];
    if(defined $LoadedModules{$Name}) {
        return;
    }
    my $Path = $MODULES_DIR."/Internals/$Name.pm";
    if(not -f $Path)
    {
        print STDERR "can't access \'$Path\'\n";
        exit(2);
    }
    require $Path;
    $LoadedModules{$Name} = 1;
}

sub compareInit()
{
    # read input XML descriptors or ABI dumps
    if(not $In::Desc{1}{"Path"}) {
        exitStatus("Error", "-xmlpath option is not specified");
    }
    if(not -e $In::Desc{1}{"Path"}) {
        exitStatus("Access_Error", "can't access \'".$In::Desc{1}{"Path"}."\'");
    }
    
    detectDefaultPaths(undef, undef, "bin", undef); # to extract dumps
    
    printMsg("INFO", "Preparing, please wait ...");
    
    {
        loadModule("ABIDump");
        readDesc(createDesc($In::Desc{1}{"Path"}, 1), 1);
        
        initLogging(1);
        detectDefaultPaths("inc", "lib", undef, "gcc");
        createABIDump(1);
    }
}

sub initAliases($)
{
    my $LVer = $_[0];
    
    initABI($LVer);
    
    $SymbolInfo{$LVer} = $In::ABI{$LVer}{"SymbolInfo"};
    $TypeInfo{$LVer} = $In::ABI{$LVer}{"TypeInfo"};
    $TName_Tid{$LVer} = $In::ABI{$LVer}{"TName_Tid"};
    $Constants{$LVer} = $In::ABI{$LVer}{"Constants"};
    
    initAliases_TypeAttr($LVer);
}

sub scenario()
{
    setTarget("default");
    
    initAliases(1);
    initAliases(2);
    
    $In::Opt{"Locale"} = "C.UTF-8";
    $In::Opt{"OrigDir"} = cwd();
    $In::Opt{"Tmp"} = $In::Opt{"DefaultTmpDir"};
    system("mkdir -p " . $In::Opt{"Tmp"});
    $In::Opt{"TargetLibShort"} = libPart($In::Opt{"TargetLib"}, "shortest");
    
    $In::Opt{"DoubleReport"} = 0;
    $In::Opt{"JoinReport"} = 1;
    
    $In::Opt{"SysPaths"}{"include"} = [];
    $In::Opt{"SysPaths"}{"lib"} = [];
    $In::Opt{"SysPaths"}{"bin"} = [];
    
    $In::Opt{"CompileError"} = 0;
    
    if($In::Opt{"TargetComponent"}) {
        $In::Opt{"TargetComponent"} = lc($In::Opt{"TargetComponent"});
    }
    else
    { # default: library
        $In::Opt{"TargetComponent"} = "library";
    }
    
    foreach (keys(%{$In::Desc{0}}))
    { # common options
        $In::Desc{1}{$_} = $In::Desc{0}{$_};
    }
    
    $In::Opt{"AddTemplateInstances"} = 1;
    $In::Opt{"GccMissedMangling"} = 0;
    
    if($In::Opt{"StdOut"})
    { # enable quiet mode
        $In::Opt{"Quiet"} = 1;
        $In::Opt{"JoinReport"} = 1;
    }
    if(not $In::Opt{"LogMode"})
    { # default
        $In::Opt{"LogMode"} = "w";
    }
    
    if($In::Opt{"UserLang"}) {
        $In::Opt{"UserLang"} = uc($In::Opt{"UserLang"});
    }
    
    if(my $LoggingPath = $In::Opt{"LoggingPath"})
    {
        $In::Desc{1}{"OutputLogPath"} = $LoggingPath;
        if($In::Opt{"Quiet"}) {
            $In::Opt{"DefaultLog"} = $LoggingPath;
        }
    }
    
    if($In::Opt{"Force"}) {
        $In::Opt{"GccMissedMangling"} = 1;
    }
    
    if($In::Opt{"Quick"}) {
        $In::Opt{"AddTemplateInstances"} = 0;
    }
    if(my $DP = $In::Opt{"OutputDumpPath"})
    { # validate
        if(not isDump($DP)) {
            exitStatus("Error", "the dump path should be a path to *.dump or *.dump.".$In::Opt{"Ar"}." file");
        }
    }
    if($In::Opt{"BinOnly"}
    and $In::Opt{"SrcOnly"})
    { # both --binary and --source
      # is the default mode
        if(not $In::Opt{"CmpSystems"})
        {
            $In::Opt{"BinOnly"} = 0;
            $In::Opt{"SrcOnly"} = 0;
        }
        
        $In::Opt{"DoubleReport"} = 1;
        $In::Opt{"JoinReport"} = 0;
        
        if($In::Opt{"OutputReportPath"})
        { # --report-path
            $In::Opt{"DoubleReport"} = 0;
            $In::Opt{"JoinReport"} = 1;
        }
    }
    elsif($In::Opt{"BinOnly"}
    or $In::Opt{"SrcOnly"})
    { # --binary or --source
        $In::Opt{"DoubleReport"} = 0;
        $In::Opt{"JoinReport"} = 0;
    }
    $In::Opt{"ReportFormat"} = "html";
    if($In::Opt{"Quiet"} and $In::Opt{"LogMode"}!~/a|n/)
    { # --quiet log
        if(-f $In::Opt{"DefaultLog"}) {
            unlink($In::Opt{"DefaultLog"});
        }
    }
    if($In::Opt{"ExtraInfo"}) {
        $In::Opt{"CheckUndefined"} = 1;
    }
    
    if($In::Opt{"Tolerant"})
    { # enable all
        $In::Opt{"Tolerance"} = 1234;
    }
    if($In::Opt{"ExtendedCheck"}) {
        $In::Opt{"CheckHeadersOnly"} = 1;
    }
    if($In::Opt{"SystemRoot"})
    { # user defined root
        if(not -e $In::Opt{"SystemRoot"}) {
            exitStatus("Access_Error", "can't access \'".$In::Opt{"SystemRoot"}."\'");
        }
        $In::Opt{"SystemRoot"}=~s/[\/]+\Z//g;
        if($In::Opt{"SystemRoot"}) {
            $In::Opt{"SystemRoot"} = getAbsPath($In::Opt{"SystemRoot"});
        }
    }

    if(my $TargetLibsPath = $In::Opt{"TargetLibsPath"})
    {
        if(not -f $TargetLibsPath) {
            exitStatus("Access_Error", "can't access file \'$TargetLibsPath\'");
        }
        foreach my $Lib (split(/\s*\n\s*/, readFile($TargetLibsPath)))
        {
            if($In::Opt{"OS"} eq "windows") {
                $In::Opt{"TargetLibs"}{lc($Lib)} = 1;
            }
            else {
                $In::Opt{"TargetLibs"}{$Lib} = 1;
            }
        }
    }
    if(my $TPath = $In::Opt{"TargetHeadersPath"})
    { # --headers-list
        if(not -f $TPath) {
            exitStatus("Access_Error", "can't access file \'$TPath\'");
        }
        
        $In::Desc{1}{"TargetHeader"} = {};
        
        foreach my $Header (split(/\s*\n\s*/, readFile($TPath)))
        {
            my $Name = getFilename($Header);
            $In::Desc{1}{"TargetHeader"}{$Name} = 1;
        }
    }
    if($In::Opt{"TargetHeader"})
    { # --header
        $In::Desc{1}{"TargetHeader"} = {};
        
        my $Name = getFilename($In::Opt{"TargetHeader"});
        $In::Desc{1}{"TargetHeader"}{$Name} = 1;
    }

    if(not $In::Opt{"CountSymbols"})
    {
        if(not $In::Opt{"TargetLib"}) {
            exitStatus("Error", "library name is not selected (-l option)");
        }
        else
        { # validate library name
            if($In::Opt{"TargetLib"}=~/[\*\/\\]/) {
                exitStatus("Error", "\"\\\", \"\/\" and \"*\" symbols are not allowed in the library name");
            }
        }
    }
    
    if(not $In::Opt{"TargetTitle"}) {
        $In::Opt{"TargetTitle"} = $In::Opt{"TargetLib"};
    }
    
    if(my $SymbolsListPath = $In::Opt{"SymbolsListPath"})
    {
        if(not -f $SymbolsListPath) {
            exitStatus("Access_Error", "can't access file \'$SymbolsListPath\'");
        }
        foreach my $S (split(/\s*\n\s*/, readFile($SymbolsListPath)))
        {
            $In::Desc{1}{"SymbolsList"}{$S} = 1;
        }
    }
    if(my $TypesListPath = $In::Opt{"TypesListPath"})
    {
        if(not -f $TypesListPath) {
            exitStatus("Access_Error", "can't access file \'$TypesListPath\'");
        }
        foreach my $Type (split(/\s*\n\s*/, readFile($TypesListPath)))
        {
            $In::Desc{1}{"TypesList"}{$Type} = 1;
        }
    }
    if(my $SymbolsListPath = $In::Opt{"SkipSymbolsListPath"})
    {
        if(not -f $SymbolsListPath) {
            exitStatus("Access_Error", "can't access file \'$SymbolsListPath\'");
        }
        foreach my $Interface (split(/\s*\n\s*/, readFile($SymbolsListPath)))
        {
            $In::Desc{1}{"SkipSymbols"}{$Interface} = 1;
        }
    }
    if(my $TypesListPath = $In::Opt{"SkipTypesListPath"})
    {
        if(not -f $TypesListPath) {
            exitStatus("Access_Error", "can't access file \'$TypesListPath\'");
        }
        foreach my $Type (split(/\s*\n\s*/, readFile($TypesListPath)))
        {
            $In::Desc{1}{"SkipTypes"}{$Type} = 1;
        }
    }
    if(my $HeadersList = $In::Opt{"SkipHeadersPath"})
    {
        if(not -f $HeadersList) {
            exitStatus("Access_Error", "can't access file \'$HeadersList\'");
        }
        foreach my $Path (split(/\s*\n\s*/, readFile($HeadersList)))
        {
            my ($CPath, $Type) = classifyPath($Path);
            $In::Desc{1}{"SkipHeaders"}{$Type}{$CPath} = 1;
        }
    }
    if(my $ParamNamesPath = $In::Opt{"ParamNamesPath"})
    {
        if(not -f $ParamNamesPath) {
            exitStatus("Access_Error", "can't access file \'$ParamNamesPath\'");
        }
        foreach my $Line (split(/\n/, readFile($ParamNamesPath)))
        {
            if($Line=~s/\A(\w+)\;//)
            {
                my $Interface = $1;
                if($Line=~/;(\d+);/)
                {
                    while($Line=~s/(\d+);(\w+)//) {
                        $AddSymbolParams{$Interface}{$1}=$2;
                    }
                }
                else
                {
                    my $Num = 0;
                    foreach my $Name (split(/;/, $Line)) {
                        $AddSymbolParams{$Interface}{$Num++}=$Name;
                    }
                }
            }
        }
    }
    
    if(my $AppPath = $In::Opt{"AppPath"})
    {
        if(not -f $AppPath) {
            exitStatus("Access_Error", "can't access file \'$AppPath\'");
        }
        
        detectDefaultPaths(undef, undef, "bin", "gcc");
        foreach my $Symbol (readSymbols_App($AppPath)) {
            $In::Opt{"SymbolsList_App"}{$Symbol} = 1;
        }
    }
    compareInit();
}

scenario();

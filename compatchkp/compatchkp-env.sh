#!/bin/bash

CURDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")";pwd)
BINDIR="${CURDIR}/bin"
export PATH="$PATH:${BINDIR}"
export LANG=C.UTF-8
export LANGUAGE=C

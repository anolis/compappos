#ifndef api_diff_h_123456789
#define api_diff_h_123456789

struct api_diff {
	int i;
	char c;
	long l;
	double dd;
	float f;
};

extern struct api_diff apidiff;

#endif

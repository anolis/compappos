use File::Basename;

use strict;

sub outputResultNames {
    my ( $outfname, @rnames ) = @_;
    my $fd;

    open($fd, ">", $outfname) || die("\nRun: open file for writing failed for $outfname.\n");
    print $fd "$_\n" for @rnames;
    close($fd);
    0;
}

sub getResultNames {
    my ( $names) = @_;
    my @fnames = @{$names->{'fnames'}};
    my @ipaths = @{$names->{'inames'}};
    my @inames;
    my @rnames;
    my @rleft;

    foreach my $ipath (@ipaths) {
        push(@inames, basename($ipath));
    }

    foreach my $fpath (@fnames) {
        my $fname = basename($fpath);
        my $hit = 0;
        foreach my $iname (@inames) {
            if ($iname eq $fname) {
                push(@rleft, $fpath);
                $hit = 1;
                last;
            }
        }
        if ($hit == 0) {
            push(@rnames, $fpath);
        }
    }

    foreach my $fpath (@rleft) {
        push(@rnames, $fpath);
    }

    outputResultNames($In::Opt{"DefaultTmpDir"} . "inames.log", @inames);
    outputResultNames($In::Opt{"DefaultTmpDir"} . "fnames.log", @fnames);
    outputResultNames($In::Opt{"DefaultTmpDir"} . "rnames.log", @rnames);

    @rnames;
}

sub getResultFromShell {
    my ( $cmd ) = @_;
    my $fd;
    my $tname = $In::Opt{"DefaultTmpDir"} . "abicc_shell_results";
    my @result;

    print "\ncurrent command: $cmd\n";
    system("$cmd > $tname") && die("\nRun: command failed.\n");
    open($fd, "<", $tname) || die("\nRun: open file for reading failed.\n");
    chomp(@result = <$fd>);
    close($fd);

    @result;
}

sub makestrs {
    my ( @dirnames ) = @_;
    my $str = "";

    foreach my $dirname (@dirnames) {
        $str = $str . " \"$dirname\""
    }

    $str;
}

sub getIncludeStrings {
    my $str = makestrs(@_);

    getResultFromShell("grep -r \"\\<include\\>\" $str | awk -F ':' '{print \$2}' | sed -e \"s/ //g\" -e \"s/\\t//g\" | grep \"^#include\\>\" | sed -e \"s/#include<//\" -e \"s/#include\\\"//\"  | sed -e \"s/>/|/\" -e \"s/\\\"/|/\" | awk -F '|' '{print \$1}' | sed \"s/\\.\\.\\///g\" | sort | uniq");
}

sub sortHeadersByInclude {
    my ( $params ) = @_;
    my @inames;
    my $names;

    @inames = getIncludeStrings(@{$params->{'dirs'}});
    $names->{'fnames'} = \@{$params->{'fnames'}};
    $names->{'inames'} = \@inames;
    getResultNames($names);
}

return 1;

#!/bin/bash

BINDIR=$(cd "$(dirname "$0")";pwd)
VERSION=1.0.0
PKGNAME="compatchkp-$VERSION"
INSTDIR="${BINDIR}/$PKGNAME"

cd "${BINDIR}"
rm -rf ${INSTDIR} *.tar.gz *.tar.bz2
mkdir "${INSTDIR}"
make install prefix="${INSTDIR}"
cp compatchkp-env.sh "${INSTDIR}"
cp -a doc/samples "${INSTDIR}"
cp -a doc/compatchkp* "${INSTDIR}"
tar -zcvf $PKGNAME.tar.gz $PKGNAME
